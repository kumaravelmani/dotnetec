﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using StackExchange.Redis;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using Newtonsoft.Json;

/// <summary>
/// Summary description for FDW
/// </summary>
[Serializable()]
public sealed class FDW
{
    private static readonly FDW instance = new FDW();
    LogFile.Log logObj = new LogFile.Log();
    public string schemaNameKey = string.Empty;
    public string db_type = String.Empty;
    public string db_connection = String.Empty;
    [NonSerialized]
    private readonly ConfigurationOptions config;
    [NonSerialized]
    public ConnectionMultiplexer redis;
    string redisIP = string.Empty;
    string redisPass = string.Empty;
    static ArrayList dsRefresh = new ArrayList();
    //FDWConCOunt- gives the nuber of connections made to redis- it should always be 0 or 1
    static int FDWConCount = 0;
    //FDWCreateDef - gives the number of definitions added to fastdata - this value will increment for each project having fastdata
    static int FDWCreateDef = 0;
    //FDWRefresh - gives the number of refresh datasets happened- this is global across projects
    static int FDWRefresh = 0;
    public static FDW Instance
    {
        get
        {
            return instance;
        }
    }


    public FDW()
    {
        if (redis == null)
        {
            if (ConfigurationManager.AppSettings["redisIP"] != null && ConfigurationManager.AppSettings["redisIP"].ToString() != "")
            {
                if (ConfigurationManager.AppSettings["redisPass"] != null)
                {
                    redisPass = ConfigurationManager.AppSettings["redisPass"].ToString();
                }

                FDWConCount = FDWConCount + 1;


                config = new ConfigurationOptions
                {
                    SyncTimeout = int.MaxValue,
                    KeepAlive = 60,
                    Password = redisPass,
                    AbortOnConnectFail = false,
                    AllowAdmin = true,
                };

                redisIP = ConfigurationManager.AppSettings["redisIP"].ToString();
                foreach (var rIP in redisIP.Split(','))
                {
                    config.EndPoints.Add(rIP);
                }


                try
                {
                    redis = ConnectionMultiplexer.Connect(config);
                }
                catch (Exception ex)
                { }
            }
        }

    }

    public void Initialize(string strProj)
    {
        if (!IsNullOrEmpty(strProj) && (redis != null ? redis.IsConnected == true : false))
        {
            string contents = "";
            if (HttpContext.Current.Application["axApps"] != null)
                contents = HttpContext.Current.Application["axApps"].ToString();
            else if (HttpContext.Current.Session["axApps"] != null)
                contents = HttpContext.Current.Session["axApps"].ToString();
            HttpContext.Current.Application["ProjUser"] = strProj;
            //schemaNameKey = strProj;
            GetDBConnection(strProj, contents);
			//cleanup
            //if (DSasJsonFromRedis(Constants.RS_FD_DEFINITION, schemaName) == null)
            //{
            //    DataSet DsDefinition = GetDefinition(schemaName);
            //    if (DsDefinition.Tables.Count > 0)
            //        CreateFdDataset(DsDefinition, false, schemaName);
            //    CreateFDThread(schemaName);
            //    FDWCreateDef = FDWCreateDef + 1;
            //}

        }
    }


//cleanup
    //public void CreateFdDataset(DataSet dtDefin, bool isLogedin)
    //{
    //    try
    //    {
    //        Ihelper objHelper = new Helper().SetDatabase(db_type, db_connection);
    //        foreach (DataRow row in dtDefin.Tables[Constants.FD_DT_DEFINITION].Rows)
    //        {
    //            string strDsName = row["DataSetName"].ToString();
    //            string strSqlQuery = row["SQLText"].ToString();
    //            string strLoadEvent = row["LoadEvent"].ToString().ToLower();
    //            if (!IsNullOrEmpty(strDsName) && !IsNullOrEmpty(strSqlQuery) && !IsNullOrEmpty(strLoadEvent))
    //            {
    //                DataTable dtName = new DataTable(strDsName);
    //                try
    //                {
    //                    if (strLoadEvent == "onapp")
    //                    {
    //                        dtName.Merge(objHelper.ExecuteDataSetSqlInline(strSqlQuery).Tables[0]);
    //                        SaveInRedisServerDT(strDsName, dtName, "");
    //                    }
    //                }
    //                catch (Exception ex)
    //                {
    //                    ClearRedisServerDataByKey(strDsName, "", false);

    //                    if (HttpContext.Current != null && HttpContext.Current.Session != null)
    //                        logObj.CreateLog("Fastdata datatables- DsName-" + strDsName + "-exception-" + ex.Message, HttpContext.Current.Session.SessionID, "CreateFdDataset", "new");
    //                    else
    //                        logObj.CreateLog("Fastdata datatables- DsName-" + strDsName + "-exception-" + ex.Message, "Fast data dataset on application start", "CreateFdDataset", "new");
    //                }
    //            }
    //            else
    //            {
    //                if (HttpContext.Current != null && HttpContext.Current.Session != null)
    //                    logObj.CreateLog("Fastdata datatables- DsName-" + strDsName + "-SqlQuery-" + strSqlQuery + "-LoadEvent-" + strLoadEvent, HttpContext.Current.Session.SessionID, "CreateFdDataset", "new");
    //                else
    //                    logObj.CreateLog("Fastdata datatables- DsName-" + strDsName + "-SqlQuery-" + strSqlQuery + "-LoadEvent-" + strLoadEvent, "Fast data dataset on application start", "CreateFdDataset", "new");
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        if (HttpContext.Current != null && HttpContext.Current.Session != null)
    //            logObj.CreateLog("Exception in CreateFdDataset-" + ex.Message, HttpContext.Current.Session.SessionID, "CreateFdDataset", "new");
    //        else
    //            logObj.CreateLog("Exception in CreateFdDataset-" + ex.Message, "Fast data dataset on application start", "CreateFdDataset", "new");
    //    }
    //}

    public bool SaveInRedisServerDT(string key, object value, string type, string schemaName = "")
    {
        bool added = false;
        string jsonstr = JsonConvert.SerializeObject(value);
        if (!string.IsNullOrEmpty(jsonstr))
        {
            byte[] bytes;
            try
            {
                using (var stream = new MemoryStream())
                {
                    new BinaryFormatter().Serialize(stream, jsonstr);
                    bytes = stream.ToArray();
                }
                IDatabase cacheClient = redis.GetDatabase();
                lock (cacheClient)
                {
                    if (redis.IsConnected)
                    {
                        key = schemaName == "" ? schemaNameKey + '-' + key : schemaName + '-' + key;
                        added = cacheClient.StringSet(key, bytes);
                    }
                }
            }
            catch (Exception ex)
            {
                logObj.CreateLog("Redis Server Functon(SaveInRedisServer), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
            }
        }
        return added;
    }

    public bool SaveInRedisServer(string key, object value, string type, string schemaName = "")
    {
        bool added = false;
        byte[] bytes;
        try
        {
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, value);
                bytes = stream.ToArray();
            }
            IDatabase cacheClient = redis.GetDatabase();
            lock (cacheClient)
            {
                if (redis.IsConnected)
                {
                    key = schemaName == "" ? schemaNameKey + '-' + key : schemaName + '-' + key;
                    added = cacheClient.StringSet(key, bytes);
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(SaveInRedisServer), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return added;
    }

    public bool ClearRedisServerDataByKey(string key, string type, bool schemaexist, string schemaName = "")
    {
        bool removed = false;
        if (schemaNameKey == string.Empty)
            return removed;
        try
        {
            IDatabase cacheClient = redis.GetDatabase();
            if (redis.IsConnected)
            {
                string keyname = string.Empty;
                if (!schemaexist)
                    keyname = schemaName == "" ? schemaNameKey + '-' + key : schemaName + '-' + key;
                else
                    keyname = key;

                string result = string.Empty;
                if (cacheClient.KeyExists(keyname))
                    removed = cacheClient.KeyDelete(keyname);
            }
        }
        catch (Exception ex)
        {
            removed = false;
            logObj.CreateLog("Redis Server Functon(ClearRedisServerDataByKey), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return removed;
    }


    public bool ParseString(string str)
    {
        Int32 intValue;
        Int64 bigintValue;
        Double doubleValue;
        Decimal decimalValue;
        DateTime dateValue;

        if (Int32.TryParse(str, out intValue))
            return false;
        else if (Int64.TryParse(str, out bigintValue))
            return false;
        else if (Double.TryParse(str, out doubleValue))
            return false;
        else if (Decimal.TryParse(str, out decimalValue))
            return false;
        else if (DateTime.TryParse(str, out dateValue))
            return true;
        else return true;

    }

    public DataSet DSasJsonFromRedis(string key, string schemaName = "")
    {
        DataSet result = null;
        if (schemaNameKey == string.Empty)
        {
            return result;
        }
        byte[] bytes = null;
        try
        {

            IDatabase cacheClient = redis.GetDatabase();
            if (redis.IsConnected)
            {
                key = schemaName == "" ? schemaNameKey + '-' + key : schemaName + '-' + key;
                bytes = (byte[])cacheClient.StringGet(key);
            }
            if (bytes != null)
            {
                using (var stream = new MemoryStream(bytes))
                {
                    var str = (object)new BinaryFormatter().Deserialize(stream);
                    result = JsonConvert.DeserializeObject<DataSet>(str.ToString());
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(ObjectJsonFromRedisDT), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return result;
    }
    public DataSet DataSetFromRedis(string key)
    {
        DataSet result = null;
        if (schemaNameKey == string.Empty)
            return result;

        byte[] bytes = null;
        try
        {
            IDatabase cacheClient = redis.GetDatabase();
            if (redis.IsConnected)
            {
                key = schemaNameKey + '-' + key;
                bytes = cacheClient.StringGet(key);
            }
            if (bytes != null)
            {
                using (var stream = new MemoryStream(bytes))
                {
                    result = (DataSet)new BinaryFormatter().Deserialize(stream);
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(DataSetFromRedis), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return result;
    }

    private string GetSessionId()
    {
        try
        {
            if (HttpContext.Current.Session != null)
            {
                return HttpContext.Current.Session.SessionID.ToString();
            }
            else
            {
                return Constants.GeneralLog;
            }
        }
        catch (Exception)
        {
            return Constants.GeneralLog;
        }
    }

    public void CreateFDThread()
    {
        if (FDWCreateDef == 0)
        {
            //TODO: Thread to run the Pop Service for Refresh
            Thread popTh = new Thread(PopRefreshDs);
            popTh.Start();
        }
        //logObj.CreateLog("Popping thread started", GetSessionId(), "LogThreadPath", "new", "true");
    }

    public void PopRefreshDs()
    {
        if (dsRefresh.Count > 0)
        {
            while (true)
            {
                for (int i = 0; i < dsRefresh.Count; i++)
                {
                    FDWRefresh = FDWRefresh + 1;
                    var curDsName = dsRefresh[i].ToString();
                    //Pop the record
                    PopInDsRefresh(curDsName);
                    //logObj.CreateLog("POP Started----------In PopRefreshDs, Currently Popping-" + curDsName, "7777", "LogThreadPath", "", "true");
                    //Call Refresh for the removed record
                    RefreshFDDataSet(curDsName);
                    //logObj.CreateLog("POP Completed----------In PopRefreshDs, Currently Popping-" + curDsName, "7777", "LogThreadPath", "", "true");
                }
            }
        }
    }

    public void PushToDsRefresh(string KeyName)
    {
        lock (dsRefresh)
        {
            int idx = dsRefresh.IndexOf(KeyName);
            if (idx == -1)
                dsRefresh.Add(KeyName);
        }
    }

    public void PopInDsRefresh(string KeyName)
    {
        lock (dsRefresh)
        {
            int idx = dsRefresh.IndexOf(KeyName);
            dsRefresh.RemoveAt(idx);
        }
    }

    public DataTable DataTableFromRedis(string key)
    {
        DataTable result = null;
        if (schemaNameKey == string.Empty)
        {
            return result;
        }
        byte[] bytes = null;
        try
        {
            IDatabase cacheClient = redis.GetDatabase();
            if (redis.IsConnected)
            {
                key = schemaNameKey + '-' + key;
                bytes = (byte[])cacheClient.StringGet(key);
            }

            if (bytes != null)
            {
                using (var stream = new MemoryStream(bytes))
                {
                    result = (DataTable)new BinaryFormatter().Deserialize(stream);
                }
            }

        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(DataTableFromRedis), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return result;
    }

    public void LogFDWDetails()
    {
        string logStr = string.Empty;
        logStr = "No of connections made in FDW-" + FDWConCount + "----- No of Definitions created in redis-" + FDWCreateDef
            + "----- No of times Refresh DS is called-" + FDWRefresh;
        logObj.CreateLog(logStr, GetSessionId(), "LogFDW", "new", "true");
    }

    public bool PushPopInRedisServer(string key, object value, string type)
    {
        bool added = false;
        if (schemaNameKey == string.Empty)
        {
            return added;
        }
        byte[] bytes;
        try
        {
            using (var stream = new MemoryStream())
            {
                new BinaryFormatter().Serialize(stream, value);
                bytes = stream.ToArray();
            }

            IDatabase cacheClient = redis.GetDatabase();
            lock (cacheClient)
            {
                if (redis.IsConnected)
                {
                    key = schemaNameKey + '-' + key;
                    added = cacheClient.StringSet(key, bytes);
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(SaveInRedisServer), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
            //throw ex;
        }
        return added;
    }

    public void RefreshFDDataSet(string dsName)
    {
        //logObj.CreateLog("Refresh Started----------In RefreshFDDataSet, Currently Refreshing-" + dsName, GetSessionId(), "LogThreadPath", "", "true");
        var memFastDataDef = DSasJsonFromRedis(Constants.RS_FD_DEFINITION);
        if (memFastDataDef != null)
        {
            var dsDef = (DataSet)memFastDataDef;
            try
            {
                //logObj.CreateLog("Refreshing ds-" + dsName, HttpContext.Current.Session.SessionID, "RefreshFastDS", "");
                DataTable dsData = new DataTable();
                var dsQuery = dsDef.Tables[Constants.FD_DT_DEFINITION].AsEnumerable().Where(s => s.Field<string>("Datasetname") == dsName).Select(r => new { sqlText = r.Field<string>("sqltext"), sqlparam = r.Field<string>("Params") }).First();
                Ihelper objHelper = new Helper().SetDatabase(db_type, db_connection);
                dsData.Merge(objHelper.ExecuteDataSetSqlInline(dsQuery.sqlText).Tables[0]);
                SaveInRedisServerDT(dsName, dsData, "");
                //logObj.CreateLog("Refresh Completed----------In RefreshFDDataSet, Currently Refreshing-" + dsName, GetSessionId(), "LogThreadPath", "", "true");
            }
            catch (Exception ex)
            {
                logObj.CreateLog("Exception in RefreshFastDataset-" + ex.Message, GetSessionId(), "RefreshFastDS", "new", "true");
            }
        }
    }

    public DataSet GetDefinition(string schemaName = "")
    {
        DataSet DfDataset = new DataSet();
        try
        {
            Ihelper objHelper = new Helper().SetDatabase(db_type, db_connection);
            DfDataset.Tables.Add(Constants.FD_DT_DEFINITION);
            DfDataset.Tables[Constants.FD_DT_DEFINITION].Merge(objHelper.ExecuteDataSetSqlInline(Constants.FD_TBL_DEFINITION).Tables[0]);
            DfDataset.Tables.Add(Constants.FD_DT_ASSOCIATION);
            DfDataset.Tables[Constants.FD_DT_ASSOCIATION].Merge(objHelper.ExecuteDataSetSqlInline(Constants.FD_TBL_ASSOCIATION).Tables[0]);
            SaveInRedisServerDT(Constants.RS_FD_DEFINITION, DfDataset, "", schemaName);
        }
        catch (Exception ex)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                logObj.CreateLog("Exception in GetDefinition-" + ex.Message, HttpContext.Current.Session.SessionID, "GetDefinition", "new");
            else
                logObj.CreateLog("Exception in GetDefinition-" + ex.Message, "Fast data on application start", "GetDefinition", "new");
        }
        return DfDataset;
    }

    public void GetAxRelations(string schemaName = "")
    {
        DataTable arDatatable = new DataTable();
        try
        {
            Ihelper objHelper = new Helper().SetDatabase(db_type, db_connection);
            arDatatable = objHelper.ExecuteDataSetSqlInline(Constants.TBL_AXRELATIONS).Tables[0];
            SaveInRedisServer(Constants.RS_AXRELATIONS, arDatatable, "", schemaName);
        }
        catch (Exception ex)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
                logObj.CreateLog("Exception in GetAxRelations-" + ex.Message, HttpContext.Current.Session.SessionID, "GetAxRelations", "new");
            else
                logObj.CreateLog("Exception in GetAxRelations-" + ex.Message, "AxRelations data on application start", "GetAxRelations", "new");
        }
    }

    public void GetDBConnection(string strProj, string contents)
    {
        DataSet dataSet = new DataSet();
        DataTable dataTable = new DataTable(strProj);
        dataTable.Columns.Add("type", typeof(string));
        dataTable.Columns.Add("db", typeof(string));
        dataTable.Columns.Add("version", typeof(string));
        dataTable.Columns.Add("driver", typeof(string));
        dataTable.Columns.Add("dbcon", typeof(string));
        dataTable.Columns.Add("dbuser", typeof(string));
        dataTable.Columns.Add("structurl", typeof(string));
        dataTable.Columns.Add("dataurl", typeof(string));
        dataTable.Columns.Add("pwd", typeof(string));
        dataSet.Tables.Add(dataTable);
        System.IO.StringReader xmlSR = new System.IO.StringReader(contents);
        dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);
        string dbuser = dataSet.Tables[0].Rows[0]["dbuser"].ToString();
        string dbtype = dataSet.Tables[0].Rows[0]["db"].ToString();
        string pwd = dataSet.Tables[0].Rows[0]["pwd"].ToString();
        string Server = dataSet.Tables[0].Rows[0]["dbcon"].ToString();
        string database = dataSet.Tables[0].Rows[0]["dbuser"].ToString();
        string cs = "";
        Util.Util util = new Util.Util();
        pwd = util.GetDBPassword(database);

        if (dbtype.ToLower() == "ms sql")
        {
            cs = @"Server = " + Server + "; Database= " + database + "; User Id = " + dbuser + "; Password = " + pwd + ";";
        }
        if (dbtype.ToLower() == "oracle")
        {
            cs = @"Data Source=" + Server + ";User Id=" + dbuser + ";Password=" + pwd + ";Pooling=False;";
        }
        if (dbtype.ToLower() == "mysql" || dbtype.ToLower() == "mariadb")
        {
            cs = @"Server=" + Server + ";Database=" + database + ";Uid=" + dbuser + ";Pwd=" + pwd + ";";
        }
        if (dbtype.ToLower() == "postgresql"|| db_type.ToLower() == "postgre")
        {
            if (dbuser.Contains("\\") || database.Contains("\\"))
            {
                string[] userDtls = dbuser.Split('\\');
                string[] databaseDtls = database.Split('\\');
                if ((userDtls.Length > 1 && userDtls[1] != "") && (databaseDtls.Length > 1 && databaseDtls[1] != ""))
                {
                    dbuser = userDtls[1];
                    database = databaseDtls[1];
                }
                else
                {
                    dbuser = userDtls[0];
                    database = databaseDtls[0];
                }
            }
            else
            {
                database = "axpertdb";

            }
            cs = @"Server=" + Server + ";Database=" + database + ";Uid=" + dbuser + ";Pwd=" + pwd + ";";
        }

        db_type = dbtype;
        db_connection = cs;
        schemaNameKey = dbuser;
    }

    public bool IsNullOrEmpty(String value)
    {
        return (value == null || value.Length == 0);
    }

    #region FastDataUtility
    public bool DeleteAllKeys(string key)
    {
        bool removed = false;
        if (schemaNameKey == string.Empty)
        {
            return removed;
        }
        try
        {

            IDatabase cacheClient = redis.GetDatabase();
            if (redis.IsConnected)
            {
                string[] multikeys = key.Split('&');
                for (int i = 0; i < multikeys.Length; i++)
                {
                    if (cacheClient.KeyExists(multikeys[i]))
                    {
                        removed = cacheClient.KeyDelete(multikeys[i]);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Functon(DeleteKey), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }
        return removed;
    }
    #endregion

    public bool FlushRedisKeysByRoles(ArrayList roles)
    {
        bool flush = false;
        try
        {
            if (redis.IsConnected)
            {
                var endpoints = redis.GetEndPoints(true);
                IDatabase cacheClient = redis.GetDatabase();
                foreach (var endpoint in endpoints)
                {
                    var server = redis.GetServer(endpoint);
                    foreach (var role in roles)
                    {
                        var roleKeys = server.Keys(pattern: "*-" + role + "-*");
                        var defaultRoleKeys = server.Keys(pattern: role + "-*");
                        var serverKeys = roleKeys.Concat(defaultRoleKeys);
                        foreach (string key in serverKeys)
                        {
                            if (key.Contains(schemaNameKey) && cacheClient.KeyExists(key))
                                flush = cacheClient.KeyDelete(key);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            logObj.CreateLog("Redis Server Function(FlushAllRedisKeys), Message:" + ex.Message, GetSessionId(), "RedisServer", "new");
        }

        return flush;
    }

}
