﻿
$j(document).ready(function () {
    modalHeader = eval(callParent("divModalHeader", "id") + ".getElementById('divModalHeader')");
    modalHeader.innerText = eval(callParent('lcm[359]'));
    $("#cmdSend").prop("title", eval(callParent('lcm[360]'))).val(eval(callParent('lcm[360]')));
    $("#close").prop("title", eval(callParent('lcm[249]'))).val(eval(callParent('lcm[249]')));
    $('#filMyFile').bind({
        change: function () {
            var existingFlCount = parent.$("#" + $j("#hdnAttFld").val() + "attach").parent().find(".attachment-count span").text();// 0;//$j(this, doParentOpInIframe("document", "rtn", this))[0].value;
            existingFlCount = existingFlCount == "" ? 0 : existingFlCount;
            var FileUploadlmt = eval(callParent('AxpFileUploadlmt'));
            var uploadFileCount = $('#filMyFile')[0].files.length;
            var totFlCount = parseInt(existingFlCount) + uploadFileCount;
            if (FileUploadlmt != undefined && FileUploadlmt != 0 && (uploadFileCount > FileUploadlmt || totFlCount > FileUploadlmt)) {
                $("#filMyFile").val('');
                $('#fileuploadsts').val('');
                $("#fileuploadsts").text("[Files can upload " + FileUploadlmt + " only.]");
                return;
            }
            var filename = $("#filMyFile").val();
            if (filename.indexOf("+") > -1) {
                $("#fileuploadsts").text("[FileName should not contain '+' character.]");
            }
            if (/^\s*$/.test(filename)) {
                $(".file-upload").removeClass('active');
                var cutMsg = eval(callParent('lcm[66]'));
                $("#noFile").text(cutMsg);
            }
            else {
                $(".file-upload").addClass('active');
                $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
            }
            var uploadControl = $('#filMyFile')[0].files;
            if (uploadControl.length > 0)
                $("#lblnofilename").text(uploadControl[0].name);
            
            if (ValidateFileExtension(filename, "file") == true) {
                $('#fileuploadsts').val();
                $('#cmdSend').prop('disabled', false);
                $(".file-upload").addClass('active');
                $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
            }
            else {
                $("#fileuploadsts").text("[" + eval(callParent('lcm[305]')) + "]");
                $('#cmdSend').prop('disabled', true);
                ResetFileUploadProperties();
            }
        }
    });
    // ChangeTheme();
    checkSuccessAxpertMsg();
    setTimeout(function () { IFrameModalDialogTabEvents("file-select"); }, 500);
    closeRemodalPopupOnEsc();
    validateFilesize();
});

function DoClientFunction() {
    var fn = $j("#fname");
    var filename = escape(fn.val()); //encode file name
    filename = filename.replace(/%2C/g, ","); //ignoring ',' char while encoding(because we are separating multiple files by comma)
    var upl = $j("#upsts");
    var succ = upl.val();
    var fldId = "";
    var act = $j("#hdnAttFld");
    var hdnFilePath = $j("#hdnFilePath");
    if (succ == "Uploaded Successfully") {
        eval(callParent('isGrdEditDirty') + "= true");
        var id = act.val();
        var rowFrmNo = id.substring(id.lastIndexOf("F") - 3);
        var rowNo = rowFrmNo.substring(0, rowFrmNo.lastIndexOf("F"));
        var frmNo = rowFrmNo.substring(rowFrmNo.lastIndexOf("F") + 1);
        var hlFld = $j("#" + id, doParentOpInIframe("document", "rtn", "#" + id));
        var fldsByClass = doParentOpInIframe("document.getElementsByClassName('grdAttach')", "rtn", ".grdAttach");
        var tmpFldid = "";
        var hasReferFiles = false;
        for (var i = 0; i < fldsByClass.length; i++) {
            tmpFldid = fldsByClass[i].getAttribute('id');
            if ((tmpFldid == 'dc' + frmNo + '_image' + rowFrmNo) || (tmpFldid == 'axp_gridattach_' + frmNo + rowFrmNo)) {
                fldId = tmpFldid;
            }
        }

        var existingFiles = $j('#' + fldId, doParentOpInIframe("document", "rtn", "#" + fldId))[0].value;
        if (existingFiles != undefined) {
            if (existingFiles != "") {
                if (existingFiles.indexOf("@") != -1) {
                    hasReferFiles = true;
                    existingFiles = GetReferedFiles(existingFiles);
                }
                filename = existingFiles + "," + filename;

                $j("#" + fldId, doParentOpInIframe("document", "rtn", "#" + fldId)).val("");
                $j("#GridAttach" + rowFrmNo, doParentOpInIframe("document", "rtn", "#GridAttach" + rowFrmNo)).html("");
            }

        }

        var fileLinks = "";
        //if(!callParentNew("axInlineGridEdit")){
        //    fileLinks = "<span id=\"grdAtt_img_" + rowFrmNo + "\" class=\"fa fa-paperclip\" onclick=\"ShowGridAttachPopUp(this);\" class=\"grdAttach handCur\" />";
        //}
        var bMultipleFiles = false;
        var totalFiles = 1;
        if (filename.indexOf(',') > -1) {
            bMultipleFiles = true;
            arrFileNames = filename.split(',');
            totalFiles = arrFileNames.length;
            hlFld.innerHTML = "";
            var referfilename = "";
            for (var j = 0; j < arrFileNames.length; j++) {
                var unescapedChar = unescape(arrFileNames[j]);
                if (hasReferFiles && window.localStorage.refFiles != undefined) {
                    if ((JSON.parse(window.localStorage.refFiles)).indexOf(arrFileNames[j]) == -1)
                        fileLinks += "<div id=\"Link_" + rowNo + "_" + arrFileNames[j] + "\"><a onclick=\"DeleteFileFromRow('" + fldId + "','" + rowNo + "','" + arrFileNames[j] + "')\"><img src=\"../AxpImages/icons/16x16/delete.png\" /></a><a href=\"javascript:void(0)\" id='grdAtt_hlnk_" + rowFrmNo + "' class='grdAttach handCur' onclick='ShowGridAttLink(\"" + hdnFilePath.val() + arrFileNames[j] + "\")'>" + unescapedChar + "</a></div>";
                    else {
                        fileLinks += "<div id=\"Link" + arrFileNames[j] + "\"></a><a id='grdAtt_hlnk_" + rowFrmNo + "' class='grdAttach handCur' onclick='ShowGridAttLink(\"" + hdnFilePath.val() + (JSON.parse(window.localStorage.referRecId)) + "-" + arrFileNames[j] + "\")'>" + unescapedChar + "</a></div>";
                        if (referfilename == "")
                            referfilename = arrFileNames[j].toString();
                        else
                            referfilename += "," + arrFileNames[j].toString();
                    }
                }
                else {
                    fileLinks += "<div id=\"Link_" + rowNo + "_" + arrFileNames[j] + "\"><a onclick=\"DeleteFileFromRow('" + fldId + "','" + rowNo + "','" + arrFileNames[j] + "')\"><img src=\"../AxpImages/icons/16x16/delete.png\" /></a><a href=\"javascript:void(0)\" id='grdAtt_hlnk_" + rowFrmNo + "' class='grdAttach handCur' onclick='ShowGridAttLink(\"" + hdnFilePath.val() + arrFileNames[j] + "\")'>" + unescapedChar + "</a></div>";
                }

            }

            if (referfilename != "")
                filename = filename.replace(referfilename, (JSON.parse(window.localStorage.fldExp)));

        }
        else
            fileLinks += "<div id=\"Link_" + rowNo + "_" + filename + "\"><a onclick=\"DeleteFileFromRow('" + fldId + "','" + rowNo + "','" + filename + "')\"><img src=\"../AxpImages/icons/16x16/delete.png\" /></a><a href=\"javascript:void(0)\" id='grdAtt_hlnk_'" + rowFrmNo + " class='grdAttach handCur' onclick='ShowGridAttLink(\"" + hdnFilePath.val() + filename + "\")'>" + unescape(filename) + "</a></div>";


        filename = unescape(filename); //decode file name

        $j("#" + fldId, doParentOpInIframe("document", "rtn", "#" + fldId)).val(filename);
        UpdateArray(fldId, filename);


        if ($j("#axpattach_filename" + frmNo + rowFrmNo, doParentOpInIframe("document", "rtn", "#axpattach_filename" + frmNo + rowFrmNo)).length > 0) {
            filename = $j("#axpattach_filename" + frmNo + rowFrmNo, doParentOpInIframe("document", "rtn", "#axpattach_filename" + frmNo + rowFrmNo)).val();

            $j("#axpattach_filename" + frmNo + rowFrmNo, doParentOpInIframe("document", "rtn", "#axpattach_filename" + frmNo + rowFrmNo)).val(filename);
            UpdateArray("axpattach_filename" + frmNo + rowFrmNo, filename);

        }
        if ($j("#hdnType").val() != "") {
            if (filename.lastIndexOf(".") == -1) {
                filename = filename + $j("#hdnType").val();
            }
        }
        //if(!callParentNew("axInlineGridEdit"))
         //   $j("#GridAttach" + rowFrmNo, doParentOpInIframe("document", "rtn", "#GridAttach" + rowFrmNo)).html(fileLinks);
        //else {
            var labelHtml = "<span id=\"grdAtt_img_" + rowFrmNo + "\" tabindex='0' class=\"fa fa-paperclip upload-icon\" onclick=\"ShowGridAttachPopUp(this);\" class=\"grdAttach handCur\" />";
            labelHtml += "<div id='grdAtt_hlnk_" +  rowFrmNo + "attach' class='attach-files' style='display:none'>" + fileLinks + "</div>";
            labelHtml += "<a class='pull-" + (callParentNew("gllangType", "gllangType") === "ar" ? "left" : "right") + " attachment-count' href='javascript:void(0)' class=''><span data-target='grdAtt_hlnk_" +  rowFrmNo + "attach' class='badge attach-popover'>" + totalFiles + "</span></a>";
            $j("#GridAttach" + rowFrmNo, doParentOpInIframe("document", "rtn", "#GridAttach" + rowFrmNo)).html(labelHtml);
            callParentNew("showAttachmentPopover();", "function");
            callParentNew("assignInlineGridEditEvent(" + frmNo + ")", "function");
        //}
        setTimeout(function () { closeUploadDialog(); }, 300);
    }
    else {
        closeUploadDialog();
    }
}

// Get Referred Filenames
function GetReferedFiles(fldValue) {

    var attachedFiles = "";
    var TempfldValues = fldValue;
    var OIndx = TempfldValues.indexOf('(');
    var CommaIndx = TempfldValues.indexOf(',');
    var fileNameSIndx = OIndx + 1;
    var fileNameEIndx = TempfldValues.indexOf(")") - 1;
    referRecId = TempfldValues.substring(CommaIndx + 1, fileNameSIndx - 1);
    var referFiles = TempfldValues.substr(fileNameSIndx, fileNameEIndx - fileNameSIndx + 1);
    var attachedFiles = TempfldValues.substring(fileNameEIndx + 3);
    if (attachedFiles != "")
        return (referFiles + "," + attachedFiles);
    else
        return referFiles;
}

function UpdateArray(fldName, fldValue) {

    var isAlreadyFound = false;
    for (var x = 0; x < doParentOpInIframe("ChangedFields", "rtn").length; x++) {

        var fName = doParentOpInIframe("ChangedFields[" + x + "]", "rtn").toString();
        if (fName == fldName) {
            if (fldValue == "***") {
                doParentOpInIframe("ChangedFields", "rtn").splice(x, 1);
                doParentOpInIframe("ChangedFieldDbRowNo", "rtn").splice(x, 1);
                doParentOpInIframe("ChangedFieldValues", "rtn").splice(x, 1);
                doParentOpInIframe().ChangedFieldOldValues.splice(x, 1);
            }
            else {
                //doParentOpInIframe("ChangedFieldOldValues[" + x + "]", "rtn") = doParentOpInIframe("ChangedFieldValues[" + x + "]", "rtn").toString();
                doParentOpInIframe("ChangedFieldOldValues[" + x + "]", "var", doParentOpInIframe("ChangedFieldValues[" + x + "]", "rtn").toString());
                doParentOpInIframe("ChangedFieldDbRowNo[" + x + "]", "var", doParentOpInIframe("ChangedFieldDbRowNo[" + x + "]", "rtn"));
                doParentOpInIframe("ChangedFieldValues[" + x + "]", "var", fldValue);
            }
            isAlreadyFound = true; // the field name is already found and updated.
            break;
        }
    }

    if ((!isAlreadyFound) && (fldValue != "***")) {
        var fIndx = fldName.lastIndexOf("F");
        var rowNo = fldName.substring(fIndx - 3, fIndx);
        var dcNo = fldName.substring(fIndx + 1);
        var dbRowNo = parseInt(rowNo, 10);
        parent.ChangedFields.push(fldName);
        parent.ChangedFieldDbRowNo.push(dbRowNo);
        parent.ChangedFieldValues.push(fldValue);
        parent.ChangedFieldOldValues.push("");
    }
}
function ChangeTheme() {
    //var theme = $j("#DropDownList1 option:selected", window.parent.document).text();

    theme = eval(callParent('currentThemeColor'));

    if (theme != "") {
        $j("#themecss").attr('href', "../App_Themes/" + theme + "/Stylesheet.min.css?v=23");
    }
    else {
        var themeref = "";
        if (parent) {
            themeref = $j("#themecss", parent.document).attr("href");
            if (themeref != "") {
                $j("#themecss").attr("href", themeref);
            }
        }
        if (parent.parent.document) {
            themeref = $j("#themecss", parent.parent.document).attr("href");
            if (themeref != "") {
                $j("#themecss").attr("href", themeref);
            }
        }
        if (themeref == "" || themeref == undefined) {
            themeref = "../App_Themes/" + axTheme + "/Stylesheet.min.css?v=23";
            $j("#themecss").attr("href", themeref);
        }
    }
}
$(document).keydown(function (e) {
    if (e.which == 27) {
        if ($(parent.$('.modal')) != undefined && $(parent.$('.modal')).length > 0) {
            $(parent).focus();
            if ($(parent.$('.modal .close')) != undefined && $(parent.$('.modal .close')).length > 0) {
                $(parent.$('.modal .close'))[$(parent.$('.modal .close')).length - 1].click();
            }
            setTimeout(function () {
                $(parent.$('.modal'))[$(parent.$('.modal')).length - 1].remove();
            }, 300);
        }
    }
})

function closeUploadDialog() {
    if ($(parent.$('.modal')) != undefined && $(parent.$('.modal')).length > 0) {
        $(parent).focus();
        if ($(parent.$('.modal .close')) != undefined && $(parent.$('.modal .close')).length > 0) {
            $(parent.$('.modal .close'))[$(parent.$('.modal .close')).length - 1].click();
        }
        setTimeout(function () {
            $(parent.$('.modal'))[$(parent.$('.modal')).length - 1].remove();
        }, 300);
    }
}

function AllowAttachements() {
    var dcNo = $("#hdnDcNo").val();
    var aStrings = "";
    if (callParentNew("axInlineGridEdit"))
        aStrings = window.parent.$("#divDc" + dcNo + " [id^=grdAtt_hlnk_]").find(".grdAttach").text();
    else
        aStrings = window.parent.$("#divDc" + dcNo).find("textarea[data-type=gridattach]").text();
    if (aStrings != null && aStrings != "") {
        var myFile = $('#filMyFile').val();
        if (aStrings.indexOf(myFile.substr(12)) == -1) {
            if (myFile != null && myFile != "")
                return true;
            else
                return false;
        }
        else {
            if (myFile != null && myFile != "") {
                $('#fileuploadsts').text("File Already Exists");
                $('#fileuploadsts').css('color', 'RED');
            }
            return false;
        }
    }
    else {
        return true;
    }
}
