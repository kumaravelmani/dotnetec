﻿$j(document).ready(function () {
    //Code to set the focus in Forgot Password Page
    if ($j('#axSelectProj').is(':visible') && $j('#axSelectProj').val() == "")
        $j('#axSelectProj').focus();
    else
        $j("#txtName").focus();
});

function AddProjs()
{
    var axProj = $j("#hdnProj").val();
    var axProjs = new Array();
    axProjs = axProj.split(",");

    if (axProjs.length < 2) {
        $j("#selectProj").hide();
    }
    else if (axProjs.length == 2) {
        $j('#axSelectProj').append('<option selected="selected" value="' + axProjs[1] + '">' + axProjs[1] + '</option>');
        $j("#selectProj").hide();
    }
    else {
        for (k in axProjs) {
            $j('#axSelectProj').append('<option value="' + axProjs[k] + '">' + axProjs[k] + '</option>');
        }

        var selectedProj = $j('#axSelectProj :selected').val();

        var regex = new RegExp("^[a-zA-Z0-9]+$");
        if (regex.test(selectedProj)) {
            if (selectedProj)
                $j('#hdnProj').val(selectedProj);
        }

    }
}

function SetProj()
{
    try {
        $j("#hdnProj").val($j('#axSelectProj :selected').val());
    }
    catch (ex)
    { }
}


function OpenSigninPage() {
    var projSelected = $j('#axSelectProj').val();
    window.document.location.href = "Signin.aspx?proj=" + projSelected + "";
}