﻿$(document).ready(function () {
   
    $("#commentSave").on("click", function (e) {
        ShowDimmer(true);
        $("#hdnComment").val($("#comment").val());
    })
});


$(document).on("click","#submit", function (e) {
    ShowDimmer(true);
    var password = $("#password").val();
    var hash = MD5(password);
    var a = "1234";
    var newhash = MD5(a + hash);
    $("#hdnPwd").val(newhash);
});


function createModal() {
    $('#workflowComments').modal('show');
}

function ShowDimmer(status) {
    DimmerCalled = true;
    var dv = $("#waitDiv");

    if (dv.length > 0 && dv != undefined) {
        if (status == true) {

            var currentfr = $("#middle1", parent.document);
            if (currentfr) {
                dv.width(currentfr.width());
            }
            dv.show();
            document.onkeydown = function EatKeyPress() { return false; }
        }
        else {
            dv.hide();
            document.onkeydown = function EatKeyPress() { if (DimmerCalled == true) { return true; } }
        }
    }
    else {
        //TODO:Needs to be tested
        if (window.opener != undefined) {

            dv = $("#waitDiv", window.opener.document);
            if (dv.length > 0) {
                if (status == true)
                    dv.show();
                else
                    dv.hide();
            }
        }
    }
    DimmerCalled = false;
}