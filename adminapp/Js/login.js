﻿var arrProjects = new Array();
var arrLangs = new Array();
var appUrl = "";
var errField = "";

function GetSchemaDetails() {
    var regId = document.getElementById("AxRegId").value;
    try {
        ASB.WebService.GetSchemas(regId, SucceededCallback);
    }
    catch (exp) {

    }
}

function SucceededCallback(result, eventArgs) {
    //parse the result and get the schemas
    //The schema name and alias name will be in the format schemaname^aliasname~schemaname^aliasname
    //TODO: if no schemas defined display message :- No projects published.
    var strData = result.split("~");
    var ddlSchema = $j("#AxSchema");
    if (strData.length == 1 && strData[0] == "") return;
    ddlSchema.empty();
    for (var i = 0; i < strData.length; i++) {
        if (strData[i] != "") {
            var strSchemas = strData[i].split("^");
            var optText = "";
            optText = "<option value='" + strSchemas[0] + "'>" + strSchemas[1] + "</option>";
            ddlSchema.append(optText);
        }
    }

    if (strData.length == 1) ddlSchema[0].selectedIndex = 0;
    $j("#hdnApp").val(ddlSchema.find("option:selected").text());
}

// JScript File
function chkLoginForm() {
    if ($j('#axSelectProj').is(':visible') && !ValidateProject()) { return false; }
    if ($j('#language').is(':visible') && !ValidateLanguage()) { return false; }

    var valProj = $j('#axSelectProj').is(':visible') && !$j('#axSelectProj').val() || $j('#axSelectProj').val().toLowerCase() == "select project";
    var valLang = $j('#language').is(':visible') && !$j('#language').val();
    if (valProj) {
        showAlertDialog("error", 1008, "client");
        $j("#axSelectProj").focus();
        errField = 'axSelectProj';
        return false;
    }
    else if (!usercheck($j("#Text1").val())) {
        return false;
    }
    else if (valLang) {
        showAlertDialog("error", 1009, "client");
        $j("#language").focus();
        errField = 'language';
        return false;
    }
    else {
        return md5auth();
    }
}

function SetFocus() {
    if ($j('#axSelectProj').is(':visible') && $j('#axSelectProj').val() == "")
        $j('#axSelectProj').focus();
    else
        $j("#Text1").focus();
}

function usercheck(a) {
    var userTxt = $j("#Text1");
    if (a == "") {
        showAlertDialog("error", 1010, "client");
        userTxt.val("");
        userTxt.focus();
        errField = 'Text1';
        return false;
    }
    else if (!alphanumeric(a)) {
        showAlertDialog("error", 1013, "client");
        userTxt.val("");
        userTxt.focus();
        errField = 'Text1';
        return false;
    }
    else if ($j("#sw").val() == "") {
        showAlertDialog("error", 1011, "client");
        $j("#sw").focus();
        errField = 'sw';
        return false;
    }
    else {
        return true;
    }
}

function alphanumeric(alphane) {
    var numaric = alphane;
    for (var j = 0; j < numaric.length; j++) {
        var alphaa = numaric.charAt(j);
        var hh = alphaa.charCodeAt(0);
        if ((hh > 47 && hh < 58) || (hh > 64 && hh < 91) || (hh > 96 && hh < 123) || alphaa == '_' || alphaa == '-' || alphaa == '.' || alphaa == '@') { }
        else {
            return false;
        }
    }
    return true;
}

function IsValidEmail(Email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(Email);
}


function ForgotPwd() {
    if ($j('#axSelectProj').is(':visible') && !ValidateProject()) { return false; };
    var un = $j("#txtName").val();
    var hdnproj = $j('#axSelectProj').val();
    var valproj = $j('#axSelectProj').is(':visible') && !$j('#axSelectProj').val() || $j('#axSelectProj').val().toLowerCase() == "select project";
    if (valproj) {
        showAlertDialog("error", 1008, "client");
        $j("#axSelectProj").focus();
        errField = 'axSelectProj';
        return false;
    }

    if (!un) {
        showAlertDialog("error", 1010, "client");
        $j("#txtName").focus();
        errField = 'txtName';
        return false;
    }

    var emailID = $j("#txtMl");
    if ((emailID.val() == null) || (emailID.val() == "")) {
        showAlertDialog("error", 1015, "client");
        emailID.focus();
        errField = 'txtMl';
        return false;
    }
    else if (!IsValidEmail(emailID.val())) {
        showAlertDialog("error", 1014, "client");
        emailID.focus();
        errField = 'txtMl';
        return false;
    }

    $j("#hdnUsrName").val($j("#txtName").val());
    $j("#hdnMl").val($j("#txtMl").val());
    $j("#hdnProj").val(hdnproj);
    $j("#btnReset").click();

}

//Function to check for Email.
function CheckEmail(email) {

    var regex = new RegExp("^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+.[a-zA-Z]{2,4}$");
    return regex.test(email);
}

function AddLanguages() {
    if ($j('#language').length) {
        var axLang = $j("#hdnLangs").val();
        arrLangs = axLang.toLowerCase().split(",");
        $j('#language').val("");

        if (arrLangs.length < 1) {
            $j('#language').val("English");
            $j("#axLangFld").hide();
        }
        else if (arrLangs.length == 1) {
            if (arrLangs[0] != "") {
                $j('#language').val(arrLangs[0]);
                $j("#axLangFld").hide();
            }
            else {
                $j('#language').val("English");
                $j("#axLangFld").hide();
            }
        }
        else {
            $j("#axLangFld").show();
            createLanguageAutoComplete();
            var localStorageLang = "";
            try {
                if (typeof (Storage) !== "undefined") {
                    localStorageLang = localStorage["langInfo-" + appUrl];
                    if (typeof localStorageLang == "undefined") {
                        localStorageLang = "";
                    }
                }
            } catch (e) {
                //console.log("LocalStorage not Suported");
            }
            if (localStorageLang != "" && arrLangs.indexOf(localStorageLang.toLowerCase()) > -1 && $j('#language:visible').length > 0) {
                $j("#language").val(localStorageLang);
            } else {
                $j('#language').val(arrLangs[0]);
            }
        }
        $j('#language').val(capitalizeFirstLetter($j('#language').val().trim()));
    }
}

$j(document).ready(function () {
    if (typeof ok == 'undefined') {
        $j("#axpertMob").hide();
    }
    else {
        $j("#axpertMob").show();
    }
    appUrl = top.window.location.href.toLowerCase().substring("0", top.window.location.href.indexOf("/aspx/"));
    //loadCustJsLang();
    arrProjects = $j("#hdnAxProjs").val().replace("Select Project,", "").replace("Select Project", "").split(",");
    if ($j("#axpertVer").length > 0) {
        getVersionDetails();
    }
    AddLanguages();

    if (arrProjects.length < 1) {
        $j("#selectProj").hide();
    }
    else if (arrProjects.length == 1) {
        $j('#axSelectProj').val(arrProjects[0]);
        $j("#selectProj").hide();
    }
    else {
        createProjectListAutoComplete();
    }

    if ($j("#hdnProj").length && $j("#hdnProj").val() != "") {
        $j("#axSelectProj").val($j("#hdnProj").val());
    }
    var localStorageProj = "";
    try {
        if (typeof (Storage) !== "undefined") {
            localStorageProj = localStorage["projInfo-" + appUrl];
            if (typeof localStorageProj == "undefined") {
                localStorageProj = "";
            }
        }
    } catch (e) {
        //console.log("LocalStorage not Suported");
    }
    if (localStorageProj != "" && arrProjects.indexOf(localStorageProj) > -1 && arrProjects.length > 1 && $j('#axSelectProj').length > 0) {
        $j("#axSelectProj").val(localStorageProj);
    }

    SetFocus();
    GetProjLang();

    $("#axSelectProj, #Text1, #sw, #language, #txtName, #txtMl").on('keyup keypress blur change', function () {
        if ($(this).attr('id') == errField && $(this).val()) {
            HideErrorMsg();
            errField = "";
        }
    });
});


var myVersionJSON;
var versionInfoFile = "../versionInfo.json";
function getVersionDetails() {
    $.ajax({
        url: versionInfoFile,
        type: "GET",
        cache: false,
        statusCode: {
            404: function () {
                $.getJSON(versionInfoFile, function (json) {
                    //If File Dont Exist
                });
            }
        },
        success: function (json) {
            var data = { value: json };
            myVersionJSON = data.value;
            setVersionInfo();
        }
    });
}

function setVersionInfo() {

    var currentVerion = "10.0.0.0";
    var subVersion = "";
    currentVerion = myVersionJSON.version;
    if (myVersionJSON.subVersion) {
        subVersion = "_" + myVersionJSON.subVersion.toString();
    }
    var finalVersion = "";
    finalVersion = "Version " + currentVerion + subVersion;
    $j("#axpertVer").text(finalVersion);
}

function OpenForgotPwd() {
    var hdnproj = $j('#axSelectProj').val();
    window.document.location.href = "ForgotPassword.aspx?proj=" + hdnproj + "";

}

function HideErrorMsg() {
    hideAlertDialog('');
}

function GetCPOFL() {
    $.ajax({
        type: "POST",
        url: "signin.aspx/GetCPOFL",
        data: '{name: "' + $j('#axSelectProj').val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccessCPOFL,
        failure: function (response) {
            showAlertDialog("error", response.d);
        }
    });
}

function OnSuccessCPOFL() {

}

function GetProjLang() {
    $.ajax({
        type: "POST",
        url: "signin.aspx/GetCurrLang",
        data: '{name: "' + $j('#axSelectProj').val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: function (response) {
            showAlertDialog("error", response.d);
        }
    });
}

function GetProjTitle() {
    $.ajax({
        type: "POST",
        url: "signin.aspx/GetCurrAppTitle",
        data: '{name: "' + $j('#axSelectProj').val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccessTitle,
        failure: function (response) {
            showAlertDialog("error", response.d);
        }
    });
}
function OnSuccess(response) {
    var result = "";
    result = response.d;
    if (result != "" && result.toUpperCase() != "ENGLISH")
        $j("#axLangFld").show();
    else
        $j("#axLangFld").hide();
    $j("#hdnLangs").val(response.d);
    AddLanguages();
    GetProjCopyRightTxt();
}

function OnSuccessTitle(result) {
    if (result.d != "") {
        appTitle = result.d;
        $j("#spnAppTitle").html(appTitle);
    }
    else {
        $j("#spnAppTitle").html("Axpert");
    }
}

function GetProjCopyRightTxt() {
    $.ajax({
        type: "POST",
        url: "signin.aspx/GetCurrCopyRightTxt",
        data: '{name: "' + $j('#axSelectProj').val() + '", lang: "' + $j('#language').val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccessCopyRight,
        failure: function (response) {
            showAlertDialog("error", response.d);
        }
    });
}
function OnSuccessCopyRight(result) {
    if (result.d != "") {
        copyRightTxt = result.d;
        $j("#dvCopyRight").html(copyRightTxt);
    }

}

function ValidateProject() {
    var strProj = $j('#axSelectProj').val();
    if (strProj.length) {
        var isValidProj = false;
        for (k in arrProjects) {
            if (arrProjects[k].toLowerCase() == strProj.toLowerCase()) {
                isValidProj = true;
                break;
            }
        }
        if (strProj.toLowerCase() == "select project" || !isValidProj) {
            $j('#axSelectProj').val("");
            showAlertDialog("error", 1016, "client");
            $j("#axSelectProj").focus();
            errField = 'axSelectProj';
            return false;
        }
    }
    return true;
}

function ValidateLanguage() {
    var strLang = $j('#language').val();
    if (strLang.length) {
        var isValidLang = false;
        for (k in arrLangs) {
            if (arrLangs[k].toLowerCase() == strLang.toLowerCase()) {
                isValidLang = true;
                break;
            }
        }
        if (!isValidLang) {
            $j('#language').val("");
            showAlertDialog("error", 1017, "client");
            $j("#language").focus();
            errField = 'language';
            return false;
        }
    }
    return true;
}

function createProjectListAutoComplete() {
    $("#axSelectProj").autocomplete({
        minLength: 0,
        selectFirst: true,
        autoFocus: true,
        source: function (request, response) {

            var arrProjSearch = new Array;
            for (k in arrProjects) {
                if (arrProjects[k].toLowerCase().indexOf(request.term.toLowerCase()) >= 0) {
                    arrProjSearch.push(arrProjects[k]);
                }
            }

            if (arrProjSearch.length != 0) {
                response($.map(arrProjSearch, function (item) {
                    return {
                        label: item, value: item, link: ""
                    }
                }))
            }
            else {
                var result = [{ label: lcm[0], value: response.term, link: "" }];
                response(result);
            }
        },

        open: function (event, ui) {
            var dialog = $(this).closest('.ui-dialog');
            if (dialog.length > 0) {
                $('.ui-autocomplete.ui-front').zIndex(dialog.zIndex() + 1);
            }
        },

        select: function (event, ui) {

            if (ui.item.label == lcm[0]) {
                $(this).val('');
                return false;
            }
            else {
                $(this).val(ui.item.label);
            }
            try {
                if (typeof (Storage) !== "undefined") {
                    localStorage["projInfo-" + appUrl] = ui.item.label;
                }
            } catch (e) {
                //console.log("LocalStorage not Suported");
            }
            HideErrorMsg();
            GetCPOFL();
        }
    });
}

function createLanguageAutoComplete() {
    $("#language").autocomplete({
        minLength: 0,
        selectFirst: true,
        autoFocus: true,
        source: function (request, response) {
            var arrLangSearch = new Array;
            for (k in arrLangs) {
                if (arrLangs[k].toLowerCase().indexOf(request.term.toLowerCase().trim()) >= 0) {
                    arrLangSearch.push(capitalizeFirstLetter(arrLangs[k]));
                }
            }

            if (arrLangSearch.length != 0) {
                response($.map(arrLangSearch, function (item) {
                    return {
                        label: item, value: item, link: ""
                    }
                }))
            }
            else {
                var result = [{ label: lcm[0], value: response.term, link: "" }];
                response(result);
            }
        },

        open: function (event, ui) {
            var dialog = $(this).closest('.ui-dialog');
            if (dialog.length > 0) {
                $('.ui-autocomplete.ui-front').zIndex(dialog.zIndex() + 1);
            }
        },

        select: function (event, ui) {
            if (ui.item.label == lcm[0]) {
                $(this).val('');
                return false;
            }
            else {
                $(this).val(ui.item.label);
                try {
                    if (typeof (Storage) !== "undefined") {
                        localStorage["langInfo-" + appUrl] = ui.item.label;
                    }
                } catch (e) {
                    //console.log("LocalStorage not Suported");
                }
            }

            $("#language").blur();
            GetProjCopyRightTxt();
        }
    });
}

function capitalizeFirstLetter(status) {
    return status.charAt(0).toUpperCase() + status.slice(1);
}
