<%@ Page Language="VB" AutoEventWireup="false" CodeFile="err.aspx.vb" Inherits="err" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Error Page">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">

    <title>Error</title>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript">
        var enableBackButton = false;
        var enableForwardButton = false;
    </script>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <%--custom alerts end--%>
    <script src="../Js/gen.min.js?v=13" type="text/javascript"></script>
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />
    <%End If%>
    <script src="../Js/err.min.js?=3" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
</head>
<body>
    <form dir="<%=direction%>" name="f1" id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="../Js/helper.min.js?v=101" />
                </Scripts>
                <Services>
                    <asp:ServiceReference Path="../WebService.asmx" />
                    <asp:ServiceReference Path="../CustomWebService.asmx" />
                </Services>
            </asp:ScriptManager>

            <%If errMsg.Contains("Disconnected because you have logged into another session.") Then
                    Response.Write("<script>")
                    Response.Write("parent.parent.location.href='../aspx/sess.aspx';")
                    Response.Write("</script>")
                    Return
                ElseIf errMsg.Contains("Session Authentication failed...") Then
                    Response.Write("<script>")
                    Response.Write("parent.parent.location.href='../aspx/sess.aspx';")
                    Response.Write("</script>")
                    Return
                End If
            %>

            <div class="error err">
                <h3>
                    <asp:Label ID="lblerror" runat="server" meta:resourcekey="lblerror">Error</asp:Label></h3>
                <% If errMsg <> String.Empty Then%>
                <script type="text/javascript"> parent.window.closeFrame(); </script>
                <asp:Label ID="lblerror1" runat="server" meta:resourcekey="lblerror1">Error occured due to the following reason:&nbsp;</asp:Label>
                <%=errMsg%>
                <%Else%>
                <%=customError%>
                <%End If%>
            </div>
            <%=enableBackForwButton%>
            <div id="dvGoBack">

                <span class="navLeft icon-arrows-left-double-32" onclick="javascript:BackForwardButtonClicked(&quot;back&quot;);"
                    id="goback" style="text-decoration: none; cursor: pointer; border: 0px;"
                    title="Click here to go back" />

            </div>
            <asp:Label ID="lblCustomerror" runat="server" meta:resourcekey="lblCustomerror" Visible="false">Server error. Please try again.If the problem continues, please contact your administrator.</asp:Label>
            <asp:Label ID="lblInvParams" runat="server" meta:resourcekey="lblInvParams" Visible="false">Invalid parameter</asp:Label>
            <asp:Label ID="lbleroccurred" runat="server" meta:resourcekey="lbleroccurred" Visible="false">Error occurred(2). Please try again or contact administrator.</asp:Label>
            <asp:Label ID="lblUnknownError" runat="server" meta:resourcekey="lblUnknownError" Visible="false">Unknown error. Please try again. If the problem continues, please contact your administrator.</asp:Label>
        </div>
    </form>
</body>
</html>
