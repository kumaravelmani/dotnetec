<%@ Page Language="VB" AutoEventWireup="false" CodeFile="cpwd.aspx.vb" Inherits="cpwd" %>

<!DOCTYPE html>
<html>

<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Axpert Tstruct">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Change Password</title>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <%--custom alerts end--%>
    <script src="../Js/md5.min.js" type="text/javascript"></script>
    <script src="../Js/user.min.js?v=12" type="text/javascript"></script>
    <script src="../Js/adjustwindow.min.js?v=1" type="text/javascript"></script>
    <script src="../newPopups/axpertPopup.min.js?v=28"></script>
    <script src="../Js/gen.min.js?v=13" type="text/javascript"></script>
    <script src="../Js/helper.min.js?v=101" type="text/javascript"></script>
    <script src="../Js/messagebox.min.js?v=1" type="text/javascript"></script>
    <script src="../Js/cpwd.min.js?v=12" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <link href="../Css/messagebox.min.css" rel="stylesheet" type="text/css" />

    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />

    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.theme.min.css" rel="stylesheet" />
    <%--<link href="../Css/jQuery/jquery.ui.theme.css" rel="stylesheet" type="text/css" />--%>

    <!--Links for general styles -->
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />
    <link href="../Css/button.min.css" rel="stylesheet" />
    <link href="../Css/Icons/icon.min.css" rel="stylesheet" type="text/css" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link id="themecss" type="text/css" rel="Stylesheet" href="" />
    <link href="../Css/cpwd.min.css?v=3" rel="stylesheet" />
    <%End If%>
    <script type="text/javascript">
        var cpwdRemark = "";
        $(document).ready(function () {
            <%If (Request.QueryString("remark") <> Nothing) Then%>
            cpwdRemark = "<%=Request.QueryString("remark")%>";
            <%End If%>
        })
        var transid = 'pwda';
        var recordid = 0;
        var fldnameArr = new Array();
        var expr = new Array();
        var formcontrols = new Array();
        var patternName = new Array();
        var pattern = new Array();
        var AllowEmpty = new Array();
        var fNames = new Array();
        var gllangType = '<%=langType%>';
        fNames[0] = "uname";
        fNames[1] = "cepwd";
        fNames[2] = "epwd";
        fNames[3] = "pwd";
        fNames[4] = "cpwd";
        fNames[5] = "encpwd";
        var patterns = new Array();
        var isCloudApp = '<%=isCloudApp%>';
        <%If (remark <> "chpwd") Then %>
            var errorEnable = false;
        <%End If %>
    </script>
</head>
<%
    Dim xHtm As String
    Dim user As String
    Dim sid As String
    user = Session("user")


    sid = Session("nsessionid")
    xHtm = "<Script type='text/javascript'>"
    xHtm = xHtm & "function b() { a = '" & Session("project") & "';ba='" & user & "';c='pwda';d='" & sid & "';e = '" & Session("AxRole") & "'; f='" & Session("trace") & "';SetTstProps(a,ba,c,d,e,f);}"
    xHtm = xHtm & "</script>"
    Response.Write(xHtm)
%>
<body id="main_body" runat="server">
    <%If remark <> "chpwd" %>
    <video id="bgvid" runat="server" playsinline="" autoplay="" muted="" loop="">
        <source src="" type="video/mp4" id="bgvidsource" runat="server">
    </video>
    <%End If %>
    <div class="Family btextDir-<%=direction%>"  dir="<%=direction%>">
      <div <% If (remark <> "chpwd") Then%> class="  col-xs-8  col-xs-offset-2 col-sm-4  col-sm-offset-4 content vertical-align" <%End If%>>
        <form id="form1" runat="server" class="Pagebody" dir="<%=direction%>">
            <div>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                    <Scripts>
                        <asp:ScriptReference Path="../Js/gen.min.js?v=13" />
                        <asp:ScriptReference Path="../Js/tstruct.min.js?v=299" />
                    </Scripts>
                    <Services>
                        <asp:ServiceReference Path="../WebService.asmx" />
                    </Services>
                </asp:ScriptManager>
            </div>

            <%If (remark = "chpwd") Then%>
            <div id="dvMainPwd" runat="server" style="margin-top: -18px;">
                <div id="breadcrumb-panel">
                    <div id='breadcrumb' class='icon-services left'>
                        <div class='icon-services left bcrumb pd10' style='margin-top: 0px; padding: 12px; font-size: 18px; padding-left: 30px;'>
                        </div>
                    </div>
                    <div id='icons' class="right" runat="server" style="display: none">
                        <asp:ImageButton runat="server" AlternateText="Save" ID="btnSubmit" OnClientClick="javascript:return ValidatePassword(this);"
                            ImageUrl="../AxpImages/save.png" />
                        &nbsp;&nbsp;
                    </div>
                </div>
            </div>
            <%End If%>
            <div>
                <div>
                    <%If (remark <> "chpwd") Then %>
                    <h2 class="form-title" style="font-size: 1.5em; margin-top: 8px; margin-left: -17px;">
                        <img src="../images/favicon.ico">
                        <span id="spnCpwdHeading"></span>
                    </h2>
                    <div id="dvMgs" style="color: #606060;">
                        <% If (remark = "1") Then%>
                        <asp:Label ID="lbllogging" runat="server" meta:resourcekey="lbllogging">
                            You are logging in for the first time. You need to change the password.
                        </asp:Label>
                        <%ElseIf (remark = "2") Then%>
                        <asp:Label ID="lblpwdexp" runat="server" meta:resourcekey="lblpwdexp">
                            Your password is expired. You need to change the password.
                        </asp:Label>
                        <%ElseIf (remark = "3") Then%>
                        <asp:Label ID="lblpwdreset" runat="server" meta:resourcekey="lblpwdreset">
                            Your password has been reset. You need to change your password.
                        </asp:Label>
                        <%End If%>
                    </div>
                    <%End If %>

                    <div class="<%If (remark = "chpwd") %>chpwd-position <%Else %> firsttime-chpwd-position <%End If %> form-horizontal">
                        <div id="dvCp">
                            <div style="display: none;">
                                <input type="hidden" name="recordid000F0" value="0" />
                                <input type="hidden" name="html_transid000F0" value="pwda" /><input type="hidden"
                                    name="rn000F0" value="<%=Session("user")%>" />
                            </div>
                            <div class="form-group" id="Epwddiv">
                                <%If (remark = "chpwd") Then %>
                                <asp:Label ID="lblexistingpwd" runat="server" meta:resourcekey="lblexistingpwd" class="control-label col-lg-5" for="email">Existing Password<span class="allowempty">* </span></asp:Label>
                                <%End If %>
                                <div class="<%If Not (remark = "chpwd") %> col-lg-12 txt-cpwd <%Else %> col-lg-7 <%End If %>">
                                    <%If not (remark = "chpwd") Then %>
                                    <span class="fa fa-lock fpwdlock"></span>
                                    <%End If %>
                                    <input id="existingPwd" type="password" value="" name="swe000F0"
                                        onfocus="this.value=this.value;" class="tem Family form-control <%If Not (remark = "chpwd") %> border-left <%End If %>" onblur="document.getElementById('swee000F0').value=md5auth(this.value);" autofocus autocomplete="off">
                                    <input type="hidden" runat="server" name="swee000F0" id="swee000F0" value="" />
                                </div>
                            </div>
                            <div class="form-group" id="Npwddiv">
                                <%If (remark = "chpwd") Then %>
                                <asp:Label ID="lblnewpwd" runat="server" meta:resourcekey="lblnewpwd" class="control-label col-lg-5" for="email">New Password<span class="allowempty">* </span></asp:Label>
                                <%End If %>
                                <div class="<%If Not (remark = "chpwd") %> col-lg-12 txt-cpwd input-icon left <%Else %> col-lg-7 <%End If %>">
                                    <%If not (remark = "chpwd") Then %>
                                    <span class="fa fa-lock fpwdlock"></span>
                                    <%End If %>
                                    <input id="newPwd" type="password" value="" name="swn" onfocus="this.value = this.value;"
                                        class="tem Family form-control <%If Not (remark = "chpwd") %> border-left <%End If %>" onblur="document.getElementById('sw000F0').value=md5auth(this.value);" autocomplete="off" />
                                    <input runat="server" type="hidden" id="pwdlength" name="swlength" value="" />
                                    <input runat="server" type="hidden" id="sw000F0" name="sw000F0" value="" />
                                </div>
                            </div>
                            <div class="form-group" id="Region">
                                <%If (remark = "chpwd") Then %>
                                <asp:Label ID="lblconfirmpwd" runat="server" meta:resourcekey="lblconfirmpwd" class="control-label col-lg-5" for="email">Confirm Password<span class="allowempty">* </span></asp:Label>
                                <%End If %>
                                <div class="<%If Not (remark = "chpwd") %> col-lg-12 txt-cpwd <%Else %> col-lg-7 <%End If %>">
                                    <%If not (remark = "chpwd") Then %>
                                    <span class="fa fa-lock fpwdlock"></span>
                                    <%End If %>
                                    <input id="confirmPwd" type="password" value="" name="swc" onblur="document.getElementById('swc000F0').value=md5auth(this.value);" onfocus="this.value=this.value;"
                                        class="tem Family form-control <%If Not (remark = "chpwd") %> border-left <%End If %>" autocomplete="off">
                                    <input runat="server" type="hidden" name="swc000F0" id="swc000F0" value="" />
                                </div>
                            </div>
                            <div class="form-group " style="float: right; width: 100%; padding-right: 12px; text-align: right;">
                                <asp:Button runat="server" ID="btnSumit" OnClientClick="javascript:return ValidatePassword(this);"
                                    Text="" CssClass="hotbtn btn" OnClick="btnSubmit_Click" title="Save" />
                                <% If (remark = "chpwd") Then%>
                                <button type="button" class="coldbtn btn" onclick="parent.closeModalDialog()" id="btnClose" title="Cancel">Cancel</button>
                                <%End If %>
                            </div>
                            <% If (remark <> "chpwd") Then%>
                            <div class="col-lg-12" style="padding-right: 0px;">
                                <span style="float: right"><span id="axpertVer" runat="server"></span></span>
                            </div>
                            <%End If %>
                        </div>
                    </div>
                </div>
            </div>
            <div id="wBdr" class="wBdr" style="display: none">
                <div id="tabbody1" class="dcContent" style="width: 95%; top: 50px;" runat="server">
                    <div id="tab-body-new:1" class="dcContent" style="position: relative; width: 50%; top: 40px; margin-left: 300px; border: solid 5px gray;">
                        <div id="1" style="width: 100%; position: relative; height: 370px; overflow-x: auto; overflow-y: hidden;">
                        </div>
                    </div>
                </div>
            </div>
            <!--end-->
            <%If Request.Browser.Browser = "Firefox" Then%>
            <script type="text/javascript">        b();</script>
            <%End If%>
        </form>
    </div>
    </div>
</body>
</html>
