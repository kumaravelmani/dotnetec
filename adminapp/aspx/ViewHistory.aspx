<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewHistory.aspx.vb" Inherits="ViewHistory" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Axpert View history">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>View History</title>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript">
        var IsViewHist = true
    </script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server" dir="<%=direction%>">
        <div>
            <asp:Label ID="lblErrMsg" runat="server" CssClass="erred"></asp:Label></div>

        <%=htmlText %>
    </form>
</body>
</html>
