<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OpenLogFile.aspx.vb" Inherits="OpenLogFile" %>


<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Open log File">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Open Log File</title>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" id="generic" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="Link1" />
    <%End If%>
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <script src="../Js/gen.min.js?v=13" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <link id="themecss" type="text/css" rel="Stylesheet" href="" />
    <script src="../Js/OpenLogFile.min.js?v=4" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            //for focusing tab within the page
            setTimeout(function () {
                var elemntsToCheck = 'button[tabindex!="-1"],a[tabindex!="-1"],input[tabindex!="-1"],select[tabindex!="-1"],radio[tabindex!="-1"]';
                var inputs = $('#form1').find(elemntsToCheck).filter(':visible').not(':disabled');
                firstInput = inputs.first();
                lastInput = inputs.last();
                firstInput.addClass("firstFocusable");
                lastInput.addClass("lastFocusable");
                $(".firstFocusable").focus();
                $(".lastFocusable").on('keydown.tabRot', function (e) {
                    if ((e.which === 9 && !e.shiftKey)) {
                        e.preventDefault();
                        $(".firstFocusable").focus();
                    }
                });
                $(".firstFocusable").on('keydown.tabRot', function (e) {
                    if ((e.which === 9 && e.shiftKey)) {
                        e.preventDefault();
                        $(".lastFocusable").focus();
                    }
                });
            },500)
        })
    </script>
</head>

<body class="Family">
    <form id="form1" runat="server" dir="<%=direction%>">
        <div class="tstruct-content">
            <asp:GridView CellSpacing="-1"  ID="gvLogfiles" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                Font-Size="Small" GridLines="Both" AllowSorting="true" CssClass="dataGrid" 
                Width="100%" style= "margin-top: 50px">
                <RowStyle Height="5px" />
                <Columns>
                    <asp:TemplateField HeaderText="Trace files" ShowHeader="True"
                        ControlStyle-Width="80%">
                        <ItemStyle Font-Size="Small" HorizontalAlign="Left" VerticalAlign="Middle" Width="100%" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkfile" runat="server" CausesValidation="False" CommandName="Select"
                                CommandArgument='<%# Bind("link")%>' Text='<%# Bind("FileName")%>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
           
        </div>
        <asp:Label ID="lblNodata" runat="server" meta:resourcekey="lblNodata" Visible="false">No data found.</asp:Label>
    </form>
</body>
</html>
