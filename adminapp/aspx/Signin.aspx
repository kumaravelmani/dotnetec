<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Signin.aspx.vb" Inherits="Signin"
    Debug="true" %>

<%@ OutputCache Duration="1" Location="None" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Axpert Sign in">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">

    <title>
        <%=appTitle%></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="../Css/agileui.min.css?v=8" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../assets/css/custome.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/login.min.css?v=16" rel="stylesheet" type="text/css" />
    <link href="../assets/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- END PAGE LEVEL STYLES -->
    <link rel="shortcut icon" href="../images/favicon.ico" />
    <meta http-equiv="CACHE-CONTROL" content="NO-CACHE" />
    <meta http-equiv="EXPIRES" content="0" />
    <%If EnableOldTheme = "true" Then%>
    <link href="../Css/genericOld.min.css" rel="stylesheet" type="text/css" />
    <%Else%>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" />
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <%End If%>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <%--custom alerts end--%>
    <script type="text/javascript" src="../Js/login.min.js?v=32"></script>
    <script type="text/javascript" src="../Js/md5.min.js"></script>
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>
    <script src="../Js/common.min.js?v=62"></script>
    <script type="text/javascript">
        // to update error message timeout in config app after saving
        var errorEnable = '<%=HttpContext.Current.Session("AxErrorMsg")%>' == ''? 'false'  : '<%=HttpContext.Current.Session("AxErrorMsg")%>'
        //callParentNew("errorEnable=", errMessage);
   
        var errTimeout='<%=HttpContext.Current.Session("AxErrorMsgTimeout")%>' == ''? '0'  : '<%=Convert.ToInt32(HttpContext.Current.Session("AxErrorMsgTimeout")) * 1000%>'
        //callParentNew("errorTimeout=", errTimeout);
        
        history.go(1); // disable the browser's back button
        var gllangType = '<%=langType%>';
        function md5auth() {
            $j("#hdnAxProjs").val("");
            var password = $j("#sw").val();
            var hash = MD5(password); 
           
            var a = <%=Session("seed")%>;
            var newhash = MD5(a+hash);
            var selectedProj = '<%=Session("project")%>'; 
            var ddlProj = $j("#axSelectProj").val();
            if(selectedProj == "" && ddlProj != "")
                selectedProj = ddlProj;
            if(selectedProj != ddlProj)
                selectedProj = ddlProj;

            $j("#sw").val("");
            $j("#hash").val(newhash);   
            $j("#csrftoken").val(<%=Session("seed")%>);
            
            var un = $j("#Text1").val();
            var lang = $j("#language").val();
            var key = '<%=Session("CSRFToken")%>'; 
            $j("#key").val(key);
            $j("#rn").val(un);
            try {
                var isMobile ='<%=isMobileDevice%>';
                if (isMobile == "True" || jQBrowser.mozilla)
                    $j("#hdnAxProjs").val(selectedProj);
                ASB.WebService.CreateLoginKey(selectedProj, un, newhash, a, lang, key, SucceededCallbackKey, OnException);
            }
            catch (ex) {
                console.log("Exception in md5auth +" + ex.message);
            }
            return true;
        }

        function SucceededCallbackKey(result, eventArgs) {
            if (result == "Session Added") {
                var isMobile ='<%=isMobileDevice%>';
                if (isMobile != "True" || (!jQBrowser.mozilla && isMobile != "True")) {
                    $j("#hash").val("");
                    $j("#rn").val("");
                    $j("#rndh").val("");
                }
                setTimeout(function () {
                    window.form1.submit();
                },10);
            }
            else
                console.log("Error while passing details to Main page");
        }

        function OnException(result, eventArgs) {

            console.log("Error while passing details to Main page");
            return false;
        }
    </script>

    <noscript>
        <div style="background-color: #FFFF00">
            JavaScript is turned off in your web browser. Turn it on to take full advantage
            of this site, then refresh the page.
        </div>
    </noscript>
</head>
<!-- END HEAD -->
<body class="page-header-fixed login" id="main_body" runat="server">
    <video id="bgvid" runat="server" playsinline="" autoplay="" muted="" loop="">
        <source src="" type="video/mp4" id="bgvidsource" runat="server">
    </video>
    <div class="row-fluid" style="display: table; position: absolute; width: 100%; height: 100%;">
        <div class="center-view" style="display: table-cell; vertical-align: middle; margin: 0px auto;">
            <div class="content" style="margin-top: 0%; border-top: 3px solid #f67c0f;">
                <!-- BEGIN LOGIN FORM -->
                <form name="form1" id="form1" method="POST" action='SigninInter.aspx' defaultfocus="uname"
                    onsubmit="" class="form-vertical login-form" novalidate>
                    <%If (isMobileDevice = True) Then%>
                    <button type="button" id="axpertMob" value="axpertMobile" class="hybridAppSettingBtn" onclick="ok.performClick(this.value);"><i class="fa fa-cog"></i></button>
                    <% End If %>
                    <h2 class="form-title" style="font-size: 1.5em;">
                        <img src="../images/favicon.ico" /><asp:Label ID="lblSignin" runat="server" meta:resourcekey="lblSignin">Sign In </asp:Label>
                    </h2>
                    <div class="control-group" id="selectProj" runat="server">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <asp:Label ID="lblslctproj" runat="server" meta:resourcekey="lblslctproj" class="control-label visible-ie8 visible-ie9">
                            Select Project</asp:Label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-desktop"></i>
                                <input runat="server" type='text' value='' onblur="GetProjLang();" id='axSelectProj' name="axSelectProj" title="Select Project" tabindex='2' placeholder='Select Project' class='m-wrap placeholder-no-fix new-search-input search-input' required />
                                <i class="icon-cross" title="Clear" onclick="$('#axSelectProj').val('').focus();"></i>
                                <i class="icon-chevron-down autoClickddl" title="Select Project" onclick="$('#axSelectProj').autocomplete('search','').focus();"></i>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <asp:Label ID="lblusername" runat="server" meta:resourcekey="lblusername" class="control-label visible-ie8 visible-ie9">
                            Username</asp:Label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-user"></i>
                                <input class="m-wrap placeholder-no-fix" tabindex="2" size="15" id="Text1" type="text"
                                    autocomplete="off" placeholder="Username" name="rn" title="Username" required>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <asp:Label ID="lblpwd" runat="server" meta:resourcekey="lblpwd" class="control-label visible-ie8 visible-ie9">
                            Password</asp:Label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-basic-lock"></i>
                                <input id="sw" class="m-wrap placeholder-no-fix" type="password" autocomplete="off"
                                    placeholder="Password" name="sw" tabindex="2" title="Password" required>
                            </div>
                            <div class="" id="dvActSes" runat="server">
                                <%= ousers%>
                            </div>

                            <input type="hidden" name="rndh" id="rndh" value='<%=Session("seed")%>' />
                            <input type="hidden" name="hdnflag" id="hdnflag" />
                            <input type="hidden" runat="server" name="hdnAxProjs" id="hdnAxProjs" />
                            <input type="hidden" name="hash" id="hash" value="" />
                            <input type="hidden" name="csrftoken" id="csrftoken" value="" />
                            <input type="hidden" name="key" id="key" value="" />
                            <input type="hidden" runat="server" name="hdnesw" id="hdnesw" />

                            <asp:Label ID="lblCustomerror" runat="server" meta:resourcekey="lblCustomerror" Visible="false">Server error. Please try again.If the problem continues, please contact your administrator.</asp:Label>
                        </div>
                    </div>

                    <div class="hide control-group" id="axLangFld" runat="server" style="display: none;">
                        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                        <asp:Label ID="lblslctlang" runat="server" meta:resourcekey="lblslctlang" class="control-label visible-ie8 visible-ie9">
                            Select Language</asp:Label>
                        <div class="controls">
                            <div class="input-icon left">
                                <i class="icon-earth"></i>
                                <input type='text' value='' id='language' name="language" title="Select Language" tabindex='2' placeholder='Select Language' class='m-wrap placeholder-no-fix new-search-input search-input' required />
                                <i class="icon-cross" title="Clear" onclick="$('#language').val('').focus();"></i>
                                <i class="icon-chevron-down autoClickddl" title="Select Language" onclick="$('#language').autocomplete('search','').focus();"></i>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">

                        <button type="submit" title="Login" tabindex="3" id="btnSubmit" class="hotbtn btn pull-right" onclick="return chkLoginForm();">
                            Login <i class="icon-arrows-right"></i>
                        </button>
                    </div>
                    <div class="create-account">
                        <a href="javascript:void(0)" class="subtextblue" style="display: none;" onclick="OpenForgotPwd()">
                            <asp:Label ID="lblForgot" runat="server" meta:resourcekey="lblForgot">Forgot password?</asp:Label>
                        </a>
                        <span style="float: right; position: relative;"><span id="axpertVer" runat="server"></span></span>
                        <div class="clear"></div>
                    </div>

                    <div class="copyrightlabs">
                        <p id="dvCopyRight" runat="server" class="copyrightpara">©2017 Agile Labs Pvt. Ltd. All Rights Reserved.</p>

                    </div>
                </form>
                <!-- END LOGIN FORM -->
            </div>
        </div>
    </div>
    <script src="../assets/plugins/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
    <!-- IMPORTANT! Load jquery-ui-1.10.1.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
    <script src="../assets/plugins/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <%--<script src="../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>--%>
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>
    <!--[if lt IE 9]>
	<script src="assets/plugins/excanvas.min.js"></script>
	<script src="assets/plugins/respond.min.js"></script>  
	<![endif]-->
    <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js?v=3" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/scripts/app.min.js?v=1" type="text/javascript"></script>
    <script src="../assets/scripts/tasks.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL STYLES -->
    <!-- END PAGE LEVEL SCRIPTS -->
    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init(); // initlayout and core plugins            
            $("#sidemenu-leftt").click();
        });
    </script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <!-- END JAVASCRIPTS -->
    <form id="Form2" runat="server">
        <input type="hidden" id="hdnLangs" runat="server" />
        <input type="hidden" id="hdnAppTitle" runat="server" />
        <asp:ScriptManager ID="ScriptManager2" runat="server">
            <Services>
                <asp:ServiceReference Path="../WebService.asmx" />

            </Services>
        </asp:ScriptManager>
    </form>
</body>
</html>
