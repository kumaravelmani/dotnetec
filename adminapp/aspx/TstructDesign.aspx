<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TstructDesign.aspx.cs" Inherits="aspx_TstructDesign" %>



<!DOCTYPE html>
<html>

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Axpert Tstruct Design Mode">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Tstruct Design</title>
    <%-- <script type="text/javascript" src="../Js/Jquery-2.2.2.min.js"></script>--%>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>

    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <%--custom alerts start--%>
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <% if (direction == "rtl")
        { %>
    <link rel="stylesheet" href="../ThirdParty/bootstrap_rtl.min.css" type="text/css" />
    <% } %>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <%--<script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=1" type="text/javascript"></script>--%>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2"></script>
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../Css/codemirror.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Js/codemirror/addon/hint/show-hint.css">
    <script type="text/javascript">
        var mode = "design";
        //variables used for new picklist control
        var totalPLRows = 0;
        var pageSize = 10;
        var curPageNo = 1;
        var noOfPLPages = 0;
        var FetchPickListRows = '<%=FetchPickListRows%>';
        var isTstPop = '<%=isTstPop%>';
        //variables used for storing images in folders        
        var imgNames = new Array();
        var imgSrc = new Array();
        var clickedPosition = 0;
        var enableBackButton = false;
        var enableForwardButton = false;
        var appsessionKey = '<%=appsessionKey%>';
        var tracePath = '<%=traceLog%>';
        var tableValues = {};
    </script>

    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script src="../Js/main-tstruct.min.js?v=139"></script>

    <script type="text/javascript" src="../Js/ckeditor/ckeditor.js"></script>
    <script src="../Js/ckRtf.min.js?v=5" type="text/javascript"></script>

    <!--Links for Tab control -->

    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.theme.min.css" rel="stylesheet" />
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../Css/msgBoxLight.min.css" rel="stylesheet" type="text/css" />
    <link type="text/css" href="../AssetsNew/css/style.min.css?v=2" rel="stylesheet" />
    <link type="text/css" href="../Css/TstructNew.min.css?v=90" rel="stylesheet" />
    <link href="../Css/sqlBuilder.min.css?v=1" rel="stylesheet" />
    <link href="../Css/TstructDesign.min.css?v=4" rel="stylesheet" />
    <link href="../App_Themes/Gray/Stylesheet.min.css?v=23" rel="stylesheet" />
    <link id="themecss" type="text/css" href="" rel="stylesheet" />
    <link href="../Css/MergeColumn.min.css" rel="stylesheet" />

    <script src="../Js/jQueryUi/jquery.tablesorter.min.js" type="text/javascript"></script>
    <script src="../Js/JDate.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Js/jquery.msgBox.min.js" type="text/javascript"></script>
    <script src="../Js/jQueryUi/jquery.scrollabletab.min.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Js/md5.min.js"></script>
    <script type="text/javascript" src="../Js/tstruct.min.js?v=299"></script>
    <script type="text/javascript" src="../Js/helper.min.js?v=101"></script>

    <script type="text/javascript" src="../Js/process.min.js?v=134"></script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <script src="../Js/tstructvars.min.js?v=39" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/jsclient.min.js?v=21"></script>
    <script type="text/javascript" src="../Js/util.min.js?v=2"></script>


    <link href="../Css/propSheet.min.css?v=5" rel="stylesheet" />
    <script src="../Js/propSheet.min.js?v=19"></script>

    <script src="../Js/dimmingdiv.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/newGridJS.min.js?v=118"></script>
    <script src="../Js/autoComplete.min.js?v=55"></script>
    <link href="../Css/Tstruct-auto.min.css?v=22" rel="stylesheet" />

</head>

<body onload="ChangeDir('<%=direction%>');" class="btextDir-<%=direction%>">

    <%=tstHeader %>
    <div id="visibleMobile">
        <div id="dvToolbar" class="contentMobile" runat="server">
            <div class="row" id="comprsdModeTgl" title="Compress Mode" style="display: inline-block;">
                <div class="switch" onclick="toggleCompressedMode('s')">
                    <input type="checkbox" id="ckbCompressedMode">
                    <div class="slider round"></div>
                </div>
            </div>
            <div class="row" title="Add field">
                <a class="toolbarIcons" href="javascript:void(0)" id="propSheet" onclick="openProprtySht()"><i class="icon-arrows-circle-plus"></i></a>
            </div>

            <div class="row" title="one-column">
                <a class="toolbardiv" href="javascript:void(0)" id="btnAlignOne">
                    <img src="../AssetsNew/ICONS/1.png" style="height: 22px; width: 20px;" />
                </a>
            </div>

            <div class="row " title="merge">

                <a class="toolbardiv" href="javascript:void(0)" id="btnAlignMerge">
                    <img src="../AssetsNew/ICONS/2.png" style="height: 22px; width: 20px;" /></a>
            </div>

            <div class="row" title="Increase Width">
                <a class="toolbarIcons toolbardiv" href="javascript:void(0)" id="increaseWidth" onclick="IncreaseWidth();"><i class="glyphicon glyphicon-resize-horizontal icon-arrows-horizontal"></i></a>
            </div>
            <div class="row" title="Decrease width">
                <a class="toolbarIcons toolbardiv" href="javascript:void(0)" onclick="DecreaseWidth();" id="A2"><i class="glyphicon glyphicon-resize-small icon-arrows-shrink-horizonal2"></i></a>
            </div>
            <div class="row" id="incrheight" title="Increase Height">
                <a class="toolbarIcons toolbardiv" href="javascript:void(0)" onclick="IncreaseHeight();" id="A3"><i class="glyphicon glyphicon-resize-vertical icon-arrows-vertical"></i></a>
            </div>
            <div class="row" id="dcrheight" title="Decrease Height">
                <a class="toolbarIcons toolbardiv" href="javascript:void(0)" onclick="DecreaseHeight();" id="A4"><i class="glyphicon glyphicon-resize-small icon-arrows-shrink-vertical2"></i></a>
            </div>
            <div class="row" title="Show Field" style="padding: 5px 0px;">

                <a class="toolbardiv toolbarIcons" href="javascript:void(0)" id="showTheField" onclick="javascript:showTheField();">
                    <i class="glyphicon icon-basic-eye" style="text-decoration: none;"></i>

                </a>

            </div>
            <div class="row" title="Hide Field" style="padding: 5px 0px;">

                <a class="toolbardiv toolbarIcons" href="javascript:void(0)" id="hideTheField" onclick="javascript:hideTheField();">
                    <i class="glyphicon icon-basic-eye-closed" style="text-decoration: none;"></i>

                </a>

            </div>

            <div class="row " title="Reset Design" style="padding: 5px 0px;">

                <a class="toolbarIcons" href="javascript:void(0)" id="A7"><i class="glyphicon glyphicon-refresh icon-arrows-rotate" style="text-decoration: none;" onclick="javascript:resetButtonClicked();"></i></a>

            </div>


            <div class="row" title="Save">
                <a class="toolbarIcons toolbardiv" href="javascript:void(0)" id="A5"><i class="glyphicon glyphicon-floppy-disk icon-basic-floppydisk" onclick="javascript:saveAsJson();"></i></a>

            </div>
            <div class="row " title="Close" style="padding: 5px 0px;">

                <a class="toolbarIcons" href="javascript:void(0)" id="A8"><i class="glyphicon glyphicon-remove-circle icon-arrows-circle-remove" onclick="javascript:CancelButtonClicked();"></i></a>

            </div>
        </div>
    </div>
    <div class="" id="dvlayout" runat="server" style="margin-left: 40px; overflow-y: auto;">
        <div id="divmainheader" runat="server" style="margin-left: -15px;"></div>
        <form id="form1" runat="server" enctype="multipart/form-data">
            <div>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                    <Scripts>
                        <asp:ScriptReference Path="../Js/tstruct.min.js?v=299" />

                        <asp:ScriptReference Path="../Js/process.min.js?v=134" />

                    </Scripts>
                    <Services>
                        <asp:ServiceReference Path="../WebService.asmx" />
                        <asp:ServiceReference Path="../CustomWebService.asmx" />
                    </Services>
                </asp:ScriptManager>
                <asp:UpdateProgress ID="Up1" Visible="false" runat="Server">
                    <ProgressTemplate>
                        <span>
                            <img src="../AxpImages/icons/loading.gif" alt="Please wait" />
                            Please wait ...</span>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <asp:HiddenField ID="hdnDataObjId" runat="server" />
            <div class="col s3 card hoverable scale-transition scale-out" id="propertySheet">
                <div class="hpbHeaderTitle">
                    <span class="icon-paint-roller"></span>
                    <span class="title">Property Sheet</span>
                    <button title="Close" type="button" id="propertySrchCls" onclick="closeProprtySht();" class="btn-flat waves-effect btn-floating pull-right"><i class="icon-arrows-remove"></i></button>
                    <button title="Save" type="button" onclick="updateField();" id="updateWidget" class="btn-flat waves-effect btn-floating pull-right "><span class="icon-arrows-check"></span></button>
                    <div class="clear"></div>
                </div>
                <div id="propertySheetDataWrapper">
                    <div class="clear"></div>
                    <div id="propertySearch">
                        <input placeholder="Search..." type="text" id="propertySearchFld" class="normalFld searchFld">
                        <span class="srchIcn icon-magnifier"></span>
                    </div>
                    <div class="posr" id="propTableContent">
                        <table class="bordered">
                            <tr>
                                <td class="subHeading" colspan="2">General <span data-target="general" class="propShtDataToggleIcon icon-arrows-down"></span></td>
                            </tr>
                            <tr data-group="general">
                                <td>Name<sup style="color: red">*</sup></td>
                                <td>
                                    <input id="fldName" class="form-control" type="text">
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>Caption<sup style="color: red">*</sup></td>
                                <td>
                                    <input id="fldCaption" class="form-control" type="text">
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>Data Type</td>
                                <td>
                                    <select onchange="moeChanger(this)" class="form-control" id="seldataType">
                                        <option>Character</option>
                                        <option>Numeric</option>
                                        <option>Date/Time</option>
                                        <option>Image</option>
                                        <option>Text</option>
                                    </select>
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>DC</td>
                                <td>
                                    <select runat="server" class="form-control" id="seldc">
                                    </select>
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>Mode of entry</td>
                                <td>
                                    <select class="form-control" id="selmoe">
                                        <option value="Accept">Accept</option>
                                        <option value="Select">Select</option>
                                    </select>
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>Width</td>
                                <td>
                                    <input class="form-control" maxlength="2" id="fldWidth" value="10" type="text">
                                </td>
                            </tr>
                            <tr class="decimalFld notSearchable" style="display: none" data-group="general">
                                <td>Decimal</td>
                                <td>
                                    <input id="fldDecimal" maxlength="2" class="form-control" value="0" type="text">
                                </td>
                            </tr>
                            <tr data-group="general">
                                <td>Visible</td>
                                <td>
                                    <select class="form-control" id="selvisible">
                                        <option value="T">Yes</option>
                                        <option value="F">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="subHeading" colspan="2">Appearance <span data-target="appr" class="propShtDataToggleIcon icon-arrows-down"></span></td>
                            </tr>
                            <tr data-group="appr">
                                <td>Normalized</td>
                                <td>
                                    <select class="form-control" id="selNormalized">
                                        <option value="F">No</option>
                                        <option value="T">Yes</option>
                                    </select>
                                </td>
                            </tr>
                            <tr data-group="appr">
                                <td>Read Only</td>
                                <td>
                                    <select id="fldReadOnly" class="form-control">
                                        <option value="F">No</option>
                                        <option value="T">Yes</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="subHeading" colspan="2">Source <span data-target="src" class="propShtDataToggleIcon icon-arrows-down"></span></td>
                            </tr>
                            <tr data-group="src">
                                <td>SQL</td>
                                <td>
                                    <input onfocus="createSqlWindow()" id="sqlSource" class="form-control" type="text">
                                </td>
                            </tr>
                            <%--   <tr data-group="src">
                                <td>Expression</td>
                                <td>
                                    <input class="form-control" type="text">
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="subHeading" colspan="2">Validation <span data-target="valid" class="propShtDataToggleIcon icon-arrows-down"></span></td>
                            </tr>
                            <tr data-group="valid">
                                <td>Allow Empty</td>
                                <td>
                                    <select id="selAlwEmpty" class="form-control">
                                        <option value="T">Yes</option>
                                        <option value="F">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr data-group="valid">
                                <td>Allow Duplicate</td>
                                <td>
                                    <select id="selAlwDup" class="form-control">
                                        <option value="T">Yes</option>
                                        <option value="F">No</option>
                                    </select>
                                </td>
                            </tr>
                            <%--<tr data-group="valid">
                                <td>Validate Expression</td>
                                <td>
                                    <input class="form-control" type="text">
                                </td>
                            </tr>--%>
                        </table>
                    </div>
                </div>
            </div>
            <%--div to be shown as search dialog--%>
            <div id="searchFields" style="display: none;">
                <input type="text" id="srchDynamicText" class="AxSearchField" onkeyup="FindTstructString(this.value);" />&nbsp;&nbsp;&nbsp;&nbsp;<label id="searchResCount"></label>
                <button type="button" id="SearchPrevBtn" onclick="MovePrev();" class="icon-arrows-up" style="font-size: 12px"></button>
                <button type="button" id="SearchNextBtn" onclick="MoveNext();" class="icon-arrows-down" style="font-size: 12px"></button>
                <button type="button" onclick="UnSelectSearchItem('close');" class="icon-arrows-remove"></button>
            </div>
            <%--div to be shown as search dialog On Multi Select--%>
            <div id="srchMulSelFlds" style="display: none;">
                <input type="text" id="srchMulSelDynTxt" class="AxSearchField" onkeyup="FndMulSelStr(this.value);" />&nbsp;&nbsp;&nbsp;&nbsp;<label
                    id="srchMulSelResCnt"></label>
            </div>


            <div runat="server" id="searchoverlay" class="overlay hide">
                <% if (langauge == "ARABIC")
                    {%>
                <div style="float: right; font-size: 25px; color: White; margin-right: 20px; width: 90%;">
                    <asp:Label ID="lblheadsrch" runat="server" meta:resourcekey="lblheadsrch">Search</asp:Label>
                </div>
                <div class="tstructclose-button">
                    <a href="javascript:void(0)" style="float: left; margin-top: 2px; margin-left: 3px;">
                        <img alt="Close" onclick="javascript:Closediv();" id="closeimg" src="../AxpImages/icons/close-button.png" /></a>
                </div>
                <% }
                    else
                    { %>
                <div style="float: left; font-size: 25px; margin-left: -123; color: White;">
                    <asp:Label ID="lblheadsrch1" runat="server" meta:resourcekey="lblheadsrch1">Search</asp:Label>
                </div>
                <div class="tstructclose-button">
                    <a href="javascript:void(0)" style="float: right; margin-top: 2px; margin-right: 3px;">
                        <img alt="Close" onclick="javascript:Closediv();" id="closeimg" src="../AxpImages/icons/close-button.png" /></a>
                </div>
                <%} %>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <p>
                            <% if (langauge == "ARABIC")
                                {%>
                            <asp:Label ID="lblwith" runat="server" meta:resourcekey="lblwith" Style="margin-top: 20px;">
                                With</asp:Label>
                            <% }
                                else
                                { %>
                            <asp:Label ID="lblsrchfor" runat="server" meta:resourcekey="lblsrchfor" Style="margin-top: 20px;">
                                Search For</asp:Label>
                            <%} %>
                            <asp:DropDownList Width="200px" Style="margin-top: 20px;" ID="ddlSearch" CssClass="combotem Family" runat="server">
                            </asp:DropDownList>
                            <% if (langauge == "ARABIC")
                                {%>
                            <asp:Label ID="lblsrch" runat="server" meta:resourcekey="lblsrch" Style="margin-top: 20px;">
                                Search For
                            </asp:Label>
                            <% }
                                else
                                { %>
                            <asp:Label ID="lblwth" runat="server" meta:resourcekey="lblwth" Style="margin-top: 20px;">
                                With
                            </asp:Label>
                            <%} %>
                            <asp:TextBox Style="margin-top: 20px;" ID="searstr" Width="180" CssClass="tem Family" runat="server" value=""></asp:TextBox>
                            <asp:HiddenField ID="goval" runat="server" Value=""></asp:HiddenField>
                            <input id="Button1" type="button" name="go" value="Go" runat="server" onclick="javascript: valid_submit(); setDocht();" />
                            <div class="buttgo hide">
                                &nbsp;<asp:Button CssClass="searchHeadbar" ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click" />
                                <asp:HiddenField ID="hdnHtml" runat="server" Value=""></asp:HiddenField>
                                <asp:HiddenField ID="hdnFilename" runat="server" Value=""></asp:HiddenField>
                                <asp:Button CssClass="searchHeadbar" ID="btnHtml" runat="server" OnClick="btnHtml_Click" />
                                <asp:HiddenField ID="hdnSearchStr" runat="server" Value=""></asp:HiddenField>
                            </div>
                            <p>
                            </p>
                            <div id="srchcontent" runat="server">
                                <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" Width="100%">
                                    <asp:GridView CellSpacing="-1" ID="grdSearchRes" Style="margin-left: 5px;" runat="server" AllowSorting="false"
                                        AutoGenerateColumns="false" CellPadding="2" CssClass="gridData"
                                        GridLines="Vertical" OnPageIndexChanging="grdSearchRes_PageIndexChanging"
                                        OnRowDataBound="grdSearchRes_RowDataBound" PageSize="10" RowStyle-Wrap="false">
                                        <HeaderStyle CssClass="Gridview" ForeColor="#000000" />
                                        <AlternatingRowStyle CssClass="GridAltPage" />
                                    </asp:GridView>
                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="records" runat="server" Text=""></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="pgCap" runat="server" CssClass="seartotrecords" Text="Page No."
                                Visible="false"></asp:Label>
                                    <asp:DropDownList ID="lvPage" runat="server" AutoPostBack="true"
                                        onchange="javascript:Pagination();" Visible="false" Width="40px">
                                    </asp:DropDownList>
                                    <asp:Label ID="pages" Text="" runat="server" CssClass="totrec"></asp:Label>
                                </asp:Panel>
                            </div>
                            <p>
                            </p>
                        </p>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <div id="progressArea" class="Family loadingTheDataPW">
                            <asp:Label ID="lbldataload" runat="server" meta:resourcekey="lbldataload">
                                Loading the data, please wait...</asp:Label>

                            <asp:Image ID="LoadingImage" runat="server" ImageUrl="../AxpImages/icons/loading.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </div>
            <div runat="server" class="success hide" id="dvMessage">
            </div>
            <div id="workflowoverlay" runat="server" class="overlay hide">

                <p class="left">
                    <asp:TextBox ID="txtCommentWF" runat="server" Width="350px" TextMode="MultiLine"></asp:TextBox>
                </p>
                <p class="left workflow-buttons">
                    <input type="hotbtn btn" id="btntabapprove" onclick="CheckFields(this);" value="Approve" class="button" />
                    <input type="coldbtn btn" id="btntabreject" onclick="CheckFields(this);" value="Reject" class="button" />
                    <input type="hotbtn btn" id="btntabreview" onclick="CheckFields(this);" value="Approve & Forward" class="button" />
                    <input type="coldbtn btn" id="btntabreturn" onclick="CheckFields(this);" value="Return" class="button" />
                    <input type="coldbtn btn" id="btntabcomments" onclick="javascript: ViewComments();" value="View comments" class="button" />
                </p>
                <div class="clear">
                </div>
                <div class="wfsuccess">
                    <asp:Label ID="lblStatus" runat="server" class="wkfText" Text="Reviewed by"></asp:Label>
                    <asp:HiddenField runat="server" ID="hdnWfLno" />
                    <asp:HiddenField runat="server" ID="hdnWfELno" />
                    <asp:HiddenField ID="designHidden" runat="server" />
                </div>
            </div>
            <div id="file" class="hide">
                <table class="tblfile" style='padding: 1px; text-align: center;'>
                    <tr>
                        <td class="rowbor">
                            <asp:Label ID="LabelFs" runat="server" meta:resourcekey="LabelFs" Font-Bold="True" ForeColor="#1e90ff">File Name:</asp:Label>
                            <input id="filMyFile" accept="text/html" type="file" />
                            <input type="button" onclick="javascript: CallAfterFileUploadAction();" id="cafterfload"
                                name="cafterfload" />
                        </td>
                    </tr>
                    <tr style="vertical-align: top; text-align: center">
                        <td class="rowbor">
                            <asp:TextBox ID="TextBox2" runat="server" Width="1" Visible="true" BorderStyle="None"
                                ForeColor="white" BackColor="white"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div style="display: none;">
                    <input type="hidden" id="attachfname" />
                    <input type="button" onclick="javascript: callAfterFileAttach();" id="afterfattach"
                        name="afterfattach" />
                </div>
            </div>
            <asp:HiddenField ID="hdnScriptsUrlpath" runat="server" />
            <asp:HiddenField ID="hdnShowAutoGenFldValue" runat="server" />
            <asp:HiddenField ID="HdnAxAdvPickSearch" runat="server" Value="false" />
            <asp:HiddenField ID="hdnDesign" runat="server" />
            <asp:HiddenField ID="hdnLayout" runat="server" />
            <asp:Button runat="server" ID="btnSave" CssClass="hidden" OnClick="btnSave_Click" UseSubmitBehavior="false" />
            <div id="dvActivityInfo" runat="server" style="display: none; text-align: center;">
                <table style="table-layout: fixed; border: 0px; padding: 0px; border-spacing: 0px; width: 100%; height: 100%;">
                    <tbody>
                        <tr>
                            <td style="vertical-align: top;">
                                <table style="table-layout: fixed; margin-left: 82px; border: 0px; padding: 0px; border-spacing: 0px; width: 100%;">
                                    <tbody>
                                        <tr>
                                            <td style="padding: 0pt; vertical-align: top; text-align: left;">
                                                <table style="text-align: center; width: 100%; border: 0px; padding: 8px; border-spacing: 0px">
                                                    <tbody>
                                                        <tr>
                                                            <td style="vertical-align: top;" colspan="2">
                                                                <asp:GridView CellSpacing="-1" ID="grvActivities" runat="server" BackColor="White" CssClass="labelcap"
                                                                    BorderStyle="Groove" BorderWidth="2px" CellPadding="4" OnRowDataBound="grvActivities_RowDataBound">
                                                                    <RowStyle BackColor="White" BorderStyle="Groove" BorderWidth="2px" />
                                                                    <FooterStyle BackColor="#99CCCC" ForeColor="#003399" />
                                                                    <PagerStyle BackColor="#99CCCC" ForeColor="#003399" HorizontalAlign="Left" />
                                                                    <SelectedRowStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
                                                                    <HeaderStyle BackColor="#c3d9ff" Font-Bold="True" />
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="CustomDiv" runat="server" class="hide">
            </div>
            <div id="dvFormatDc" runat="server" class="hide">
            </div>
        </form>

        <div class="container-fluid">
            <div class="row">
            </div>
        </div>
        <form id="pagebdy" class="Pagebody" name="f1" onclick="javascript:HideTaskList();" dir="<%=direction%>">

            <%--  <div class="dvheightframe col-lg-12 col-xs-12 col-sm-12 col-md-12" style="float: none">--%>


            <div id="wBdr" class="tstructcontent" runat="server">
            </div>
            <%--</div>--%>
            <div style="display: none">
                <input type="button" value="CustomBtn" visible="false" id="btnAxCuxtom" onclick="javascript: CallAxCustomBtnFunction();" />
            </div>
            <input type="hidden" id="hdnActionName" />

            <asp:Label ID="lblNodata" runat="server" meta:resourcekey="lblNodata" Visible="false">No data found.</asp:Label>

            <div id="dvCustToolbar" style="position: relative; height: auto; display: none; width: 100%; top: 7px;" class="">
            </div>

            <div class="subres" style="display: none">
                <%=tstJsArrays.ToString() %>
                <%=tstVars.ToString()%>
                <%=tstTabScript.ToString()%>
                <%=tstScript.ToString()%>
                <%=enableBackForwButton%>
            </div>
            <div id="dvTip" class="randomDiv hide">
                <div class="tooltipBg Family">
                    <span class="closebtn"><a title="Close"
                        onclick="javascript:HideTooltip();">X</a></span>
                    <div id="dvInnerTip">
                    </div>
                </div>
            </div>
            <!--The below div is used to display the picklist dropdown -->
            <div id="dvPickList" style="height: 242px;" class="randomDivPl hide">
                <div style="width: 100%; height: 100%; background-color: White;">
                    <div id="dvPickHead">
                    </div>
                    <div id="dvPickFooter">
                        <table style="width: 100%; padding-bottom: 5px;">
                            <tr>
                                <td id="tdAdvSrch">
                                    <a onclick="javascript:CallSearchOpen();" class="curHand">
                                        <asp:Label ID="lbladvnce" runat="server" meta:resourcekey="lbladvnce">Advanced search</asp:Label></a>
                                </td>
                                <td style="width: 50%; padding-left: 20px;">
                                    <a id="prevPick" onclick="javascript:GetData('prev')" class="curHand">
                                        <img src="../AxpImages/arrow-right - Copy.png"></a>&nbsp;&nbsp;
                            <a id="nextPick" onclick="javascript:GetData('next');" class="curHand">
                                <img src="../AxpImages/arrow-right.png"></a>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <input type='hidden' id='hdnPickFldId' /><input type="hidden" id="hdnFiltered" />
                </div>
            </div>
            <div id="dvFillGrid" style="">
            </div>
            <div id="dvPrintDoc" class="hide">
            </div>
            <div id="divCustomAct" runat="server" visible="false" class="wBdr" style="width: 99.8%; border-collapse: collapse;">
                <div id="divActHeader" class="dcHeaderBar" style="background-color: #f8f8f8;">
                    <a href='javascript:ShowAndHideActDiv("divActBody");'><span style="vertical-align: top"
                        id="dcButspan3">
                        <img id="imgAct" style="border: 0px" alt="show" src="../AxpImages/icons/16x16/expandwt.png" /></span></a>
                    <span style="vertical-align: top" class="frameCap">
                        <asp:Label ID="lblrecentact" runat="server" meta:resourcekey="lblrecentact">Recent activities</asp:Label></span>
                </div>
                <div id="divActBody" style="padding-bottom: 20px; display: none;">
                    <div id="divActContent">
                    </div>
                </div>
            </div>
            <div id="dvFooter">
                <div>
                </div>
            </div>
            <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="../Js/TstructDesigner.min.js?v=27"></script>
            <script src="../Js/codemirror/codemirror.js" type="text/javascript"></script>
            <script src="../Js/multiselect.min.js" type="text/javascript"></script>
            <script src="../Js/codemirror/mode/sql.js" type="text/javascript"></script>
            <script src="../Js/codemirror/addon/hint/show-hint.js" type="text/javascript"></script>
            <script src="../Js/codemirror/addon/hint/sql-hint.js" type="text/javascript"></script>
            <script src="../Js/sqlBuilder.min.js?v=4"></script>
        </form>
    </div>


</body>

</html>
