<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HomePage.aspx.cs" Inherits="aspx_HomePage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../ThirdParty/materialize/css/materialize.min.css?v=1" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css" rel="stylesheet" />
    <link href="../ThirdParty/DataTables-1.10.13/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../newPopups/Remodal/remodal.min.css?v=2" rel="stylesheet" />
    <link href="../newPopups/axpertPopup.min.css?v=14" rel="stylesheet" />
    <link href="../Css/builderHome.min.css?v=32" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="" id="homeBuilderLink" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script>
        var userResponsibilities = '<%=Session["AxResponsibilities"] %>';
        userResponsibilities = userResponsibilities.split(",");
        var buildModeAccess = '<%=homeBuilder%>';
        buildModeAccess = buildModeAccess.split(',');
        var globalVars = '<%=globalVars%>';
        var imageBase = '<%=homeBuilderImagePath%>';
        var myTaskString = '<%=myTaskString%>';
        var appsessionKey = '<%=appsessionKey%>';
        var dbType = '<%=Session["axdb"]%>';
    </script>
</head>
<body dir="<%=direction %>">
    <div class="row" style="margin: 0px;">
        <div id="HPBmainWrapper">
            <div class="col s12" id="HPBdesignerCanvas">
                <div class="hpbWrapper">
                    <div class="hpbHeaderTitle">
                        <span class="icon-home2"></span>
                        <!-- <i class="material-icons">border_all</i> -->
                        <span class="title">Home</span>
                        <span id="buildMode" style="display: none;" class="right upDwnBtnWrapper">
                            <button title="Builder" onclick="parent.LoadIframe('HomeBuilder.aspx');" class="waves-effect waves-teal btn-flat"><span class="icon-hammer2"></span></button>
                        </span>
                        <div class="clear"></div>
                    </div>
                    <div id="hpbDsgnrcnvsWrapper">
                        <ul class="tabs" id="HPBtabsHaeder" style="display: none;">
                        </ul>
                        <div id="sortable" class="row mainWidgetAddedWrapper">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <script type="text/javascript" src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <script src="../ThirdParty/jquery-mousewheel/jquery-mousewheel.min.js"></script>
    <script src="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.js"></script>
    <script src="../ThirdParty/materialize/js/materialize.min.js?v=11"></script>
    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js"></script>
    <script src="../newPopups/Remodal/remodal.min.js"></script>
    <script src="../newPopups/axpertPopup.min.js?v=28"></script>
    <script src="../Js/alerts.min.js?v=24"></script>
    <script src="../ThirdParty/Highcharts/highcharts.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-3d.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-more.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-exporting.js"></script>
    <script src="../Js/high-charts-functions.min.js?v=17"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/moment.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/media/js/jquery.dataTables.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/datetime-moment.js"></script>
    <script src="../Js/templateParser.min.js?v=8"></script>
    <script src="../Js/commonBuilder.min.js?v=42"></script>
    <script src="../Js/commonHome.min.js?v=20"></script>
    <script src="../Js/builderHome.min.js?v=51"></script>

</body>
</html>
