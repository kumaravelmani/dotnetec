<%@ Page Language="C#" AutoEventWireup="true" CodeFile="listIview.aspx.cs" ValidateRequest="false"
    Inherits="aspx_listIview" EnableEventValidation="false" MetaKeywords="ListView" %>

<!DOCTYPE html>
<html>

<head runat="server">
    <meta charset="utf-8" />
    <meta name="description" content="Axpert List View">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>List IView</title>
    <!-- ________ CSS __________ -->
    <!-- jQuery library -->
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="generic" />
    <link href="../App_Themes/Gray/Stylesheet.min.css?v=23" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <%--custom alerts end--%>
    <link id="themecss" type="text/css" rel="Stylesheet" href="" />

    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.theme.min.css" rel="stylesheet" />
    <link href="../Css/listIview.min.css?v=8" rel="stylesheet" />
    <link href="../Css/iviewNew.css?v=11" rel="stylesheet" />
    <!-- ________ JQUERY __________ -->

    <link href="../newPopups/Remodal/remodal.min.css?v=2" rel="stylesheet" />
    <link href="../newPopups/Remodal/remodal-default-theme.min.css?v=2" rel="stylesheet" />
    <link href="../newPopups/axpertPopup.min.css?v=14" rel="stylesheet" />
    <script src="../newPopups/Remodal/remodal.min.js" type="text/javascript"></script>
    <script src="../newPopups/axpertPopup.min.js?v=28" type="text/javascript"></script>
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <!-- ________ JAVASCRIPT __________ -->
    <script type="text/javascript">
        var enableBackButton = false;
        var enableForwardButton = false;

        var IsNqIvTaskBtnCliked = false;
        var IsFindBtnClicked = false;
        var SaveWindow;
        var X = 0;
        var Y = 0;

        // var IsNqListViewBtnCliked = false;
        var IsNqViewBtnCliked = false;
        var isChartsAvailable = true;
        var isParamVisible = "false";
        //below line will load the google charts
    </script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/iview.min.js?v=175"></script>
    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <script src="../Js/helper.min.js?v=101" type="text/javascript"></script>
    <script src="../Js/listview.min.js?v=3" type="text/javascript"></script>
    <script src="../Js/dimmingdiv.min.js?v=2" type="text/javascript"></script>

    <script src="../Js/listIview.min.js?v=5" type="text/javascript"></script>
</head>

<body dir="<%=direction%>" class="btextDir-<%=direction%>" onload="ChangeDir('<%=direction%>');AdjustLviewWinff();">
    <div id="err">
    </div>
    <form id="form1" runat="server" onclick="HideNqIvTaskList();HideFindList();HideNqIvViewList();">
        <div id="divcontainer" runat="server">
            <%=strBreadCrumbBtns %>
            <div id='breadcrumb-panel'>
                <div id='breadcrumb' class='icon-services <%=bread_direction%>'>
                    <%=strBreadCrumb.ToString()%>
                </div>
            </div>
            <div>
                <%-- <h3 style="position: absolute;top: -5px;font-size: 20px;left: 34px;margin-bottom: 5px;margin-left: 10px;">
                    <%=  breadCrumb %>
                </h3>--%>
            </div>
            <%--<div id="visibleMobile" class="dvmainvm" runat="server">
                <div id="dvToolbar" class="contentMobile" runat="server">
                    
                </div>
            </div>--%>
            <div id="ivContainer">

                <div class="container">
                    <div class="row">
                        <div class="bs-example">
                            <div style="margin-bottom: 3px">
                                <div id="searchBar" class="" runat="server" style="float: left; margin-top: 5px;">
                                    <div id='icons'>
                                        <ul id="level1">
                                            <%=CustomListViewbtn%>
                                            <%=defaultBut%>
                                        </ul>
                                    </div>
                                </div>

                                <div id="showRecDetails" style="margin-top: 2px; margin-right: 10px; float: right;">
                                    <asp:Label ID="records" runat="server" Text="" CssClass="totrec"></asp:Label>
                                    <asp:Label ID="pgCap" Text="Page No." runat="server" Style="margin-left: 15px;"
                                        CssClass="totrec"></asp:Label>
                                    <asp:DropDownList ID="lvPage" runat="server"
                                        Style="width: 46px; height: 23px; position: relative;" AutoPostBack="true"
                                        CssClass="combotem" onchange="javascript:ShowDimmer(true);CloseTstructPopUp();"
                                        OnSelectedIndexChanged="lvPage_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:Label ID="pages" Text="" runat="server" CssClass="totrec"></asp:Label>



                                </div>

                                <div class="clear"></div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>

            <input type="hidden" name="ivtype" value="listview" />
        </div>
        <div class="listViewDc">
            <div id="noRecccord" style="display: none;" runat="server">
                <asp:Label ID="lblNodata" runat="server" meta:resourcekey="lblNodata">No data found.</asp:Label>
            </div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Services>
                    <asp:ServiceReference Path="../WebService.asmx" />
                </Services>
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnRemove" EventName="Click" />
                </Triggers>
                <ContentTemplate>
                    <div runat="server" id="searchoverlay" class="overlay">
                        <table>
                            <tr>
                                <td style="width: 25px;">&nbsp;
                                </td>
                                <td>
                                    <div id="dvStatus" style="margin-left: 25px;" runat="server" class="error hide">
                                        <asp:Label Visible="false" ID="lblerr" runat="server"
                                            Text="Please fill in the filter criteria."></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25px;">&nbsp;
                                </td>
                                <td>
                                    <div style="height: 35px; margin-left: 25px;">
                                        <asp:Label ID="lblcolname" runat="server" meta:resourcekey="lblcolname">
                                            Column Name
                                        </asp:Label>
                                        <asp:DropDownList ID="ddlFilter" runat="server" Width="150px"
                                            AutoPostBack="true" OnSelectedIndexChanged="ddlFilter_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblfilter" runat="server" meta:resourcekey="lblfilter">
                                            Filter With
                                        </asp:Label>
                                        <asp:DropDownList ID="ddlFilcond" Width="150px"
                                            OnSelectedIndexChanged="ddlFilcond_SelectedIndexChanged" runat="server"
                                            AutoPostBack="true" onchange="javascript:CheckCond(this);">
                                        </asp:DropDownList>
                                        <asp:Label ID="lblvalue" runat="server" meta:resourcekey="lblvalue">
                                            Value1
                                        </asp:Label>
                                        <asp:TextBox ID="txtFilter" runat="server" Width="150px" Text=""
                                            ReadOnly="true"></asp:TextBox>
                                        <asp:Label ID="lblfilVal2" runat="server" meta:resourcekey="lblfilVal2"
                                            Visible="false">
                                            Value2
                                        </asp:Label>
                                        <asp:TextBox ID="filVal2" runat="server" Width="150px" Text="" Visible="false">
                                        </asp:TextBox>
                                        <asp:HiddenField ID="goval" runat="server" Value=""></asp:HiddenField>
                                        &nbsp;
                                        <asp:Button ID="btnFilter" Text="Go" runat="server"
                                            OnClientClick="ShowDimmer(true);" OnClick="btnFilter_Click" />
                                        &nbsp;
                                        <asp:Button ID="btnClear" Text="Clear" runat="server" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="overflow: hidden;">



                        <div id="leftPanel" runat="server" title="Filter Pannel" style="display: none">
                            <div>


                                <div>

                                    <asp:HiddenField ID="hdnJsonData" runat="server" />
                                    <asp:HiddenField ID="hdnSortColName" runat="server" />
                                    <asp:HiddenField ID="hdnFilterColName" runat="server" />
                                    <asp:HiddenField ID="hdnFilterBy" runat="server" />
                                    <asp:HiddenField ID="hdnHideColName" runat="server" />
                                    <asp:HiddenField ID="hdnNoOfRecords" runat="server" />

                                    <asp:HiddenField ID="hdnGridColumns" runat="server" />
                                    <asp:HiddenField ID="hdnHideColumns" runat="server" />
                                    <asp:HiddenField ID="hdnFilterChecked" runat="server" />
                                    <table id="tblFilterCols" style="display: none"></table>
                                    <asp:HiddenField ID="hdnIsParaVisible" runat="server" />
                                    <asp:HiddenField ID="hdnHideShowFld" runat="server" />
                                    <asp:HiddenField ID="hdnSortColumns" runat="server" />
                                    <asp:HiddenField ID="hdnFilterColumns" runat="server" />
                                    <asp:HiddenField ID="hdnlvKey" runat="server" />
                                    <asp:HiddenField ID="hdnAutoSplit" runat="server" />
                                    <asp:HiddenField ID="hdnDisableSplit" runat="server" />
                                    <div style="display: none;">
                                        <asp:Button ID="SortGrid" runat="server" OnClick="btnSortGrid_Click" />
                                        <asp:Button ID="FilterGrid" runat="server" OnClick="btnFilterGrid_Click" />
                                        <asp:Button ID="HideColumn" runat="server" OnClick="btnHideColumn_Click" />


                                        <asp:HiddenField ID="hdnTotalRec" runat="server" />
                                        <asp:HiddenField ID="printRowsMaxLimitField" runat="server" />
                                    </div>

                                </div>


                                <h3 id="ivSortHeader" onclick="UpdateActiveArray(this);">
                                    <asp:Label ID="lblsortby" runat="server" meta:resourcekey="lblsortby">Sort By
                                    </asp:Label>
                                </h3>
                                <div id="ivSortContent">
                                    <ul id="sortIvByColumn">
                                    </ul>
                                </div>
                            </div>
                            <div>
                                <h3 id="HideCol" onclick="UpdateActiveArray(this);">
                                    <asp:Label ID="lblhidecol" runat="server" meta:resourcekey="lblhidecol">Hide Column
                                    </asp:Label>
                                </h3>
                                <div id="HideColDiv">
                                    <ul id="HideColUl">
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="contentPanelGrid">
                            <div id="divLContainer" style="vertical-align: top;" class="dvIvCont iviewTableWrapper">
                                <asp:Panel ScrollBars="auto" runat="server" Width="100%" ID="Panel2" CssClass="pnlgrid">
                                    <asp:Label ID="lblFltrCondition" class="HeaderLabel" runat="server" Visible="false">
                                    </asp:Label>
                                    <asp:GridView CellSpacing="-1" ID="GridView1" runat="server" CellPadding="0"
                                        RowStyle-Wrap="false" AutoGenerateColumns="false" CssClass="gridData"
                                        AllowSorting="true" OnRowDataBound="GridView1_RowDataBound"
                                        OnSorting="GridView1_Sorting">
                                        <RowStyle Wrap="False" />
                                        <EmptyDataRowStyle />
                                        <HeaderStyle />
                                    </asp:GridView>

                                </asp:Panel>



                            </div>
                        </div>
                        <div style="display: none">
                            <asp:DropDownList ID="ddlFldTypes" runat="server">
                            </asp:DropDownList>
                            <asp:HiddenField ID="hdnLvlValues" runat="server" />
                        </div>
                        <div id="dvCharts" class="hide dvChart">
                            <div class="dvChartCont">
                                <a href="javascript:void(0)" onclick="javascript:CloseChart('LView');"
                                    style="float: right; margin-right: 3px; text-decoration: none">
                                    <img src="../AxpImages/icons/close-button.png" /></a>
                                <table style="width: 90%">
                                    <tr>
                                        <td>
                                            <span>
                                                <asp:RadioButtonList ID="rbtnChartType" runat="server"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="line" Selected="True">Line</asp:ListItem>
                                                    <asp:ListItem Value="bar">Bar</asp:ListItem>
                                                    <asp:ListItem Value="pie">Pie</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </span>
                                        </td>
                                        <td>
                                            <a id="lnkChart" href="javascript:ShowChartPopUp(''LView);">View Full
                                                Screen</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-left: 10px;">
                                            <asp:Label ID="xaxis" runat="server" meta:resourcekey="xaxis">
                                                X-Axis</asp:Label>
                                            <asp:DropDownList ID="ddlChartCol1" Style="width: 100px" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:Label ID="yaxis" runat="server" meta:resourcekey="yaxis">
                                                Y-Axis</asp:Label>
                                            <asp:DropDownList ID="ddlChartCol2" Style="width: 100px" runat="server">
                                            </asp:DropDownList>
                                            <asp:Button ID="btnGetChart1" runat="server" OnClick="btnGetChart_Click"
                                                Text="Go" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="chart_div" style="height: auto; margin: 0 auto; overflow-x: auto;">
                            </div>
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnChartSize" runat="server" />
                    <div id="bg" class="popup_bg">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div id="divAction" class="popup">
                <table style="width: 100%" id="breadcrumb-panelpopup">
                    <tr>
                        <td style="font-size: large; padding: 5px;">
                            <asp:Label ID="lblcustom" runat="server" meta:resourcekey="lblcustom">View custom
                            </asp:Label>
                        </td>
                        <td style="width: 5%;  text-align: right;">
                            <i id="closeList" class="glyphicon glyphicon-remove icon-arrows-remove" tabindex="0"
                                onkeypress="javascript: return CloseListActionWin();"
                                onclick="javascript: return CloseListActionWin();"></i>
                        </td>
                    </tr>
                </table>
                <table id="Table1" style="width: 100%;">
                    <tr>
                        <td colspan="5"></td>
                    </tr>
                    <tr>
                        <td style="width: 35%"></td>
                        <td style="width: 5%; text-align: left;">
                            <asp:Label ID="lblviewtext" runat="server" meta:resourcekey="lblviewtext"
                                Style="width: 40px">
                                Views :
                            </asp:Label>
                        </td>
                        <td id="tdView" style="width: 5%; text-align: left;">
                            <asp:DropDownList runat="server" Width="150px" ID="ddlViewList" onchange="EditView(this);">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 5%; text-align: left;">
                            <asp:Label ID="lblViewName" runat="server" meta:resourcekey="lblViewName"
                                Style="width: 87px; margin-top: 6px;">
                                View name :</asp:Label>
                        </td>
                        <td style="width: 50%; text-align: left;">
                            <asp:TextBox runat="server" ID="txtViewName" MaxLength="15" ToolTip="View Name"
                                CssClass="form-control"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr style="vertical-align: bottom">
                        <td style="vertical-align: top">
                            <asp:Panel ID="pnl1" runat="server" CssClass="pnlInner" Width="150px"
                                Style="margin-bottom: 118px" TabIndex="0">
                                <div onclick="javascript:ToggleParamsLstProp();" id="divProp" style="width: 100%">
                                    <span class="tstcap">&nbsp;<img id="imgArrow" runat="server"
                                            src="../AxpImages/arrow-down-black.gif" alt="Hide" class="curhand" />
                                        <a id="lblHeading" tabindex="0" class="popLabel">
                                            <asp:Label ID="lblprop" runat="server" meta:resourcekey="lblprop">Properties
                                            </asp:Label>
                                        </a>
                                    </span>
                                </div>
                                <div id="divProperty" style="width: 100%; border: 10px;">
                                    <table id="tblProp" style="width: 100%;">
                                        <tr id="tr1" class="tvRow RowCust">
                                            <td>
                                                <a id="Columns" href="javascript:void(0)"
                                                    style="text-decoration: none; padding-left: 10px"
                                                    title="Select columns for filtering">
                                                    <asp:Label ID="lblcolumn" runat="server"
                                                        meta:resourcekey="lblcolumn">Columns</asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr id="tr2" class="tvRow RowCust">
                                            <td>
                                                <a href="javascript:void(0)" id="Conditions"
                                                    style="text-decoration: none; padding-left: 10px"
                                                    title="Set filter conditions">
                                                    <asp:Label ID="lblcond" runat="server" meta:resourcekey="lblcond">
                                                        Conditions</asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr id="tr3" class="tvRow RowCust">
                                            <td>
                                                <a href="javascript:void(0)" id="NoRepeat"
                                                    style="text-decoration: none; padding-left: 10px"
                                                    title="Select columns for grouping">
                                                    <asp:Label ID="lblrepeat" runat="server"
                                                        meta:resourcekey="lblrepeat">No Repeat </asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr id="tr4" class="tvRow RowCust">
                                            <td>
                                                <a href="javascript:void(0)" id="SortBy"
                                                    style="text-decoration: none; padding-left: 10px"
                                                    title="Select columns for sorting">
                                                    <asp:Label ID="lblsort" runat="server" meta:resourcekey="lblsort">
                                                        Sort By</asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr id="tr5" class="tvRow RowCust">
                                            <td>
                                                <a href="javascript:void(0)" id="SubTotal"
                                                    style="text-decoration: none; padding-left: 10px"
                                                    title="Select columns to define subtotal">
                                                    <asp:Label ID="lblsubtotal" runat="server"
                                                        meta:resourcekey="lblsubtotal">Sub Total</asp:Label>
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Label ID="lblProperty" runat="server" meta:resourcekey="lblProperty"
                                class="HeaderLabel" Style="width: 790px">
                                Columns</asp:Label>
                            <asp:Panel ID="pnlMain" runat="server" CssClass="noHScroll" BorderColor="AliceBlue"
                                BorderStyle="Ridge" BorderWidth="2px" Width="790px" Wrap="false" Height="295px">
                                <div id="divtr1" class="ListCust" title="Columns">
                                    <asp:TreeView ID="tvList" runat="server" CssClass="Treeview"
                                        AfterClientCheck="CheckChildNodes();" onclick="OnTreeClick(event)" LeafNodeStyle-CssClass="childnode">
                                    </asp:TreeView>
                                </div>
                                <div id="divtr2" class="ListCust" style="width: 95%">
                                    <table style="width: 100%">
                                        <tr>
                                            <td align="right" style="padding-left: 82px;">
                                                <span style="font-weight: bold">
                                                    <asp:Label ID="lblstatus" runat="server"
                                                        meta:resourcekey="lblstatus">Status : </asp:Label>
                                                </span>
                                            </td>
                                            <td>
                                                <asp:RadioButtonList ID="rbtnStatus" CssClass="radioBtnList"
                                                    TextAlign="Right" RepeatDirection="Horizontal" runat="server">
                                                    <asp:ListItem Text="ON" Value="ON">ON</asp:ListItem>
                                                    <asp:ListItem Text="OFF" Value="OFF">OFF</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="filterDiv" style="width: 100%">
                                        <table id="tblpanel" style="width: 100%; padding: 0px;">
                                            <tr>
                                                <th style="padding-left: 92px">
                                                    <asp:Label ID="lblfiled" runat="server" meta:resourcekey="lblfiled">
                                                        Field</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lblcondtn" runat="server"
                                                        meta:resourcekey="lblcondtn">Condition</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lblval" runat="server" meta:resourcekey="lblval">
                                                        Value</asp:Label>
                                                </th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <td id="lvfld" style="text-align: left">
                                                    <asp:DropDownList ID="ddlListFilter" Width="200px"
                                                        Style="margin-left: 90px" ToolTip="Select column"
                                                        CssClass="lblTxt form-control" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td id="filcond">
                                                    <asp:DropDownList ID="ddlListFilcond" Width="200px"
                                                        ToolTip="Select filter conditions"
                                                        CssClass="lblTxt form-control" runat="server"
                                                        onclick="javascript:CheckListFilterCond('-1');">
                                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                                        <asp:ListItem Text="Equal to" Value="="></asp:ListItem>
                                                        <asp:ListItem Text="Not Equal to" Value="<>"></asp:ListItem>
                                                        <asp:ListItem Text="Containing" Value="like"></asp:ListItem>
                                                        <asp:ListItem Text="Not Containing" Value="not like">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Less Than" Value="<"></asp:ListItem>
                                                        <asp:ListItem Text="Greater Than" Value=">"></asp:ListItem>
                                                        <asp:ListItem Text="Less Than or Equal to" Value="<=">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Greater Than or Equal to" Value="">=">
                                                        </asp:ListItem>
                                                        <asp:ListItem Text="Between" Value="between"></asp:ListItem>
                                                        <asp:ListItem Text="Is empty" Value="is null"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td id="filval11" style="text-align: left;">
                                                    <asp:TextBox ID="txtListFilter" ToolTip="Enter filter text"
                                                        Width="100px" runat="server" CssClass="lblTxt  form-control"
                                                        Text=""></asp:TextBox>
                                                </td>
                                                <td id="filval12" style="text-align: left;">
                                                    <asp:TextBox ID="ListfilVal2" Width="100px"
                                                        ToolTip="Enter filter text" Enabled="false" runat="server"
                                                        CssClass="lblTxt form-control" Text=""></asp:TextBox>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value="">
                                                    </asp:HiddenField>
                                                    <asp:HiddenField ID="FilterRowCount" Value="0" runat="server" />
                                                    <asp:HiddenField ID="HiddenField2" runat="server" />
                                                    <asp:HiddenField ID="hdnFilterXml" runat="server" />
                                                    <asp:HiddenField ID="hdnValidate" runat="server" />
                                                </td>
                                                <td style="text-align: left;">
                                                    <span title="Add condition"
                                                        class="curHand curHand icon-arrows-circle-plus" id="btnAddNew"
                                                        onclick="javascript:AddCondition();"></span>
                                                    <span title="Remove condition"
                                                        class="curHand icon-arrows-circle-minus" id="btnDelete"
                                                        onclick="javascript:RefreshFirstListCondition();"></span>
                                                </td>
                                            </tr>
                                        </table>
                                        <table id="tblFilter" style="width: 100%">
                                        </table>
                                        <div id="dvNewrow"
                                            style="position: relative; top: 3%; height: auto; display: block">
                                        </div>
                                    </div>
                                </div>
                                <div id="divtr3" class="ListCust" title="No Repeat">
                                    <table id="tblNorep">
                                    </table>
                                </div>
                                <div id="divtr4" class="ListCust">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>
                                                    <asp:Label ID="lblsortby1" runat="server"
                                                        meta:resourcekey="lblsortby1">Sort By</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lblorder" runat="server" meta:resourcekey="lblorder">
                                                        Order</asp:Label>
                                                </th>
                                                <th>
                                                    <asp:Label ID="lblgroup" runat="server" meta:resourcekey="lblgroup">
                                                        Group By</asp:Label>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td style="width: 26%;">
                                                    <asp:DropDownList ID="ddlSortBy" CssClass="lblTxt form-control"
                                                        runat="server" ToolTip="Select column for sorting"
                                                        Width="175px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 49%;">
                                                    <asp:RadioButtonList ID="rbtnOrder" ToolTip="Sort order"
                                                        CssClass="radioBtnList" runat="server"
                                                        RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="Asc" Text="Asc">Asc</asp:ListItem>
                                                        <asp:ListItem Value="Desc" Text="Desc">Desc</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td style="width: 26%;">
                                                    <asp:DropDownList ID="ddlGroupBy" CssClass="lblTxt form-control"
                                                        runat="server" Width="175px"
                                                        ToolTip="Select column for grouping">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="divtr5" class="ListCust" style="width: 100%">
                                    <div style="height: 2px">
                                    </div>
                                    <table style="width: 100%;">
                                        <tr style="width: 100%; text-align: left;">
                                            <td style="font-weight: bold; width: 30%">
                                                <asp:Label ID="lblsubtotal1" runat="server"
                                                    meta:resourcekey="lblsubtotal1">Select the column to define subtotal
                                                </asp:Label>
                                            </td>
                                            <td
                                                style="font-weight: bold; width: 50%; padding-left: 74px; text-align: left;">
                                                <asp:Label ID="lblsubtotaldtl" runat="server"
                                                    meta:resourcekey="lblsubtotaldtl">Details of subtotal</asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="vertical-align: top; width: 30%">
                                                <asp:Panel ID="pnlSub1" Width="100%" runat="server" CssClass="noHScroll"
                                                    Height="235px" BorderWidth="1px" GroupingText=""
                                                    BorderColor="#B5B0B1">
                                                    <table id="tblSubtotal" style="width: 90%">
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td style="width: 70%; float: left !important; margin-left: 74px;">
                                                <asp:Panel ID="pnlSub2" runat="server" Width="100%" Height="235px"
                                                    HorizontalAlign="Justify">
                                                    <table style="width: 65%">
                                                        <tr>
                                                            <td style="text-align: left;">Header
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtHeader" CssClass="form-control"
                                                                    Width="230px" ToolTip="Header" runat="server">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;">Footer
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtFooter" CssClass="form-control"
                                                                    Width="230px" ToolTip="Footer" runat="server">
                                                                </asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align: left;">Caption
                                                            </td>
                                                            <td>
                                                                <select id="ddlCaption" class="form-control"
                                                                    title="Caption" style="width: 230px">
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"
                                                                style="padding-top: 14px; text-align: left;">
                                                                <asp:CheckBox ID="cbLineSpace" runat="server"
                                                                    ToolTip="Line space after sub total" />
                                                                <asp:Label ID="lbllinespace" runat="server"
                                                                    meta:resourcekey="lbllinespace">Line space after sub
                                                                    total &nbsp; &nbsp;</asp:Label>
                                                                <asp:CheckBox ID="cbTtl" runat="server"
                                                                    ToolTip="Display total with heading" />
                                                                Display total with heading
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div
                                                        style="margin-left: 3px; margin-top: 30px; margin-bottom: 15px;">
                                                        <asp:Label ID="lblsubtotal2" runat="server"
                                                            meta:resourcekey="lblsubtotal2"> Sub total order</asp:Label>
                                                        <asp:TextBox ID="txtSubOrder" runat="server"
                                                            ToolTip=" Sub total order" Style="width: 100px"
                                                            CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <input type="button" value="Create Subtotal" class="hotbtn btn"
                                                        id="btnSUbTotal" onclick="javascript: CreateSubtotal();" />
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2" style="padding-top: 16px; text-align: right;">
                            <asp:Button ID="btnRemove" Text="Remove" runat="server" CssClass="coldbtn btn"
                                ToolTip="Delete view" OnClientClick="return ConfirmDelete();" />
                            <asp:Button ID="btnApply1" Text="Apply" runat="server" CssClass="coldbtn btn"
                                OnClick="btnApply1_Click" ToolTip="Apply changes"
                                OnClientClick="return saveLViewActionCond();" />
                            <asp:Button ID="btnDiscard" Text="Discard" runat="server" Style="margin-left: 7px"
                                CssClass="coldbtn btn" ToolTip="Discard changes"
                                OnClientClick="return discardChanges();" OnClick="btnDiscard_Click" />
                            <asp:Button ID="btnSave" Text="Save" runat="server" Style="margin-left: 5px"
                                CssClass="hotbtn btn" ToolTip="Save view" OnClientClick="return isExistViewName();" />
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="display: none">
            <asp:Button ID="button1" runat="server" />
            <asp:HiddenField ID="hdnColumns" runat="server" />
            <asp:HiddenField ID="hdnNoRepeat" runat="server" />
            <asp:HiddenField ID="hdnStatus" runat="server" Value="false" />
            <asp:HiddenField ID="hdnSort" runat="server" />
            <asp:HiddenField ID="hdnGroup" runat="server" />
            <asp:HiddenField ID="hdnOrder" runat="server" Value="false" />
            <asp:HiddenField ID="hdnSortCap" runat="server" />
            <asp:HiddenField ID="hdnGroupCap" runat="server" />
            <asp:HiddenField ID="hdnNoRepeatCaptn" runat="server" />
            <asp:HiddenField ID="hdnSubTypeCond" runat="server" />
            <asp:HiddenField ID="hdnCondTxt" runat="server" />
            <asp:HiddenField ID="hdnDisplayCond" runat="server" />
            <asp:HiddenField ID="hdnParent" runat="server" />
            <asp:HiddenField ID="hdnChild" runat="server" />
            <asp:HiddenField ID="hdnViewName" runat="server" />
            <asp:HiddenField ID="hdnGridCond" runat="server" />
            <asp:HiddenField ID="hdnSubtotal" runat="server" />
            <asp:HiddenField ID="hdnListViewName" runat="server" />
            <asp:HiddenField ID="hdnSubtotlCaptn" runat="server" />
            <asp:HiddenField ID="hdnSubtotalList" runat="server" />
            <asp:HiddenField ID="hdnViewDelt" runat="server" />
            <asp:HiddenField ID="hdnViewNames" runat="server" />
            <asp:HiddenField ID="hdnSaveMode" runat="server" />
            <asp:HiddenField ID="hdnCbValue" runat="server" />
            <asp:HiddenField ID="hdnSelectedView" runat="server" />
            <asp:HiddenField ID="hdnTransId" runat="server" />
            <asp:HiddenField ID="hdnParentFld" runat="server" />
            <asp:Button ID="btnListView" runat="server" OnClick="btnListView_Click" />
            <asp:Button ID="btnRemv" runat="server" OnClick="btnRemv_Click" />
            <asp:Button ID="btnSav" runat="server" OnClick="btnSav_Click" />
            <asp:Button ID="btnShowGrid" runat="server" OnClick="btnShowGrid_Click" />
            <table id="tblSubtotlList" runat="server">
            </table>



        </div>
        <%=viewList%><%=findList%><%=tstScripts%><%=enableBackForwButton%>
        <div class="divlist">
            <asp:HiddenField ID="rXml" runat="server" Value="" />
            <asp:HiddenField ID="param" runat="server" Value="" />
            <asp:HiddenField ID="paramxml" runat="server" Value="" />
            <input id="recordid000F0" type="hidden" name="recordid000F0" value="0" />
            <asp:HiddenField ID="hdnAction" runat="server" />
        </div>
        <div id='waitDiv' style='display: none;'>
            <div id='backgroundDiv'
                style='background: url(../Axpimages/loadingBars.gif) center center no-repeat rgba(255, 255, 255, 0.4); background-size: 135px;'>
            </div>
        </div>
        <asp:HiddenField ID="hdnTreeSelVal" runat="server" />
        <asp:Label ID="lblNodataServer" runat="server" meta:resourcekey="lblNodataServer" Visible="false">No data found.
        </asp:Label>
        <script type="text/javascript">
            var hdnTree = document.getElementById("hdnTreeSelVal").value;
        </script>

        <asp:Label ID="lblTaskExcel" runat="server" meta:resourcekey="lblTaskExcel" Style="display: none">Excel
        </asp:Label>
        <asp:Label ID="lblTaskPDF" runat="server" meta:resourcekey="lblTaskPDF" Style="display: none">PDF</asp:Label>
    </form>
</body>

</html>