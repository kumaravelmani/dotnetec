﻿Imports System.Configuration
Imports System.Security.Principal
Imports System.Web.SessionState
Imports System.Diagnostics
Imports System.Xml
Imports Axpert_Object
Imports System.Collections
Imports System.Data
Imports System.Threading
Imports System.IO
Imports System.Text.RegularExpressions


Partial Class Signin
    Inherits System.Web.UI.Page
    Public appName As String
    Public appTitle As String
    Public target As String = String.Empty
    Public errmsg As String = String.Empty
    Public ipAddress As String = String.Empty
    Public axProjs As String = String.Empty
    Public axLang As String
    Public showLang As Boolean = False
    Public axLangs As New StringBuilder
    Public ousers As String
    Public project As String
    Public EnableOldTheme As String
    Public IsLogoAvail As Boolean = False
    Public copyRightTxt As String = String.Empty
    Public strLanguage As String = String.Empty
    Public isMobileDevice = False
    Dim util As Util.Util
    Dim logobj As LogFile.Log = New LogFile.Log()
    ''' <summary>
    ''' onload validating the single sign on and calling portal main page or normal main page based on the value in the web.config file.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Public direction As String = "ltr"
    Public langType As String = "en"
    Protected Overrides Sub InitializeCulture()
        util = New Util.Util()
        If Not ConfigurationManager.AppSettings("proj") Is Nothing Then
            If ConfigurationManager.AppSettings("proj").ToString() <> String.Empty Then
                Dim lngproj As String = LoadLanguages()
                Dim dirLang As String = String.Empty
                dirLang = util.SetCulture(lngproj.Split(",")(0).ToUpper)
                If Not String.IsNullOrEmpty(dirLang) Then
                    direction = dirLang.Split("-")(0)
                    langType = dirLang.Split("-")(1)
                End If
                Dim filcustom As FileInfo
                filcustom = New FileInfo(HttpContext.Current.Server.MapPath("~/Js/lang/content-" + langType + ".js"))
                If Not (filcustom.Exists) Then
                    direction = "ltr"
                    langType = "en"
                End If
            End If
        End If
        If util.fBrowserIsMobile() Then
            isMobileDevice = True
        End If
    End Sub
    Protected Sub Pre_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If HttpContext.Current.Request.Url.AbsolutePath.ToLower().IndexOf(Request.FilePath.ToLower() + "/") > -1 Then
            Response.Redirect("~/CusError/AxCustomError.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        util = New Util.Util()
        If Session("project") IsNot Nothing Then
            If util.CheckLoggedUserDetails(Session("project"), Session.SessionID) = True Then
                Dim wsObj As ASB.WebService = New ASB.WebService()
                wsObj.SignOut()
            End If
        End If

        If ConfigurationManager.AppSettings("HomeSessExpriesUrl") IsNot Nothing And ConfigurationManager.AppSettings("HomeSessExpriesUrl").ToString() <> "" And Session("AxCloudDB") IsNot Nothing Then
            Dim url As String
            Session("AxCloudDB") = Nothing
            url = ConfigurationManager.AppSettings("HomeSessExpriesUrl")
            Response.Redirect(url)
        End If

        main_body.Attributes.Add("Dir", direction)
        Dim folderPath As String = Server.MapPath("~/images/Custom")
        Dim di As New IO.DirectoryInfo(folderPath)
        Dim diFileinfo As IO.FileInfo() = di.GetFiles()
        Dim drfile As IO.FileInfo
        Dim customlogoexist = "False"
        Dim custommoblogoexist = "False"
        Dim Ismobile = Request.Browser.IsMobileDevice
        If Ismobile Then
            For Each drfile In diFileinfo
                '  Dim filename As String = drfile.ToString
                If drfile.Length > 0 AndAlso drfile.Name.Contains("homelogo_mob") Then
                    main_body.Attributes.CssStyle.Add("background", "url(./../images/Custom/" + drfile.Name + "?v=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ")no-repeat center center fixed !important")
                    main_body.Attributes.CssStyle.Add("background-size", "cover !important")
                    main_body.Attributes.CssStyle.Add("height", "100vh !important")
                    custommoblogoexist = "True"
                    Exit For
                End If
            Next
            If custommoblogoexist = "False" AndAlso Ismobile Then
                main_body.Attributes.CssStyle.Add("background", "url(./../AxpImages/homelogo_mob.jpg) no-repeat")
                main_body.Attributes.CssStyle.Add("background-size", "cover !important")
                main_body.Attributes.CssStyle.Add("height", "100vh !important")
            End If
        Else
            For Each drfile In diFileinfo
                '  Dim filename As String = drfile.ToString
                If drfile.Length > 0 AndAlso drfile.Name.Contains("homelogo") Then
                    If drfile.Name.Contains("mp4") Then
                        main_body.Attributes.CssStyle.Add("background", "")
                        bgvid.Attributes.CssStyle.Add("display", "block")
                        bgvidsource.Attributes.Add("src", "./../images/Custom/homelogo.mp4?v=" + DateTime.Now.ToString("yyyyMMddHHmmss") + "")
                        customlogoexist = "True"
                        Exit For
                    Else
                        main_body.Attributes.CssStyle.Add("background", "url(./../images/Custom/" + drfile.Name + "?v=" + DateTime.Now.ToString("yyyyMMddHHmmss") + ") no-repeat center center fixed !important ")
                        main_body.Attributes.CssStyle.Add("background-size", "cover !important")
                        main_body.Attributes.CssStyle.Add("height", "100vh !important")
                        customlogoexist = "True"
                        Exit For
                    End If

                End If
            Next
            If customlogoexist = "False" Then
                main_body.Attributes.CssStyle.Add("background", "url(./../AxpImages/homelogo.jpg) center center no-repeat fixed !important")
                main_body.Attributes.CssStyle.Add("background-size", "cover !important")
            End If
        End If

        EnableOldTheme = util.enableOldTheme
        Dim proj As String = String.Empty

        Dim axpertVersion As String = ""

        If Not ConfigurationManager.AppSettings("proj") Is Nothing Then
            If ConfigurationManager.AppSettings("proj").ToString() <> String.Empty Then
                proj = ConfigurationManager.AppSettings("proj").ToString()
            End If
        End If

        If Not ConfigurationManager.AppSettings("axpertVersion") Is Nothing Then
            If ConfigurationManager.AppSettings("axpertVersion").ToString() <> String.Empty Then
                axpertVersion = "Version " + ConfigurationManager.AppSettings("axpertVersion").ToString()
            End If
        End If

        axpertVer.InnerText = axpertVersion

        If Session("AxCopyRightText") <> Nothing Then
            copyRightTxt = Session("AxCopyRightText").ToString()
        ElseIf proj IsNot String.Empty Then
            copyRightTxt = util.GetConfigAttrValue(proj, "AxCopyRightText")
        End If

        copyRightTxt = copyRightTxt.Replace("#br#", "<br/>")

        If copyRightTxt <> String.Empty Then
            dvCopyRight.InnerText = copyRightTxt
        End If

        axLang = Convert.ToString(Session("AxLanguages"))

        If axLang = String.Empty Then
            axLang = LoadLanguages()
        End If

        If (String.IsNullOrEmpty(axLang)) Then
            axLangs.Append("ENGLISH")
        Else
            Dim strLang = axLang.Split(",")
            If (strLang.Length > 1) Then
                If (strLang(1).ToString() <> "") Then
                    showLang = True
                    axLangs.Append(axLang)
                End If
            End If
        End If

        axLangFld.Style.Add("display", "none")

        IsLogoAvail = util.IsLogoExists()

        'To show the number of online users

        If proj IsNot String.Empty Then
            selectProj.Style.Add("display", "none")
            axSelectProj.Value = proj
            Session("AxCPWDOnLogin") = util.GetConfigAttrValue(proj, "AxCPWDOnLogin")
        End If

        If Not axLangs.ToString() = String.Empty Then
            hdnLangs.Value = axLangs.ToString()
        Else
            showLang = False
            hdnLangs.Value = axLang.ToString()
        End If

        If Not ConfigurationManager.AppSettings("proj") Is Nothing Then
            If (ConfigurationManager.AppSettings("proj").ToString() = String.Empty) Then
                axProjs = util.CheckForAvailableProjects()
            Else
                Session("project") = ConfigurationManager.AppSettings("proj").ToString()
                project = ConfigurationManager.AppSettings("proj").ToString()
            End If
        Else
            axProjs = util.CheckForAvailableProjects()
        End If
        ' For SQL INJECTION
        Dim isProjValid As Boolean = util.IsProjectValid(axProjs)
        If Not isProjValid Then
            Response.Redirect(Constants.LOGINERR)
        End If

        hdnAxProjs.Value = axProjs

        If project = String.Empty Then

            axProjs = util.CheckForAvailableProjects()

            If util.IsValidQueryString(Request.RawUrl) = False Then
                Response.Redirect(util.ERRPATH + Constants.INVALIDURL)
            End If
            ' For SQL INJECTION
            isProjValid = util.IsProjectValid(axProjs)
            If Not isProjValid Then
                Response.Redirect(Constants.LOGINERR)
            End If

            hdnAxProjs.Value = axProjs

            If Request.Cookies().Count > 0 Then
                If Not Request.Cookies().Item("project") Is Nothing And Session("project") Is Nothing Then
                    If Not Request.Cookies().Item("project").Value.ToString() = String.Empty Then
                        If Not util.IsProjectValid(Request.Cookies().Item("project").Value.ToString()) Then
                            Response.Redirect(lblCustomerror.Text)
                        End If
                        Session("project") = Request.Cookies().Item("project").Value
                    End If
                End If
            End If
        Else
            hdnAxProjs.Value = project
        End If

        Response.ExpiresAbsolute = DateTime.Now
        Response.Expires = -1441
        Response.CacheControl = "no-cache"
        Response.AddHeader("Pragma", "no-cache")
        Response.AddHeader("Pragma", "no-store")
        Response.AddHeader("cache-control", "no-cache")
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoServerCaching()
        If (util.CheckCrossScriptingInString(Request.QueryString("msg"))) Then
            Response.Redirect("~/CusError/AxCustomError.aspx")
        Else
            errmsg = Request.QueryString("msg")
        End If
        If (errmsg <> "") Then
            If errmsg.Contains("Your session is expired") Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallShowAlertDialog", "showAlertDialog(""error"",4049,""client"");", True)
            Else
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallShowAlertDialog", "showAlertDialog(""error"",""" + errmsg + """);", True)
            End If
        Else
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallHideAlertDialog", "hideAlertDialog("""");", True)
        End If

        ipAddress = Request.UserHostAddress

        GenerateSeed()

        'ValidateProject()

        Session("menustr") = ""
        Dim OldID As String = Session.SessionID
        'Commented as double time session change is not required
        'Dim Manager As New SessionIDManager()
        'Context.Session.Abandon()
        'Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
        'Manager.RemoveSessionID(Context)
        'Dim NewID As String = Manager.CreateSessionID(Context)
        'Dim redirected As Boolean = False
        'Dim IsAdded As Boolean = False
        'Manager.SaveSessionID(Context, NewID, redirected, IsAdded)
        Dim isSSO As String = String.Empty
        If Session("AxSSO") IsNot Nothing Then
            isSSO = Session("AxSSO").ToString()
        End If
        If isSSO = "true" Then
            Dim winID As IIdentity
            winID = HttpContext.Current.User.Identity
            If (winID.IsAuthenticated = True) Then
                Session("validated") = ""
                target = ConfigurationManager.AppSettings("portal")
                If target.ToString().ToLower() = "true" Then
                    target = "mainp.aspx"
                Else
                    target = util.MAINPATH
                End If

            Else
            End If
        Else

            target = util.MAINPATH

        End If
        Session("validated") = ""

        Session("seed") = Nothing
        Dim random As New Random()
        Dim i As Integer = random.Next(1, 9999)
        Session("seed") = i.ToString()

        Dim token As String = Guid.NewGuid().ToString()
        Response.Cookies.Add(New HttpCookie("CSRFToken", token))
        Session("CSRFToken") = token

        If (Application("AxpString1") <> Nothing) Then
            Dim errMsg As String = Application("AxpString1").ToString()

            If errMsg.StartsWith("<e>") Then
                errMsg = errMsg.Replace("<e>", "")
                errMsg = errMsg.Replace("</e>", "")
                Dim loginPath As String = Application("LoginPath").ToString()
                If (errMsg <> "") Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallShowAlertDialog", "showAlertDialog(""error"",""" + errMsg + """);", True)
                Else
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallHideAlertDialog", "hideAlertDialog("""");", True)
                End If
            End If
        End If

        If Not IsPostBack Then
            If Request.QueryString("proj") IsNot Nothing Then
                proj = Convert.ToString(Request.QueryString("proj"))
                If proj <> "undefined" AndAlso proj <> "" AndAlso proj.ToLower() <> "select project" Then
                    axSelectProj.Value = proj
                End If
            End If
        End If
    End Sub


    Private Sub GenerateSeed()
        ' Generate two random numbers
        Dim sLeft : Dim sRight
        sLeft = CStr(Int(Rnd() * 99999)) : If Len(sLeft) < 5 Then sLeft = New String("0", 5 - Len(sLeft)) & sLeft
        sRight = CStr(Int(Rnd() * 99999)) : If Len(sRight) < 5 Then sRight = New String("0", 5 - Len(sRight)) & sRight

        ' Concatenate the two parts divided by a '.' (dot) to create the seed
        Dim sSeed
        sSeed = sLeft & "." & sRight

        ' Store the seed in the Session object for later usage
        Session("auth_seed") = sSeed
    End Sub

    Function LoadLanguages() As String
        If Not ConfigurationManager.AppSettings("proj") Is Nothing Then
            If ConfigurationManager.AppSettings("proj").ToString() <> String.Empty Then
                Dim proj As String = ConfigurationManager.AppSettings("proj").ToString()
                Return util.GetConfigAttrValue(proj, "AxLanguages")
            End If
        End If
        Return String.Empty

    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCPOFL(ByVal name As String) As String
        Dim utilObj As New Util.Util()
        HttpContext.Current.Session("AxCPWDOnLogin") = utilObj.GetConfigAttrValue(name, "AxCPWDOnLogin")

        Return String.Empty
    End Function

    <System.Web.Services.WebMethod()>
    Public Shared Function GetCurrLang(ByVal name As String) As String
        Dim utilObj As New Util.Util()
        Return utilObj.GetConfigAttrValue(name, "AxLanguages")
    End Function
    <System.Web.Services.WebMethod()>
    Public Shared Function GetCurrAppTitle(ByVal name As String) As String
        Dim utilObj As New Util.Util()
        Return utilObj.GetConfigAttrValue(name, "AxAppTitle")
    End Function


    <System.Web.Services.WebMethod()>
    Public Shared Function GetCurrCopyRightTxt(ByVal name As String, ByVal lang As String) As String
        Dim utilObj As New Util.Util()
        Return utilObj.GetConfigAttrValue(name, "AxCopyRightText", lang)
    End Function

    Private Sub ValidateProject()
        If Not Application("Proj") Is Nothing Then
            If (Application("Proj") = String.Empty) Then
                errmsg = 4001
                'lblErrorMsg.InnerText = errmsg
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "CallShowAlertDialog", "showAlertDialog(""error""," + errmsg + ",""client"");", True)
            End If
        End If
    End Sub

End Class
