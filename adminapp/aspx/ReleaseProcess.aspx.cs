﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class aspx_ReleaseProcess : System.Web.UI.Page
{
    Util.Util util;
    public string direction = "ltr";

    public string langType = "en";
    protected override void InitializeCulture()
    {
        if (Session["language"] != null)
        {
            util = new Util.Util();
            string dirLang = string.Empty;
            dirLang = util.SetCulture(Session["language"].ToString().ToUpper());
            if (!string.IsNullOrEmpty(dirLang))
            {
                direction = dirLang.Split('-')[0];
                langType = dirLang.Split('-')[1];
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void btnVersion_Click(object sender, EventArgs e)
    {
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Server.MapPath("../aspx"));
        System.IO.FileInfo[] diFileinfo = di.GetFiles();
        string newVersion = string.Empty;
        string oldVersion = string.Empty;

        if (txtNewVersion.Text != "")
            newVersion = txtNewVersion.Text;
        if (txtOldVersion.Text != "")
            oldVersion = txtOldVersion.Text;

        string modFileName = "";
        modFileName = txtFilename.Text;

        for (int i = 0; i < diFileinfo.Length; i++)
        {
            string text = File.ReadAllText(diFileinfo[i].FullName);
            string oldText = text;
            if (txtFilename.Text != "")
                text = text.Replace(txtFilename.Text+ "?v=" + oldVersion, txtFilename.Text + "?v=" + newVersion);
            else
                text = text.Replace(".min.js?v=" + oldVersion, ".min.js?v=" + newVersion);
            if (oldText != text)
                File.WriteAllText(diFileinfo[i].FullName, text);
        }       
    }
}
