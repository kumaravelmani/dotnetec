<%@ Page Language="C#" AutoEventWireup="true" CodeFile="widgetBuilder.aspx.cs" Inherits="aspx_widgetBuilder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Widget Builder</title>
    <meta charset="utf-8">
    <meta name="description" content="Axpert file uploader">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">

    <link href="../ThirdParty/materialize/css/materialize.min.css?v=1" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <link href="../Css/codemirror.css" rel="stylesheet" />
    <link href="../ThirdParty/bgrins-spectrum/spectrum.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link rel="stylesheet" href="../Css/wizardComp.min.css?v=4">
    <link href="../Css/widgetBuilder.min.css?v=15" rel="stylesheet">
    <link rel="stylesheet" href="../Js/codemirror/addon/hint/show-hint.css">
    <link rel="stylesheet" href="../Css/AXpagination.min.css?v=1">
    <link href="../ThirdParty/DataTables-1.10.13/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="" id="homeBuilderLink" rel="stylesheet" />

    <link id="themecss" type="text/css" href="" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script>
        var globalVars = '<%=globalVars%>';
        var appsessionKey = '<%=appsessionKey%>';
        var appsessionKeyWOL = '<%=appsessionKeyWOL%>';
        var tracePath = '<%=traceLog%>';
        var dbType = '<%=Session["axdb"]%>';
        var AxwizardType = '<%=axWizardType%>';
    </script>
</head>
<body class="btextDir-<%=direction%>" dir="<%=direction%>">
    <div class="hpbHeaderTitle ">
        <span class="icon-home2"></span>
        <!-- <i class="material-icons">border_all</i> -->
        <span class="title" id="lblWidgetBuilder">Widget Builder</span>
        <div class="clear"></div>
    </div>
    <div class="pagination-wrapper right">
    </div>
    <div class="clear"></div>
    <div class="col s12" style="position: relative; height: 28px; margin-bottom: 30px; text-align: center;">
        <div id="main-search-wrapper" class="card">
            <a class="dropdown-button btn themeButton cutsomColorbtn" title="Search All" id="matcolumnSrchBtn" data-activates="matcolumnSearch" href="javascript:void(0)">All</a>
            <ul id="matcolumnSearch" class="dropdown-content">
                <li data-id="all"><a href="#!">All</a></li>
                <li data-id="kpi"><a href="#!">KPI</a></li>
                <li data-id="chart"><a href="#!">Chart</a></li>
                <li class="ivCreationRelated" data-id="iview"><a href="#!">Iview</a></li>
            </ul>
            <!-- Dropdown Structure -->
            <input type="hidden" name="searchSelectColumn" id="searchSelectColumn">
            <span id="mainSearchFldSpn">
                <input class="mainColSrchFld" placeholder="Search" id="mainSearchFld"><i class="icon-magnify themeTextColor searchIcon">search</i>
            </span>
        </div>
        <div id="mainActionBtnsWrapper">
            <button title="List View" onclick="createDatatable('new')" id="gridWdgtToggleBtn" class="waves-effect waves-light btn wbtn icobtn cutsomColorbtn themeButton">
                <span class="icon-list"></span>
            </button>
            <button title="Create Widget" data-target="wizardModal" class="waves-effect waves-light btn wbtn icobtn cutsomColorbtn themeButton">
                <span class="icon-plus modal-trigger"></span>
            </button>
            <!-- <span class="cutsomColorbtn waves-effect waves-light btn wbtn icobtn listPlus" onclick="javascript:void(0);">
                 <a class="icon-plus wIcon modal-trigger" href="#wizardModal" onclick="javascript:void(0);"></a> 
             </span> -->
        </div>
    </div>
    <div class="col s12">
        <div class="row" id="widgetPanelWrapper">
            <!-- <div id="dfWrapper" data-type="chart" class="col s12 m4 l3 widgetWrapper">
                <div class="card hoverable ">
                    <div class="cardTitleWrapper"><i class="icon-paper-plane"></i><span class="cardTitle">mani</span>
                        <div class="clear"></div>
                    </div>
                    <div class="cardContentMainWrapper">
                        <button type="button" title="Delete Widget" class="cardTitleClear titleLessCardTitleClear  red waves-effect btn-floating right"><i class="icon-cross2"></i></button>
                        <div class="card-content chart">
                            <div class="cardContentData"></div>
                            <div class="cardContentLoader valign-wrapper">
                                <div class="preloader-wrapper small active">
                                    <div class="spinner-layer spinner-green-only">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="gap-patch">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        <div style="display: none;" id="widgetDataTableWrapper">
            <table width="100%" class="themeTable" id="wdgtDataTable">
                <thead class='gridThemeHeader'>
                    <tr>
                        <th style="max-width: 70px;"></th>
                        <th style="width: 20%">Title</th>
                        <th style="width: 20%">Widget Type</th>
                        <th>Sql</th>
                    </tr>
                </thead>
                <tbody class="gridThemeBody"></tbody>
            </table>
        </div>
    </div>
    <%--<div class="pagination-wrapper right">
    </div>--%>
    <div class="clear"></div>
    <!-- propertySheet data -->
    <div class="col s3 card hoverable scale-transition hide scale-out" id="propertySheet">
        <div class="hpbHeaderTitle themeDarkenBackground">
            <span class="icon-paint-roller"></span>
            <span class="title">Property Sheet</span>
            <button title="Close" type="button" id="propertySrchCls" onclick="closeProprtySht();" class="btn-flat waves-effect btn-floating right"><i class="icon-cross2"></i></button>
        </div>
        <div id="propertySheetDataWrapper">
            <div id="prpShtUpdateInfo">
                <span style="margin-right: 5px;">
                    <input type="checkbox" id="prpShtAutoSave" />
                    <label for="prpShtAutoSave">Auto Save</label>
                </span>
                <div class="right upDwnBtnWrapper">
                    <button title="Save" type="button" id="updateWidget" onclick="updateDiscardPropSheet('update')" class="waves-effect waves-teal btn cutsomColorbtn themeButton"><span class="icon-check"></span></button>
                    <button title="Revert" onclick="updateDiscardPropSheet('revert')" type="button" id="refreshWidget" class="waves-effect waves-teal btn cnfrmBxColdBtn "><span class="icon-undo2"></span></button>
                    <!-- <input type="hidden" id="widgetAttribute" value="&quot;{\&quot;tablehlinks\&quot;:\&quot;h1=tempsa\&quot;}&quot;"> -->
                </div>
            </div>
            <div class="clear"></div>
            <div id="propertySearch">
                <input placeholder="Search..." type="text" id="propertySearchFld" class="normalFld searchFld">
                <span class="srchIcn icon-magnifier"></span>
            </div>
            <div class="posr" id="propTableContent">
                <table class="bordered">
                    <tr>
                        <td class="subHeading themeTextColor" colspan="2">General <span data-target="general" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr data-group="general">
                        <td>Component</td>
                        <td id="prpShtComponent">Tstruct</td>
                    </tr>
                    <tr class="prpShtTitleRelated" data-group="general">
                        <td>Title</td>
                        <td>
                            <input onkeyup="changeTheValue(this,'title')" type="text" maxlength="50" value="" class="normalInpFld ivDisabledFld" id="prpShtTitle">
                        </td>
                    </tr>
                    <tr class="prpSteIvRelated" data-group="general">
                        <td>Iview Name</td>
                        <td>
                            <input type="text" disabled value="" class="normalInpFld" id="prpShtIvName">
                        </td>
                    </tr>

                    <tr class="prpSteNonIvRelated" id="prpShtApprRelated">
                        <td class="subHeading themeTextColor" colspan="2">Appearance <span data-target="appr" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr id="prpShtGrpWrapper" class="prpSteNonIvRelated" data-group="appr">
                        <td>Group</td>
                        <td>
                            <div class="input-field col s12">
                                <select onchange="updateTmpObjBeforeSave('group', this)" class="materialSelect">
                                    <option value="" disabled selected>Choose your option</option>
                                    <!-- <option value="1">Option 1</option>
                                    <option value="2">Option 2</option>
                                    <option value="3">Option 3</option> -->
                                </select>
                                <!-- <label>Materialize Select</label> -->
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpShtShwValFrPWrapper">
                        <td>Show value in label</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('pieShwVal',this,'chart')" type="checkbox" id="prpShtShwValForPie">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpShtShwLgndWrapper">
                        <td>Show Legend</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('shwLgnd',this,'chart')" type="checkbox" id="prpShtShwLgnd">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpShtShwChrtValWrapper">
                        <td>Show Chart Value</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('shwChartVal',this,'chart')" type="checkbox" id="prpShtShwChrtVal">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpSht3DChrtWrapper">
                        <td>3D</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('3d',this,'chart')" type="checkbox" id="prpSht3DChrt">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpShtGradientChrtWrapper">
                        <td>Gradient Color</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('gradient',this,'chart')" type="checkbox" id="prpShtGradientChrt">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" class="prpSteNonIvRelated" id="prpShtkpiTtlWrapper">
                        <td>Show Title</td>
                        <td>
                            <div class="switch">
                                No
                                <label>
                                    <input onchange="chnageKpiChartView('kpttl',this,'kpi')" type="checkbox" id="prpShtshwKpiTtl">
                                    <span class="lever"></span>
                                </label>
                                Yes
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" id="gradientPickerWrapper" class="commonCloseOnOpen">
                        <td>Gradient Color</td>
                        <td>
                            <div id="gradientPicker" class="colorMe blue center-align">
                                <a class="colorMeA white-text" data-color='blue' onclick="gradientClick('colorClick')" href="javascript:void(0)">blue  
                                </a>
                            </div>
                            <div class="gradientPalletWrapper" style="display: none;">
                                <div class="pallet">
                                    <div class="colorMe blue palletIns">
                                        <a title="Blue" onclick="gradientClick('colorPick','blue')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe red palletIns">
                                        <a title="Red" onclick="gradientClick('colorPick','red')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe yellow palletIns">
                                        <a title="Yellow" onclick="gradientClick('colorPick','yellow')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe green palletIns">
                                        <a title="Green" onclick="gradientClick('colorPick','green')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe purple palletIns">
                                        <a title="Purple" onclick="gradientClick('colorPick','purple')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe multiBlue palletIns">
                                        <a title="Multiple Blue" onclick="gradientClick('colorPick','multiBlue')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                                <div class="pallet">
                                    <div class="colorMe multiLime palletIns">
                                        <a title="Multiple Lime" onclick="gradientClick('colorPick','multiLime')" class="colorMeA" href="javascript:void(0)"></a>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr data-group="appr" id="chartColorPickerWrapper">
                        <td>Color Palette</td>
                        <td>
                            <div id="chartColorPicker" class="center-align">
                                <!-- <a  href="javascript:void(0)">Mani</a> -->
                                <!-- <button onclick="chartColorChanger('colorClick')" class="chartColorWrapper mainSel waves-effect waves-light btn-flat">
                                    
                                </button> -->
                                <div id="colorSelector" class="posr">
                                    <div class="palletMinatureWrapper"></div>
                                    <div class="palletSelectFld">
                                    </div>
                                </div>

                                <input type="hidden" id="ifCustomColorCodes">
                            </div>
                            <div id="chartCustomColors" style="display: none">
                            </div>

                        </td>
                    </tr>
                    <tr class="prpSteNonIvRelated" id="prpShtHeadingSrc">
                        <td class="subHeading themeTextColor" colspan="2">Source <span data-target="src" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr class="prpSteNonIvRelated" id="prpShtTxtBxWrapper" data-group="src">
                        <td>SQL</td>
                        <td>
                            <input type="text" value="" data-key="sqlDt" onfocus="createCustomTxtBx(this)" class="normalInpFld" id="prpShtTxtBx">
                        </td>
                    </tr>
                    <tr class="prpShtAuthRelated">
                        <td class="subHeading themeTextColor" colspan="2">Authorization <span data-target="auth" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr class="prpShtAuthRelated" data-group="auth">
                        <td>Responsibility</td>
                        <td id="prpShtRolesWrappperTd">
                            <div class="input-field col s12">
                                <select multiple id="prpShtRoles">
                                    <!-- <option selected value="0">All</option> -->
                                    <option disabled value="0">Please wait...</option>
                                </select>
                            </div>
                        </td>
                    </tr>
                    <tr class="prpSteNonIvRelated" id="prpShtHeadingAPI">
                        <td class="subHeading themeTextColor" colspan="2">API <span data-target="api" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr class="prpSteNonIvRelated" data-group="api" id="prpShtAPIswitchTr">
                        <td>Is Public</td>
                        <td>
                            <div class="switch">
                                <label>
                                    Off
                                    <input id="prpShtAPIswitch" onchange="chnageKpiChartView('exposeAPI',this,'exposeAPI')" type="checkbox">
                                    <span class="lever"></span>On
                                </label>
                            </div>
                        </td>
                    </tr>
                    <tr class="prpSteNonIvRelated" data-group="api" class="commonCloseOnOpen" id="prpShtAPIuriTr">
                        <td>URI</td>
                        <td>
                            <div id="prpApiURI" style="width: 144px; word-wrap: break-word;"></div>
                        </td>
                    </tr>
                    <!-- <tr>
                        <td class="subHeading" colspan="2">Activity Log <span data-target="log" class="propShtDataToggleIcon icon-chevron-up"></span></td>
                    </tr>
                    <tr data-group="log">
                        <td>Modified By</td>
                        <td id="prpShtModifiedBy"></td>
                    </tr>
                    <tr data-group="log">
                        <td>Modified On</td>
                        <td id="prpShtModifiedOn"></td>
                    </tr> -->
                </table>
            </div>
        </div>
    </div>
    <div id="customTxtAreaWrapperBg" style="display: none;">
        <div id="customTxtAreaWrapper">
            <!-- <button id="customTxtAreaWrapperCls" type="button" class="btn-flat waves-effect btn-floating right"><i class="icon-cross2"></i></button> -->
            <textarea tabindex="-1" name="" id="" cols="30" rows="10"></textarea>
            <button id="sqlEdtExmpleBtn" class="waves-effect btn-flat">Example <span class="arrwCls icon-chevron-down"></span></button>
            <div id="sqlEdtExData">Select data_label, x-axis_label, value from Tabel1 order by label,data_label</div>
            <div id="customTxtAreaFooter" style="display: none;" class="right-align">
                <button title="save" class="cutsomColorbtn themeButton save waves-effect waves-light btn"><span class="icon-check"></span></button>
                <button title="cancel" class="cancel waves-effect waves-light btn"><span class="icon-cross2"></span></button>
            </div>
        </div>
    </div>
    <!-- wizard component -->
    <div id="wizardModal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <div id="wizarTypeSel">
                <h5>Select Widget Type</h5>
                <div class="container">
                    <div class="row marg0">
                        <div class="col s12 center iconWrapper">
                            <div class="valign-wrapper">
                                <div class="center" style="width: 100%;">
                                    <a title="Create Chart" onclick="widgetType='chart';wizardObj.createWizard('chartWizard'); $('#wizardPrevbtn').attr('onclick','wizardObj.resetTheWizard()').show();" class="iconAnchr" href="javascript:void(0)"><span class="icon-pie-chart"></span>
                                        <p>Chart</p>
                                    </a>
                                    <a title="Create KPI" onclick="widgetType='kpi';wizardObj.createWizard('kpiWizard'); $('#wizardPrevbtn').attr('onclick','wizardObj.resetTheWizard()').show();" class="iconAnchr" href="javascript:void(0)"><span class="icon-chart-growth"></span>
                                        <p>Kpi</p>
                                    </a>
                                    <a title="Create Iview" onclick="widgetType='iview';wizardObj.createWizard('iviewWizard'); $('#wizardPrevbtn').attr('onclick','wizardObj.resetTheWizard()').show();" class="iconAnchr ivCreationRelated" href="javascript:void(0)"><span class="icon-grid"></span>
                                        <p>Iview</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="display: none;" id="wizardWrappper">
                <div id="wizardHeader">
                </div>
                <div id="wizardBodyContent">
                    <div class="wizardContainer" id="iviewType">
                        <div class="valign-wrapper">
                            <div class="center" style="width: 100%;">
                                <a title="Classic" onclick="changeIvType(this,'Classic')" class="iconAnchr hoverFocusThemeColor" href="javascript:void(0)"><span class="icon-grid"></span>
                                    <p>Classic</p>
                                </a>
                                <a title="Interactive" onclick="changeIvType(this,'Interactive')" class="iconAnchr hoverFocusThemeColor" href="javascript:void(0)"><span class="icon-layers"></span>
                                    <p>Interactive</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="ivColData">
                    </div>
                    <div class="wizardContainer" id="ivParams">
                        <div class="container" id="ivParamMainWrapper">
                            <div class="row ivEchPramWrapper">
                                <div class="col m2 right-align">
                                    <button title="Move Up" onclick="moveIvParamUpDwn(this,'up')" class="waves-effect  customSmallBtn waves-light btn-flat"><span class="icon-chevron-up"></span></button>
                                    <button title="Move Down" onclick="moveIvParamUpDwn(this,'down')" class="waves-effect customSmallBtn waves-light btn-flat"><span class="icon-chevron-down"></span></button>
                                    <button title="Edit" onclick="createExtraModel({type:'param',details:this})" class="waves-effect customSmallBtn waves-light btn-flat"><span class="icon-pencil"></span></button>
                                </div>
                                <div class="col m8">
                                    <div class="input-field col s4">
                                        <input maxlength="10" type="text" class="themeMaterialInp pName">
                                        <label>Name</label>
                                    </div>
                                    <div class="input-field col s4 slctFld">
                                        <select class="materialSelect pType">
                                            <!-- <option value="" disabled selected>Choose data type</option> -->
                                            <option value="Character" selected>Character</option>
                                            <option value="Date/Time">Date/Time</option>
                                            <option value="Numeric">Numeric</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s4">
                                        <input type="text" class="themeMaterialInp pVal">
                                        <label>Value</label>
                                    </div>
                                </div>
                                <div class="col m2">
                                    <button title="Add" onclick="addDeleteIvParamRow(this,'add')" class="waves-effect waves-light customSmallBtn btn-flat"><span class="icon-plus"></span></button>
                                    <button title="Remove" onclick="addDeleteIvParamRow(this,'delete')" class="waves-effect waves-light customSmallBtn btn-flat"><span class="icon-cross2"></span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="chartSelData">
                        <div class=" chartImgWrapper">
                            <div class="row">
                                <div class="col m2 marg0">
                                    <a title="Area" href="javascript:void(0)" data-cc="3columns" data-type="area">
                                        <div class="card hoverable">
                                            <span class="selectedIcon themeDarkenBackground darkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/area.PNG" alt="">
                                        </div>
                                        <p>Area</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Line" href="javascript:void(0)" data-cc="3columns" data-type="line">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/line.PNG" alt="">
                                        </div>
                                        <p>Line</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Pie" href="javascript:void(0)" data-cc="2columns" data-type="pie">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/pie.PNG" alt="">
                                        </div>
                                        <p>Pie</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Semi Donut" href="javascript:void(0)" data-cc="2columns" data-type="semi-donut">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/semi-donut.PNG" alt="">
                                        </div>
                                        <p>Semi Donut</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Donut" href="javascript:void(0)" data-cc="2columns" data-type="donut">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/donut.PNG" alt="">
                                        </div>
                                        <p>Donut</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Bar" href="javascript:void(0)" data-cc="3columns" data-type="bar">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/bar.PNG" alt="">
                                        </div>
                                        <p>Bar</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Stacked Bar" href="javascript:void(0)" data-cc="3columns" data-type="stacked-bar">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/stacked-bar.PNG" alt="">
                                        </div>
                                        <p>Stacked Bar</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Column" href="javascript:void(0)" data-cc="3columns" data-type="column">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/column.PNG" alt="">
                                        </div>
                                        <p>Column</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Stacked Column" href="javascript:void(0)" data-cc="3columns" data-type="stacked-column">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/stacked-column.PNG" alt="">
                                        </div>
                                        <p>Stacked Column</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Stacked Grouped Column" href="javascript:void(0)" data-cc="4columns" data-type="stacked-group-column">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/stacked-grouped-column.PNG" alt="">
                                        </div>
                                        <p>Stacked Grouped Column</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Stcked Percentage Column" href="javascript:void(0)" data-cc="3columns" data-type="stacked-percentage-column">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/stacked-percentage-column.PNG" alt="">
                                        </div>
                                        <p>Stacked Percentage Column</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Scatter Plot" href="javascript:void(0)" data-cc="3columnsNumeric" data-type="scatter-plot">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/scatter.PNG" alt="">
                                        </div>
                                        <p>Scatter Plot</p>
                                    </a>
                                </div>
                                <div class="col m2 marg0">
                                    <a title="Scatter 3D Plot" href="javascript:void(0)" data-cc="4columnsNumeric" data-type="scatter-plot-3D">
                                        <div class="card hoverable">
                                            <span class="selectedIcon darkenBackground themeDarkenBackground icon-check"></span>
                                            <img class="chartImg" src="../images/widgetImages/3d-scatter.png" alt="">
                                        </div>
                                        <p>Scatter 3D Plot</p>
                                    </a>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="queryData">
                        <div class="container">
                            <div class="row">
                                <div class="input-field col s12 m6" id="widgeIvNameWrpper">
                                    <input maxlength="8" onblur="updateTheWizObj(this,'ivname')" class="themeInp themeMaterialInp" id="iviewName" type="text">
                                    <label for="iviewName">Iview Name<sup>*</sup></label>
                                </div>
                                <div class="input-field col s12" id="widgetTTlWrpper">
                                    <input maxlength="50" onblur="updateTheWizObj(this,'cptn')" class="themeInp themeMaterialInp" id="widgetTitle" type="text">
                                    <label for="widgetTitle">Title<sup>*</sup></label>
                                </div>

                                <div class="col s12" style="margin-bottom: 8px;">
                                    <span class="labelName">Source Type<sup>*</sup>:</span>
                                    <span>
                                        <input class="with-gap" data-val="sql" name="widgetSqlSource" type="radio" id="sqlQry" checked />
                                        <label for="sqlQry">SQL Query</label>
                                    </span>
                                    <span>
                                        <input class="with-gap" data-val="tbl" name="widgetSqlSource" type="radio" id="tabl" />
                                        <label for="tabl">Table</label>
                                    </span>
                                </div>
                                <div id="widgetSqlData">
                                    <div class="col s12">
                                        <textarea id="codeMirrorSql" cols="30" rows="10"></textarea>
                                    </div>
                                    <input type="text" style="display: none" data-err="" value="false" type="hidden" id="isValidSQl">
                                    <button id="sqlExmpleBtn" class="waves-effect btn-flat">Example <span class="arrwCls icon-chevron-down"></span></button>
                                    <div class="thmColorText themeTextColor" id="sqlExData">
                                        Select data_label, x-axis_label, value from Tabel1 order by label,data_label
                                    </div>
                                </div>
                                <div id="widgetTableData" style="display: none;">
                                    <div class="row marg0">
                                        <div class="input-field col s12 m6 autoComInpFld">
                                            <input type="text" id="widgetTstructName" class="themeInp themeMaterialInp autocompleteMe">
                                            <label for="widgetTstructName">Tstruct<sup>*</sup></label>
                                        </div>
                                        <div class="input-field col s12 m6 autoComInpFld">
                                            <input type="text" disabled id="widgetTstrctColumn" class="themeInp themeMaterialInp autocompleteMe">
                                            <label for="widgetTstrctColumn">Table <sup>*</sup></label>
                                        </div>
                                    </div>
                                    <div style="display: none;" id="widgetTableRemData">
                                        <div id="wtrFldsData" class="row marg0">
                                            <div class="input-field col s12 m4 autoComInpFld">
                                                <input type="text" id="widgetTstructLabelCol" class="themeInp themeMaterialInp autocompleteMe">
                                                <label for="widgetTstructLabelCol">Label Column</label>
                                            </div>
                                            <div class="input-field col s12 m4 autoComInpFld">
                                                <input type="text" id="widgetTstrctValCol" class="themeInp themeMaterialInp autocompleteMe">
                                                <label for="widgetTstrctValCol">Value Column</label>
                                            </div>
                                            <div class="input-field col s12 m4">
                                                <select class="materialSelect">
                                                    <option value="" disabled selected>Select A Function</option>
                                                    <option value="sum">Sum</option>
                                                    <option value="count">Count</option>
                                                </select>
                                                <label>Function</label>
                                            </div>
                                        </div>
                                        <!-- <div class="row marg0">
                                            <div class="col m6 s12">
                                                <p class="marg0">
                                                    <input type="checkbox" id="opnINdlg" />
                                                    <label for="opnINdlg">Open in Dialog</label>
                                                </p>
                                            </div>
                                        </div> -->
                                        <div class="input-field col s12">
                                            <textarea id="whrCls" class="themeInp themeMaterialInp materialize-textarea"></textarea>
                                            <!-- <label for="whrCls">Where Clause</label> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="appearanceData">
                        <div class="container">
                            <div class="row">
                                <div data-showfor="chart,kpi" class="input-field col s12 m12 apprData">
                                    <input type="text" id="widgetGroupsInp" class="themeInp mandatoryFld themeMaterialInp autocompleteMe">
                                    <label for="widgetGroupsInp">Group<sup>*</sup></label>
                                </div>
                                <!-- <div data-showFor="chart" class="input-field col s12 m6 apprData">
                                    <select id="chartOrientation" class="materialSelect">
                                        <option value="" disabled>Select Orientation.</option>
                                        <option value="sum" selected="selected">Horizontal</option>
                                        <option value="count">Vertical</option>
                                    </select>
                                    <label>Orientation</label>
                                </div> -->
                                <div data-showfor="chart" id="chartYaxisLabelWrapper" class="input-field col s12 m6 apprData">
                                    <input type="text" id="chartYaxisLabel" class="themeInp themeMaterialInp">
                                    <label for="chartYaxisLabel">Chart Y-axis Label</label>
                                </div>
                                <div data-showfor="chart" id="chartXaxisLabelWrapper" class="input-field col s12 m6 apprData">
                                    <input type="text" id="chartXaxisLabel" class="themeInp themeMaterialInp">
                                    <label for="chartXaxisLabel">Chart X-axis Label</label>
                                </div>
                                <div data-showfor="chart" class="col m6 apprData">
                                    <div class="switch" style="margin-top: 17px;">
                                        Show Legend:
                                        <label>

                                            <input type="checkbox" checked id="shwLgnd">
                                            <span class="lever"></span>
                                        </label>
                                    </div>
                                </div>
                                <!--  <div data-showFor="chart" id="showLgndPstn" style="display: none;" class="input-field col s12 m6 apprData">
                                    <select class="materialSelect">
                                        <option value="" disabled>Select A Group</option>
                                        <option value="sum" selected>Top</option>
                                        <option value="count">Bottom</option>
                                        <option value="count">Left</option>
                                        <option value="count">Right</option>
                                    </select>
                                    <label>Legend Direction</label>
                                </div> -->
                                <div data-showfor="chart" class="col m6 apprData">
                                    <div class="switch" style="margin-top: 17px;">
                                        Gradient Color Chart:
                                        <label>

                                            <input type="checkbox" id="gradienChartInp">
                                            <span class="lever"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="roleSelctionData">
                        <div class="valign-wrapper">
                            <div class="row con">
                                <div class="col multiSelWrap">
                                    <label class="widgetRespLabel">Responsibility<sup>*</sup></label>
                                    <select name="from[]" id="multiselect" class="form-control1 browser-default rlmultiSlectFld" size="8" multiple="multiple">
                                        <option disabled value='No Roles'>No Roles</option>
                                    </select>
                                </div>
                                <div class="col multiSelWrapOpt" style="padding-left: 20px;">
                                    <button title="Select All" type="button" id="multiselect_rightAll" class="waves-effect btn-flat smallBtn">
                                        <i class="icon-chevron-right frstIcn"></i>
                                        <i class="icon-chevron-right scndIcn"></i>
                                    </button>
                                    <button title="Select" type="button" id="multiselect_rightSelected" class="waves-effect btn-flat smallBtn">
                                        <i class="icon-chevron-right"></i>
                                    </button>
                                    <button title="Remove" type="button" id="multiselect_leftSelected" class="waves-effect btn-flat smallBtn">
                                        <i class="icon-chevron-left"></i>
                                    </button>
                                    <button title="Remove All" type="button" id="multiselect_leftAll" class="waves-effect btn-flat smallBtn">
                                        <i class="icon-chevron-left frstIcn"></i>
                                        <i class="icon-chevron-left scndIcn"></i>
                                    </button>
                                </div>
                                <div class="col multiSelWrap">
                                    <select name="to[]" id="multiselect_to" class="form-control1 browser-default rlmultiSlectFld" size="8" multiple="multiple"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wizardContainer" id="chartSelData1">
                        chartSelData
                    </div>
                    <div class="wizardContainer" id="chartSelData2">
                        chartSelData
                    </div>
                    <div class="wizardContainer" id="chartSelData3">
                        chartSelData
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button title="Previous" onclick="wizardObj.checkClick(this,'prev')" type="button" id="wizardPrevbtn" style="display: none;" class="wizardPrevNxtBtns waves-effect btn themeButton cutsomColorbtn left">&lt; Prev</button>
            <button title="Next" onclick="wizardObj.checkClick(this,'next')" data-validator="" id="wizardNextbtn" type="button" style="display: none;" class="wizardPrevNxtBtns waves-effect btn themeButton cutsomColorbtn">Next &gt;</button>
            <button onclick="wizardObj.resetTheWizard()" type="button" title="Cancel" class="right cancelButton waves-effect btn modal-action modal-close">Cancel</button>
        </div>
    </div>
    <!-- extraModel Component -->
    <div id="extraModelComp">
    </div>
    <div id="fontModelComp">
    </div>

    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.js"></script>
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script src="../ThirdParty/bgrins-spectrum/spectrum.js"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../ThirdParty/materialize/js/materialize.min.js?v=11"></script>
    <script src="../ThirdParty/DataTables-1.10.13/media/js/jquery.dataTables.min.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-3d.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-more.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-exporting.js"></script>
    <script src="../Js/high-charts-functions.min.js?v=17"></script>
    <script src="../Js/codemirror/codemirror.js" type="text/javascript"></script>
    <script src="../Js/multiselect.min.js" type="text/javascript"></script>
    <script src="../Js/codemirror/mode/sql.js" type="text/javascript"></script>
    <script src="../Js/codemirror/addon/hint/show-hint.js" type="text/javascript"></script>
    <script src="../Js/codemirror/addon/hint/sql-hint.js" type="text/javascript"></script>
    <script src="../Js/wizardComp.min.js?v=9" type="text/javascript"></script>
    <script src="../Js/commonBuilder.min.js?v=42"></script>
    <script src="../Js/widgetBuilder.min.js?v=62" type="text/javascript"></script>

    <script src="../ThirdParty/scrollbar-plugin-master/jquery.mCustomScrollbar.js"></script>
    <script src="../Js/AXpagination.min.js?v=8"></script>

</body>
</html>
