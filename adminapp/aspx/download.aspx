﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="download.aspx.vb" Inherits="aspx_download" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Download">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Download</title>
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript" src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2"></script>
</head>
<body dir="<%=direction%>">
    <form id="form1" runat="server">
        <div>
            <br /><asp:Label ID="lblautodwnld" runat="server" meta:resourcekey="lblautodwnld">
            If the download doesn't start automatically, <a id="lnkFile" href="./download.aspx?act=false"><asp:Label ID="lbldwnldclk" runat="server" meta:resourcekey="lbldwnldclk">Click Here</asp:Label></a></asp:Label>
            <br />
            <br />
            <br />
            <div style="text-align: center">
                <a href="javascript:window.close();">Close</a>
            </div>
        </div>
    </form>
</body>
</html>
