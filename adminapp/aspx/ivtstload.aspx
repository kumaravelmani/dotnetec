<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ivtstload.aspx.vb" Inherits="ivtstload"
    Debug="true" %>

<!DOCTYPE html>
<%@ Import Namespace="System.Xml" %>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Tstruct IView">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>IV Tstruct Load</title>
    <link rel="Stylesheet" href="../Css/gen.min.css" type="text/css" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" type="text/css" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>

    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" type="text/css" />
</head>

<body class="Pagebody">
    <form id="form1" runat="server" dir="<%=direction%>">
        <div>
        </div>
    </form>
</body>

</html>
