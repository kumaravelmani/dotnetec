<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ImageUpload.aspx.cs" Inherits="aspx_ImageUpload" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Image Upload">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Image Upload</title>
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="generic" />
   <%-- <link id="themecss" type="text/css" rel="Stylesheet" href="" />--%>
    <link href="../Css/ImageUpload.min.css?v=2" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script type="text/javascript" src="../Js/thirdparty/jquery/3.1.1/jquery.min.js"></script>
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
     <link href="../Css/cloudGrid.min.css" rel="stylesheet" />
    <link href="" rel="stylesheet" id="themecss" type="text/css" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../Css/Users.min.css?v=5" rel="stylesheet" type="text/css" />
    <link href="../Css/newMenu.min.css?v=22" rel="stylesheet" type="text/css" />
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/iFrameHandler.min.js"></script>
    <script src="../Js/gen.min.js?v=13" type="text/javascript"></script>
    <script src="../Js/ImageUpload.min.js?v=12" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/lang/content-<%=langType%>.js?v=26"></script>

</head>
<body dir='<%=direction%>'>
    <form id="form1" runat="server" method="post" enctype="multipart/form-data" dir="<%=direction%>">
        <div class="container">
            <div id="dvNavigate" class="form-group">
                <span id="divFile" class="col-lg-6">
                    <label class="radio-inline">
                        <input type="radio" name="imageRadio" checked="true" id="rdbImgRadio" runat="server" data-rdb-value="File" />File
                    </label>
                </span>
                <%if (AxpCameraOption != "true")
                    { %>
                <span id="divCamera" class="col-lg-6">
                    <label class="radio-inline">
                        <input type="radio" name="imageRadio" id="rdbImgCamera" runat="server" data-rdb-value="Camera">Camera
                    </label>
                </span>
                <%} %>
            </div>
            <div class="dcContent form-group">
                <div id="dvFile" runat="server" class="form-group">
                    <div class="rowbor">
                        <div class="file-upload">
                            <div tabindex="0" class="file-select">
                                <div class="file-select-button" id="fileName">
                                    <asp:Label ID="lblfilename" meta:resourcekey="lblfilename" runat="server">Choose File</asp:Label>
                                </div>
                                <div class="file-select-name" id="noFile">
                                    <asp:Label ID="lblnofilename" meta:resourcekey="lblnofilename" runat="server">No file chosen...</asp:Label>
                                </div>
                                <input tabindex="-1" type="file" name="chooseFile" id="filMyFile" runat="server" accept="image/gif,image/jpg,image/jpeg" />
                            </div>
                        </div>
                        <%--                   <input id="filMyFile" type="file" runat="server" style="height: 20px" accept="image/gif,image/jpg,image/jpeg" />--%>
                    </div>
                    <asp:Label ID="fileuploadsts" Style="display: block; padding-top: 10px;" Text="" runat="server" ForeColor="#DB2222"></asp:Label>
                    <div class="rowbor" style="margin-top: 10px">
                        <asp:TextBox ID="fname" runat="server" Visible="true" BorderStyle="None" ForeColor="white"
                            Width="1" Style="display: none"></asp:TextBox>
                        <asp:TextBox ID="filepathna" runat="server" Visible="true" BorderStyle="None" ForeColor="white"
                            Width="1" Style="display: none"></asp:TextBox>
                        <asp:Button ID="btnUpload" class="hotbtn btn handCursor" runat="server" Text="Upload" OnClick="btnUpload_Click" Style="font-size: 13px;" OnClientClick="" ToolTip="Upload" />
                        <input name="close" type="button" id="close" value="Close" title="Close" class="coldbtn btn handCursor" onclick="closeUploadDialog();">
                    </div>
                </div>
                <div id="dvCam" class="form-group">
                    <video id="videoElement" autoplay style="width: 70%;">
                    </video>
                    <canvas id="canvasid" hidden="hidden"></canvas>
                    <br />
                    <img id="imgCanvas" src="" runat="server" alt="" hidden="hidden" /><br />
                    <input type="hidden" runat="server" id="mydata" value="hidden" />
                    <asp:TextBox ID="TextBox1" runat="server" Visible="true" BorderStyle="None" ForeColor="white"
                        Width="1" BackColor="white"></asp:TextBox>
                    <asp:TextBox ID="TextBox2" runat="server" Visible="true" BorderStyle="None" ForeColor="white"
                        Width="1" BackColor="white"></asp:TextBox>
                    <asp:Button ID="btnCapture" runat="server" CssClass="hotbtn btn handCursor" disabled="true" OnClientClick="snap();"
                        Text="Take Photo" ToolTip="Take Photo" Style="margin-top: 10px; margin-left: 62px; height: 30px; width: 100px; padding-left: 10px;" OnClick="btnCapture_Click" />
                    <button id="flip-btn" class="hotbtn btn handCursor" style="margin-top: 10px; height: 30px; width: 100px; padding-left: 10px; display: none">Flip Camera</button>
                </div>
                <input runat="server" type="hidden" id="imgUploadLimit" value="4000000" />

                <asp:Label ID="lblfilecn" runat="server" meta:resourcekey="lblfilecn" Visible="false">Image could not be uploaded. Invalid FileType</asp:Label>
                <asp:Label ID="lblfilesize" runat="server" meta:resourcekey="lblfilesize" Visible="false"></asp:Label>
            </div>
        </div>
    </form>
</body>
</html>
