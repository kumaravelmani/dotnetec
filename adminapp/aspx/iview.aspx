<%@ Page Language="C#" AutoEventWireup="true" CodeFile="iview.aspx.cs" Inherits="iview"
    EnableEventValidation="false" ValidateRequest="false" %>

<!DOCTYPE html>
<html>

<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta name="description" content="IView">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <title>Iview</title>
    <!-- ________ CSS __________ -->
    <link href="../ThirdParty/DataTables-1.10.13/media/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../ThirdParty/DataTables-1.10.13/extensions/Responsive/css/responsive.bootstrap.min.css"
        rel="stylesheet" />
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../newPopups/Remodal/remodal-default-theme.min.css?v=2" rel="stylesheet" />
    <link href="../newPopups/Remodal/remodal.min.css?v=2" rel="stylesheet" />
    <link href="../newPopups/axpertPopup.min.css?v=14" rel="stylesheet" />
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <link href="../Css/smartViews.min.css?v=10" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="../App_Themes/Gray/Stylesheet.min.css?v=23" type="text/css" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=2" rel="stylesheet" />
    <link id="themecss" type="text/css" rel="Stylesheet" href="" />
    <link href="../Css/iviewNew.min.css?v=19" rel="stylesheet" />
    <link href="../Css/generic.min.css?v=10" rel="stylesheet" type="text/css" id="generic" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.theme.min.css" rel="stylesheet" />
    <link href="../ThirdParty/DataTables-1.10.13/extensions/FixedColumns/css/fixedColumns.dataTables.min.css"
        rel="stylesheet" />
    <link href="../ThirdParty/bgrins-spectrum/spectrum.css?v=1" rel="stylesheet" />
    <% if (direction == "rtl")
        { %>
    <link rel="stylesheet" href="../ThirdParty/bootstrap_rtl.min.css" type="text/css" />
    <% } %>
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <!-- jQuery library -->
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <script src="../ThirdParty/DataTables-1.10.13/media/js/jquery.dataTables.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/media/js/dataTables.bootstrap.min.js"></script>
    <!--<script src="../ThirdParty/DataTables-1.10.13/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Responsive/js/responsive.bootstrap.min.js"></script>-->
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/dataTables.buttons.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/buttons.flash.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/buttons.html5.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/buttons.print.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/jszip.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/pdfmake.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Buttons/js/vfs_fonts.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
    <%--custom alerts start--%>
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <%--custom alerts end--%>
    <script src="../newPopups/Remodal/remodal.min.js" type="text/javascript"></script>
    <script src="../newPopups/axpertPopup.min.js?v=28" type="text/javascript"></script>
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>

    <script src="../Js/thirdparty/jquery-ui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <%--_____________CHART LINKS_____________--%>
    <script src="../ThirdParty/Highcharts/highcharts.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-3d.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-more.js"></script>
    <script src="../ThirdParty/Highcharts/highcharts-exporting.js"></script>

    <!-- ________ JAVASCRIPT __________ -->

    <script type='text/javascript'>
        var gllang = '<%=langType%>';
        var parentArr = new Array();
        var typeArr = new Array();
        var depArr = new Array();
        var depParamArr = new Array();
        var hiddenArr = new Array();
        var Expressions = new Array();
        var exprSuggestions = new Array();
        var vExpressions = new Array();
        var paramType = new Array();
        var pCurArr = new Array();
        var ctrlType = "";
        var ctrlID = "";
        var childCtrlID = "";
        var oldChildCtrlID = "";
        var transid = "";
        var currFldValue = "";
        var Parameters = new Array();
        var FNames = new Array();
        var fldNameArray = new Array();
        var fldValueArray = new Array();
        var fldDeletedArray = new Array();
        var IsNqIvTaskBtnCliked = false;
        var SaveWindow;
        var AxActiveField = "";
        var GridDispHead = false;
        var DCFrameNo = new Array();
        var TstructHasPop = false;
        var validateParamOnGo = false;
        var isChartsAvailable = true;
        var showMore = true;
        var isFromClearBtn = false;
        var IVIRCaption = "<%=breadCrumb%>";
        var printTitle = "<%=PrintTitle%>";
        var capturedButton1Submit = false;
        //below line will load the google charts

        var enableBackButton = false;
        var enableForwardButton = false;

        var allowRecPerPageChange = false;
        var lastSelRecPerPage = "";
    </script>
    <script type="text/javascript" src="../Js/common.min.js?v=62"></script>
    <script type="text/javascript" src="../Js/iview.min.js?v=175"></script>
    <script type="text/javascript" src="../Js/jsclient.min.js?v=21"></script>
    <script src="../Js/process.min.js?v=134" type="text/javascript"></script>
    <script type="text/javascript" src="../Js/util.min.js?v=2"></script>
    <script src="../Js/JDate.min.js?v=2" type="text/javascript"></script>
    <!-- ________ JQUERY __________ -->
    <link href="../Css/jquery-ui-timepicker-addon.min.css" rel="stylesheet" type="text/css" />
    <script src="../Js/jquery-ui-timepicker-addon.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../Js/jquery.timeentry.package/jquery.timeentry.css">
    <script type="text/javascript" src="../Js/jquery.timeentry.package/jquery.timeentry.min.js"></script>
    <link href="../Css/iview.min.css?v=25" rel="stylesheet" />
    <link href="../ThirdParty/bootstrap-tokenfield/dist/css/bootstrap-tokenfield.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../ThirdParty/bootstrap-tokenfield/dist/bootstrap-tokenfield.min.js?v=1">
    </script>

    <script src="../Js/jquery.browser.min.js" type="text/javascript"></script>
    <script src="../Js/printjs.js" type="text/javascript"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/moment.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/jquery.visible.min.js"></script>
    <script src="../ThirdParty/DataTables-1.10.13/extensions/Extras/datetime-moment.js"></script>

    <script src="../Js/iviewjs.min.js?v=3" type="text/javascript"></script>
</head>

<body class="Mainbody Family btextDir-<%=direction%>" dir="<%=direction%>">
    <form id="form1" class="frmiview" runat="server" onclick="javascript:HideNqIvTaskList();">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="../Js/helper.min.js?v=101" />
                </Scripts>
                <Services>
                    <asp:ServiceReference Path="../WebService.asmx" />
                </Services>
                <Services>
                    <asp:ServiceReference Path="../CustomWebService.asmx" />
                </Services>
            </asp:ScriptManager>
        </div>


        <div id="iviewFrame" runat="server">
            
           <%-- <div id="lol2" runat="server" style="float: left; border-right: 1px solid black;">--%>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divfile" class="hide">
                            <table class="tblrowbor" style="padding: 1px; text-align: center;">
                                <tr>
                                    <td class="rowbor">
                                        <asp:Label ID="LabelFs" runat="server" meta:resourcekey="LabelFs" Font-Bold="True"
                                            ForeColor="#1e90ff">File Name:</asp:Label>
                                        <input id="filMyFile" accept="text/html" type="file" />
                                        <input type="button" onclick="javascript: CallAfterFileUpload();" id="cafterfload"
                                            value="load" name="cafterfload" />
                                    </td>
                                </tr>
                                <tr style="text-align: center; vertical-align: top;" class="txtrowbar">
                                    <td class="rowbor">
                                        <asp:TextBox ID="TextBox2" Width="1" runat="server" Visible="true"
                                            BorderStyle="None" ForeColor="white" BackColor="white"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>




                        <%if (hidetoolbar == "false")
                        {%>
                        <asp:Label ID="lblTime" runat="server"></asp:Label>
                        <asp:Label ID="lblTimeClient" runat="server"></asp:Label>
                        <div class="listViewDc" style="overflow: hidden;">
                            <%=strBreadCrumbBtns%>
                            <div id="breadcrumb-panel">
                                <div id='breadcrumb' class='icon-services <%=bread_direction%>'>
                                    <%=strBreadCrumb.ToString()%>
                                </div>

                                <%-- --%><div id="ivInSearch" style="">
                                    <div class="searchBox">
                                        <div class="searchBoxChildContainer">
                                            <span class="icon"><i class="fa fa-search" id="idsearch"></i></span>
                                            <input type="search" id="ivInSearchInput" runat="server" />
                                            <%-- <span id="ivirMoreButton" class="icon2" onclick="ivirMoreFilters(true);" tabindex="0">
                                            <i class="fa fa-caret-down"></i>
                                        </span>--%>
                                        </div>
                                    </div>
                                    <%-- --%>
                                </div>

                                <div id="dvActions" runat="server" style="float: right; display: none;">
                                    <dl id="dlAction" class="ddlBtn">
                                        <dt><a href="javascript:void(0)" onclick="ddToggle(event);">
                                            <img src="../AxpImages/icons/task.png" alt="task" />
                                            <asp:Label ID="lblaction" runat="server" meta:resourcekey="lblaction">select
                                                an Action</asp:Label>
                                        </a></dt>
                                        <dd>
                                            <ul id="ddActionBtns" runat="server">
                                            </ul>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div id="ivContainer">

                                <div class="container">
                                    <div class="row">
                                        <div class="bs-example">
                                            <div>
                                                <div id="searchBar" class="" runat="server">
                                                    <div id='icons' class='right' runat="server">
                                                        <ul id="iconsUl" runat="server">
                                                            <li id="myFiltersLi" runat="server"><a id="myFilters"
                                                                runat="server" class="filterBuTTon"
                                                                data-toggle="collapse" data-parent="#accordion"
                                                                href="#Filterscollapse">
                                                                <span class="ccolapsee glyphicon"
                                                                    style="color: black; font-size: 15px;"></span>
                                                                <asp:Label ID="lblfilters" runat="server"
                                                                    meta:resourcekey="lblfilters">Params</asp:Label>

                                                            </a>
                                                                <div id="dvSelectedFilters" class="badge" runat="server"
                                                                    style="display: none">
                                                                    <span id="FilterValues" runat="server" style=""></span>
                                                                </div>

                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <span class="paramFiletrs pull-right" runat="server"
                                                    id="divFilterCond"></span>
                                                <div id="dvPages" runat="server"
                                                    style="margin-right: 10px; float: right; text-align: center; display: none;">
                                                    <asp:Label ID="records" runat="server" Text="" CssClass="totrec">
                                                    </asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="pgCap" Text="Page No." runat="server" CssClass="totrec">
                                                </asp:Label>
                                                    <asp:DropDownList ID="lvPage" runat="server" AutoPostBack="true"
                                                        CssClass="combotem" onchange="javascript:ShowDimmer(true);"
                                                        OnSelectedIndexChanged="lvPage_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:Label ID="pages" Text="" runat="server" CssClass="totrec">
                                                    </asp:Label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:Label ID="lblDbRecsCount" runat="server" CssClass="totrec">
                                                </asp:Label>

                                                </div>


                                                <div id="dvStatictime" runat="server">
                                                    <asp:Label ID="lblRefresh" CssClass="colorButton"
                                                        Style="font-family: Arial; font-weight: bold; font-size: 12px; color: Red;"
                                                        runat="server"></asp:Label>
                                                </div>

                                                <div runat="server" id="dvStagLoad"
                                                    style="margin-right: 10px; float: right; display: none">
                                                    <a class="handCursor" onclick="javascript:GetStagRecords('More');">
                                                        <asp:Label ID="lblshow" runat="server" meta:resourcekey="lblshow">
                                                        Show More</asp:Label>
                                                    </a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a class="handCursor" onclick="javascript:GetStagRecords('All');">
                                                    <asp:Label ID="lblshowall" runat="server"
                                                        meta:resourcekey="lblshowall">Show All</asp:Label>
                                                </a>&nbsp;&nbsp;
                                                </div>
                                                <div id="dvSqlPages" runat="server"
                                                    style="position: relative; margin-right: 10px; top: 4px; display: none;">
                                                    <div id="nextPrevBtns" runat="server" class="nextPrevBtns"
                                                        style="margin-left: 10px; margin-top: 10px; float: right;">
                                                        <asp:LinkButton ID="lnkPrev" runat="server" title="Prev"
                                                            OnClientClick="return CheckDisabled('lnkPrev');"
                                                            OnClick="lnkPrev_Click" />
                                                        <asp:LinkButton ID="lnkNext" runat="server" title="Next"
                                                            OnClientClick="return CheckDisabled('lnkNext');"
                                                            OnClick="lnkNext_Click" />
                                                    </div>
                                                    <div id="rowsTxtCount">

                                                        <span>
                                                            <asp:Label ID="lblCurPage" runat="server" CssClass="totrec">
                                                            </asp:Label>
                                                        </span>
                                                        <span>
                                                            <asp:Label ID="lblNoOfRecs" runat="server" CssClass="totrec">
                                                            </asp:Label>
                                                        </span>
                                                        <span id="lnkShowAll" title="Show All Records" class="btn"
                                                            onclick="getAllRecords();">
                                                            <i class="fa fa-list" aria-hidden="true"></i><i
                                                                class="fa fa-long-arrow-down" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div style="display: none" id="dvRowsPerPage" runat="server">
                                                    <asp:Label ID="lbRecPerPage" meta:resourcekey="lbRecPerPage"
                                                        Style="margin-top: 0px; padding-left: 5px;" runat="server">Rows Per
                                                    Page</asp:Label>&nbsp;&nbsp;
                                                <asp:DropDownList ID="recPerPage" AutoPostBack="false" runat="server"
                                                    Style="margin-top: 0px; height: 20px; border: 1px solid gainsboro !important; box-shadow: 0px 0px 1px #c1c1c1 !important;"
                                                    OnSelectedIndexChanged="RecordsPerPage_SelectedIndexChanged"
                                                    onchange="onRecPerPageChange(event);ShowDimmer(true);">
                                                    <asp:ListItem Text="50" Value="50" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="100" Value="100"></asp:ListItem>
                                                    <asp:ListItem Text="200" Value="200"></asp:ListItem>
                                                    <asp:ListItem Text="500" Value="500"></asp:ListItem>
                                                    <%--<asp:ListItem Text="1000" Value="1000"></asp:ListItem>--%>
                                                    <asp:ListItem Text="ALL" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                </div>

                                                <div id="dvRefreshParam" runat="server">
                                                    <span>
                                                        <button type="submit" style="background: none; border: 0;"
                                                            title="Refresh parameter values"
                                                            onclick="UpdateGetParamCache();">
                                                            <i class="fa fa-refresh" style="color: #797878;"></i>
                                                        </button>
                                                    </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="panel-group" id="accordion">
                                                <div id="myFiltersBody" runat="server" class="panel panel-default">

                                                    <div id="Filterscollapse" class="panel-collapse collapse"
                                                        aria-expanded="false" runat="server">
                                                        <div class="panel-body">
                                                            <table class="tblresX">
                                                                <tr>
                                                                    <td class="Pagebody">
                                                                        <div id="dvParamCont" runat="server">
                                                                            <%=paramHtml.ToString()%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tdheadbar" style="text-align: center;">
                                                                        <table class="tblheadbar">
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Button ID="button1" runat="server"
                                                                                        OnClientClick="Javascript:return ValidateOnSubmit();"
                                                                                        CssClass="hotbtn btn" Text="Search"
                                                                                        ToolTip="Search" />
                                                                                    &nbsp;

                                                                                <asp:Button ID="button2" runat="server"
                                                                                    CssClass="coldbtn btn"
                                                                                    OnClick="button2_Click" Text="Clear"
                                                                                    ToolTip="Clear"
                                                                                    OnClientClick="Javascript:ClearParamValues();" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="ivcaptions" id="ivCap1" runat="server" visible="false">
                            </div>
                            <div class="iviewTableWrapper">





                                <div class="myview" id="divFilterView">
                                    <div class="myViewHeader">
                                        <h3>
                                            <asp:Label ID="lblview" runat="server" meta:resourcekey="lblview">Create View
                                            </asp:Label>
                                        </h3>
                                    </div>
                                    <div class="myviewname">
                                        <asp:Label ID="lblviewname" runat="server" meta:resourcekey="lblviewname">Name
                                        </asp:Label>
                                    </div>
                                    <div class="myviewtext">
                                        <asp:TextBox ID="txtViewName" runat="server"></asp:TextBox>
                                    </div>

                                    <div class="myviewcancelbtn">
                                        <asp:Button ID="btnViewCancel" runat="server" OnClientClick="CloseViewPopup();"
                                            Text="Cancel" />
                                    </div>
                                </div>
                                <div class="dvContent">

                                    <%}
                                    else
                                    { %>

                                    <div style="overflow: hidden;">
                                        <%} %>
                                        <div style="display: none;">
                                            <asp:Button ID="SortGrid" runat="server" OnClick="btnSortGrid_Click" />
                                            <asp:Button ID="FilterGrid" runat="server" OnClick="btnFilterGrid_Click" />
                                            <asp:Button ID="HideColumn" runat="server" OnClick="btnHideColumn_Click" />


                                            <asp:HiddenField ID="hdnHideColName" runat="server" />
                                        </div>

                                        <asp:HyperLink ID="btnDownloadAll" runat="server" Text="Download Selected"
                                            Visible="true" Enabled="true" NavigateUrl="Javascript:DownloadSelectedFiles();"
                                            CssClass="hide" />
                                        <div id="leftPanel" runat="server" style="display: none; float: left;">

                                            <div>

                                                <div id="paramCont" runat="server" class="wBdr Pagebody">

                                                    <asp:HiddenField ID="hdnparamValues" runat="server" />
                                                    <asp:HiddenField ID="rXml" runat="server" />
                                                    <asp:HiddenField ID="param" runat="server" Value="" />
                                                    <asp:HiddenField ID="paramxml" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnGo" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnAct" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnIsPostBack" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnSRows" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnParamChngd" runat="server" Value="" />
                                                    <asp:HiddenField ID="hdnIvRefresh" runat="server" Value="" />
                                                    <input type="hidden" name="recordid000F0" value="0" />
                                                    <asp:HiddenField ID="hdnJsonData" runat="server" />
                                                    <asp:HiddenField ID="hdnSortColName" runat="server" />
                                                    <asp:HiddenField ID="hdnFilterColName" runat="server" />
                                                    <asp:HiddenField ID="hdnFilterBy" runat="server" />
                                                    <asp:HiddenField ID="hdnNoOfRecords" runat="server" />
                                                    <asp:HiddenField ID="hdnGridColumns" runat="server" />
                                                    <asp:HiddenField ID="hdnHideColumns" runat="server" />
                                                    <table id="tblFilterCols" style="display: none">
                                                    </table>
                                                    <asp:HiddenField ID="hdnFilterChecked" runat="server" />
                                                    <asp:HiddenField ID="hdnSortColumns" runat="server" />
                                                    <asp:HiddenField ID="hdnMyViewName" runat="server" />
                                                    <asp:HiddenField ID="hdnMyViewFilterCol" runat="server" />
                                                    <asp:HiddenField ID="hdnIViewShow" runat="server" />
                                                    <asp:HiddenField ID="hdnTotalIViewRec" runat="server" />
                                                    <asp:HiddenField ID="hdnHasTotalRows" runat="server" />
                                                    <asp:HiddenField ID="hdnHideShowCol" runat="server" />
                                                    <asp:HiddenField ID="hdnHideShowFld" runat="server" />
                                                    <asp:HiddenField ID="hdnIsParaVisible" runat="server" />
                                                    <asp:HiddenField ID="hdnTotalRec" runat="server" />
                                                    <asp:HiddenField ID="printRowsMaxLimitField" runat="server" />
                                                    <asp:HiddenField ID="hdnShowParams" runat="server" />
                                                    <asp:HiddenField ID="hdnIsDirectDB" runat="server" />
                                                    <asp:HiddenField ID="hdnIsPerfXml" runat="server" Value="false" />
                                                    <asp:HiddenField ID="hdnSelParamsAftrChWin" runat="server" />
                                                    <asp:HiddenField ID="hdnAutoSplit" runat="server" />
                                                    <asp:HiddenField ID="hdnDisableSplit" runat="server" />
                                                </div>
                                            </div>
                                            <div>
                                                <h3 onclick="UpdateActiveArray(this);" id="ivSortHeader">
                                                    <asp:Label ID="lblsort" runat="server" meta:resourcekey="lblsort"> Sort
                                                    By</asp:Label>
                                                </h3>
                                                <div id="ivSortContent">
                                                    <ul id="sortIvByColumn">
                                                    </ul>
                                                </div>
                                            </div>
                                            <div>
                                                <h3 id="HideCol" onclick="UpdateActiveArray(this);">
                                                    <asp:Label ID="lblhidecolumn" runat="server"
                                                        meta:resourcekey="lblhidecolumn">Hide Column</asp:Label>
                                                </h3>



                                            </div>
                                        </div>
                                        <div id="contentPanelGridIview" runat="server">
                                            <div id="divcontainer" class="dvIvCont" runat="server">
                                                <div id="dvStatus" runat="server" class="error hide">
                                                    <asp:Label ID="lblErrMsg" runat="server"></asp:Label>
                                                </div>
                                                <div id="dvData" runat="server">
                                                    <asp:HiddenField ID="hdnIViewData" runat="server" />


                                                    <asp:Panel runat="server" ID="Panel1" Width="100%" CssClass="pnlgrid">

                                                        <asp:GridView CellSpacing="-1" ID="GridView1" runat="server"
                                                            OnPreRender="GridView1_PreRender" AutoGenerateColumns="False"
                                                            CssClass="gridData" OnRowDataBound="GridView1_RowDataBound">
                                                        </asp:GridView>
                                                        <div id="GridView2Wrapper" runat="server" visible="false">
                                                        </div>
                                                        <table class="animationLoading">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>


                                                        <div id="ivirMainChartWrapper" style="display: none;">
                                                            <div id="ivirChartPills" style="display: none;"></div>
                                                            <div id="ivirChartWrapper"></div>
                                                        </div>
                                                    </asp:Panel>


                                                    <div id="dvFilteredRowCount" runat="server">
                                                        <span style="float: left; margin-right: 15px;">
                                                            <asp:Label ID="lblFilteredRowCount" runat="server"
                                                                CssClass="totrec"></asp:Label>
                                                            <asp:Label ID="lblTotalNo" runat="server" Visible="false"
                                                                meta:resourcekey="lblTotalNo">Total no. of filtered Rows :
                                                            </asp:Label>
                                                        </span>
                                                    </div>
                                                    <div id="divCustomAct" visible="false" runat="server" class="wBdr"
                                                        style="width: 99.8%; border-collapse: collapse;">
                                                        <div id="divActHeader" style="background-color: #f8f8f8;"
                                                            class="dcHeaderBar">
                                                            <a href='javascript:ShowAndHideActDiv("dvActivityInfo");'><span
                                                                style="vertical-align: top;" id="dcButspan3">
                                                                <img id="imgAct" border="0" alt="show"
                                                                    src="../AxpImages/icons/16x16/expandwt.png"></span></a>
                                                            <font style="vertical-align: top" class="heading">Recent
                                                            activities</font>
                                                        </div>
                                                        <div id="dvActivityInfo"
                                                            style="display: none; background-color: White;" runat="server">
                                                            <center>
                                                            <table
                                                                style="table-layout: fixed;border:0px;padding:0px;border-spacing:0px;width:100%;height:100%;">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="vertical-align:top;">
                                                                            <table
                                                                                style="table-layout: fixed; margin-left: 82px;border:0px;padding:0px;border-spacing:0px;width:100%;">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td
                                                                                            style="padding: 0pt;vertical-align:top;text-align:left;">
                                                                                            <table
                                                                                                style="text-align:center;width:100%;border:0px;padding:8px;border-spacing:0px;">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="vertical-align:top;"
                                                                                                            colspan="2">
                                                                                                            <asp:GridView
                                                                                                                CellSpacing="-1"
                                                                                                                ID="grvActivities"
                                                                                                                runat="server"
                                                                                                                BackColor="White"
                                                                                                                CssClass="labelcap"
                                                                                                                BorderStyle="Groove"
                                                                                                                BorderWidth="2px"
                                                                                                                CellPadding="4"
                                                                                                                OnRowDataBound="grvActivities_RowDataBound">
                                                                                                                <RowStyle
                                                                                                                    BackColor="White"
                                                                                                                    BorderStyle="Groove"
                                                                                                                    BorderWidth="2px" />
                                                                                                                <FooterStyle
                                                                                                                    BackColor="#99CCCC"
                                                                                                                    ForeColor="#003399" />
                                                                                                                <PagerStyle
                                                                                                                    BackColor="#99CCCC"
                                                                                                                    ForeColor="#003399"
                                                                                                                    HorizontalAlign="Left" />
                                                                                                                <SelectedRowStyle
                                                                                                                    BackColor="#009999"
                                                                                                                    Font-Bold="True"
                                                                                                                    ForeColor="#CCFF99" />
                                                                                                                <HeaderStyle
                                                                                                                    BackColor="#c3d9ff"
                                                                                                                    Font-Bold="True" />
                                                                                                            </asp:GridView>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </center>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="noRecccord" style="display: none;" runat="server">
                                                <asp:Label ID="lblNodata" runat="server" meta:resourcekey="lblNodata">No
                                                data found.</asp:Label>
                                            </div>
                                        </div>
                                        <%--<div id="ivirMainChartWrapper" style="display: none;">
                                        <div id="ivirChartPills" style="display: none;"></div>
                                        <div id="ivirChartWrapper"></div>
                                    </div>--%>
                                        <div id="dvCharts" class="hide dvChart">
                                            <div class="dvChartCont">
                                                <a href="javascript:void(0)" onclick="javascript:CloseChart();"
                                                    style="float: right; margin-right: 3px; text-decoration: none">
                                                    <img alt="close" src="../AxpImages/icons/close-button.png" /></a>
                                                <table style="width: 90%">
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <asp:RadioButtonList ID="rbtnChartType" runat="server"
                                                                    RepeatDirection="Horizontal">
                                                                    <asp:ListItem Value="line" Selected="True">Line
                                                                    </asp:ListItem>
                                                                    <asp:ListItem Value="bar">Bar</asp:ListItem>
                                                                    <asp:ListItem Value="pie">Pie</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </span>
                                                        </td>
                                                        <td>
                                                            <a id="lnkChart" href="javascript:ShowChartPopUp();">View Full
                                                            Screen</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left: 10px;">
                                                            <asp:Label ID="Xaxis" runat="server" meta:resourcekey="Xaxis">
                                                            X-Axis</asp:Label>
                                                            <asp:DropDownList ID="ddlChartCol1" Style="width: 100px"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="Yaxis" runat="server" meta:resourcekey="Yaxis">
                                                            Y-Axis</asp:Label>
                                                            <asp:DropDownList ID="ddlChartCol2" Style="width: 100px"
                                                                runat="server">
                                                            </asp:DropDownList>
                                                            <asp:Button ID="btnGetChart1" runat="server"
                                                                OnClick="btnGetChart_Click" Text="Go" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="chart_div" style="height: auto; margin: 0 auto; overflow-x: auto;">
                                            </div>
                                        </div>

                                        <asp:HiddenField ID="hdnParamHtml" runat="server" />
                                        <asp:HiddenField ID="hdnChartSize" runat="server" />
                                        <asp:HiddenField ID="hdnKey" runat="server" />
                                        <asp:HiddenField ID="hdnStagTableNo" runat="server" />
                                        <asp:HiddenField ID="hdnToggleDimmer" runat="server" Value="nothing" />
                                        <div id="bg" class="popup_bg">
                                        </div>
                                    </div>

                                </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
              <%--</div>--%>
            <%--<div id="lol1" runat="server" style="float: right; width: 50%; padding : 50px 0;">
            </div>--%>
      
        <div id='waitDiv' style='display: none;'>
            <div id='backgroundDiv'
                style='background: url(../Axpimages/loadingBars.gif) center center no-repeat rgba(255, 255, 255, 0.4); background-size: 135px;'>
            </div>
        </div>
        <%=strJsArrays%>
        <%=tst_Scripts%>
        <%=enableBackForwButton%>
        <div id="dvPickList" style="position: absolute;" class="randomDivPl pickLstResultWrapper ">
            <div class="pickLstResultCntnr">
                <div class="pickLstCloseBtn">
                </div>
                <div id="dvPickHead">
                </div>
                <div class="clear"></div>
                <div id="dvPickFooter" class="pickListFooter">
                    <div class="pickListFLP">
                        <button type="button" id="advancebtn" onclick="javascript:CallSearchOpen();">
                            Advance search<span
                                class="glyphicon glyphicon-searchv icon-basic-magnifier"
                                style="float: left;"></span></button>
                    </div>
                    <div class="pickListFRP">
                        <button type="button" id="prevPick" title="previous" class="previousPickList curHand"
                            onclick="javascript:GetData('prev')">
                            <i
                                class="glyphicon glyphicon-arrow-left icon-arrows-left"></i>
                        </button>
                        <button type="button" id="nextPick" style="text-align: center; padding: 3px;"
                            onclick="javascript:GetData('next');" title="next" class="nextPickList curHand">
                            <i
                                class="glyphicon glyphicon-menu-right icon-arrows-right"></i><i
                                    class="glyphicon glyphicon-menu-right"></i>
                        </button>

                    </div>
                    <div class="clear"></div>




                </div>
                <input type='hidden' id='hdnPickFldId' /><input type="hidden" id="hdnFiltered" />
                <div id="pickDimmer"
                    style="position: absolute; background: rgba(255, 255, 255, 0.84) url(../AxpImages/icons/loading.gif) no-repeat center center; top: 0; left: 0; width: 100%; height: 100%;">
                    <%--<img src="../AxpImages/icons/loading.gif" />--%>
                </div>
            </div>
        </div>
        <a id="hdnButtonInvokeDownload" style="display: none" href="javascript:void(0)" target="_blank"></a>
        <asp:Label ID="lblNodataServer" runat="server" meta:resourcekey="lblNodataServer" Visible="false">No data found.
        </asp:Label>
        <asp:Label ID="lblTaskExcel" runat="server" meta:resourcekey="lblTaskExcel" Style="display: none">Excel
        </asp:Label>
        <asp:Label ID="lblTaskPDF" runat="server" meta:resourcekey="lblTaskPDF" Style="display: none">PDF</asp:Label>
    </form>


    <script src="../Js/smartViews.min.js?v=37"></script>
    <script src="../ThirdParty/bgrins-spectrum/spectrum.js?v=1"></script>

</body>

</html>
