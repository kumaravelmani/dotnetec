<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ExportNew.aspx.cs" ValidateRequest="false" Inherits="aspx_ExportNew" EnableEventValidation="false" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="utf-8">
    <meta name="description" content="Export Data">
    <meta name="keywords" content="Agile Cloud, Axpert,HMS,BIZAPP,ERP">
    <meta name="author" content="Agile Labs">
    <link href="../Css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet" />
    <link href="../Css/globalStyles.min.css?v=16" rel="stylesheet" />
    <link href="../Css/animate.min.css" rel="stylesheet" />
    <link href="../Css/thirdparty/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />
    <link href="../ThirdParty/Linearicons/Font/library/linearIcons.css" rel="stylesheet" />
    <link href="../ThirdParty/jquery-confirm-master/jquery-confirm.min.css?v=1" rel="stylesheet" />
    <link rel="stylesheet" href="../Css/wizardComp.min.css?v=4">
    <link href="../Css/ExportNew.min.css?v=18" rel="stylesheet" />
    <link href="../Css/Icons/icon.css" rel="stylesheet" />
    <link href="../App_Themes/Gray/Stylesheet.min.css?v=23" rel="stylesheet" />
    <link id="themecss" type="text/css" href="" rel="stylesheet" />
    <link href="../Css/thirdparty/jquery-ui/1.12.1/jquery-ui.min.css" rel="stylesheet" />
    <script>
        if (!('from' in Array)) {
            // IE 11: Load Browser Polyfill
            document.write('<script src="../Js/polyfill.min.js"><\/script>');
        }
    </script>
    <script src="../Js/thirdparty/jquery/3.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="../Js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../Js/noConflict.min.js?v=1" type="text/javascript"></script>
    <script src="../Js/thirdparty/bootstrap/3.3.6/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Js/common.min.js?v=62" type="text/javascript"></script>
    <script src="../Js/jquery.multi-select.min.js" type="text/javascript"></script>
    <script src="../Js/wizard.min.js?v=10" type="text/javascript"></script>
    <script src="../ThirdParty/jquery-confirm-master/jquery-confirm.min.js?v=2" type="text/javascript"></script>
    <script src="../Js/alerts.min.js?v=24" type="text/javascript"></script>
    <script src="../Js/wizardComp.min.js?v=9" type="text/javascript"></script>

    <script src="../Js/ExportImport.min.js?v=25" type="text/javascript"></script>
    <script src="../Js/multiselect.min.js" type="text/javascript"></script>
    <script src="../Js/bootstrap-select.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        var arrFlds = new Array();
        var arrFldDcNo = new Array();
        var arrIsDc = new Array();
        var arrGridDcs = new Array();
        var arrAllDcsData = new Array();
        var clcikOnSumbmit = true;
        var AxwizardType = '<%=Session["AxWizardType"]%>';

    </script>
</head>
<body dir='<%=direction%>' class="btextDir-<%=direction%>">
    <form id="form1" runat="server">

        <!-- Widget Work flow - begins -->
        <div id="wizardWrappper">
            <!-- Widget Header menus - begins -->
            <div id="wizardHeader">
            </div>
            <!-- Widget Header menus - end -->

            <div id="wizardBodyContent">
                <!-- Widget Data Search - begins -->
                <div class="wizardContainer wizardValidation animated fadeIn" id="exWizardDataSearch">
                    <section class="form-group col-md-12">
                        <asp:Label runat="server" class="selectalign" AssociatedControlID="ddlExTstruct">
                            <asp:Label runat="server" ID="lblextstruct" meta:resourcekey="lblextstruct" Text="Select Form" /><span class="allowempty">*</span>
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl1" data-content="Select the form, from which you want to export." data-placement="right" style="cursor: pointer;"></i>
                        <asp:DropDownList runat="server" ID="ddlExTstruct" CssClass="form-control forValidation selectpicker" data-live-search="true" data-size="6" onchange="checkForValuesChanged(this)" AutoPostBack="true" OnSelectedIndexChanged="ddlExTstruct_SelectedIndexChanged">
                        </asp:DropDownList>
                        <span class="customErrorMessage selectPageErrorMessage"></span>
                    </section>
                    <section class="form-group col-md-12">
                        <label for="txtExFields" onclick="$('.ms-selectable ul.ms-list').focus();">
                            <asp:Label ID="lbltxtex" runat="server" meta:resourcekey="lbltxtex" Text="Fields"></asp:Label><span class="allowempty">*</span>
                        </label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl2" data-content="Select the field(s) to be included in the export template." data-placement="right" style="cursor: pointer;"></i>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 45%">
                                    <select id="mSelectLeft" name="from[]" class="multiselect form-control" size="8" multiple="multiple" data-right="#mSelectRight" data-right-all="#right_All_1" data-right-selected="#right_Selected_1" data-left-all="#left_All_1" data-left-selected="#left_Selected_1">
                                        <asp:Repeater ID="rptSelectFields" runat="server">
                                            <ItemTemplate>
                                                <option value='<%# Container.DataItem.ToString()%>'><%# Container.DataItem.ToString().Substring(0,Container.DataItem.ToString().IndexOf(" ("))%></option>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </select>
                                </td>
                                <td style="width: 10%; text-align: center;">
                                    <%--button icons are updating based on the lang selection <, >, <<, >>--%>
                                    <div><a href="" id="right_All_1" title="Select All" class="fa colorButton select-button"></a></div>
                                    <div><a href="" id="right_Selected_1" title="Select" class="fa colorButton select-button"></a></div>
                                    <div><a href="" id="left_Selected_1" title="Unselect" class="fa colorButton select-button"></a></div>
                                    <div><a href="" id="left_All_1" title="Unselect All" class="fa colorButton select-button"></a></div>
                                </td>
                                <td style="width: 45%">
                                    <select name="to[]" id="mSelectRight" class="multiselect form-control" size="8" multiple="multiple"></select>
                                </td>
                            </tr>
                        </table>
                    </section>
                    <asp:HiddenField ID="hdnColValues" runat="server" Value="" />
                    <asp:HiddenField ID="hdnColNames" runat="server" Value="" />
                    <span class="customErrorMessagee SelectFieldErrorMessage"></span>
                </div>
                <!-- Widget Data Search - end -->

                <!-- Widget Data Filter - begins -->
                <div class="wizardContainer animated fadeIn" id="exWizardFilter">
                    <h2 class="con">
                        <asp:Label ID="lblcond" runat="server" meta:resourcekey="lblcond">Conditions</asp:Label></h2>
                    <section id="dvCondition" class="dvBg dvLevel bdr" style="margin-top: 5px; padding: 10px 5px;">
                        <section id="filtersection" style="max-height: 190px; overflow-y: auto; min-height: 100px;">
                            <table id="tblpanel">
                                <tr>
                                    <th>
                                        <asp:Label ID="lbfield" runat="server" meta:resourcekey="lbfield">Field</asp:Label></th>
                                    <th>
                                        <asp:Label ID="lblcond1" runat="server" meta:resourcekey="lblcond1">Condition</asp:Label></th>
                                    <th>
                                        <asp:Label ID="lblvalue" runat="server" meta:resourcekey="lblvalue">Value</asp:Label></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <td id="lvfld">
                                        <asp:DropDownList ID="ddlFilter" CssClass="lblTxt form-control" runat="server" Style="width: 200px;">
                                        </asp:DropDownList>
                                    </td>
                                    <td id="filcond">
                                        <asp:DropDownList ID="ddlFilcond" CssClass="lblTxt form-control" runat="server" onchange="javascript:CheckFilterCond('-1');">
                                            <asp:ListItem Text="-- Select --" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Equal to" Value="="></asp:ListItem>
                                            <asp:ListItem Text="Not Equal to" Value="!="></asp:ListItem>
                                            <asp:ListItem Text="Less Than" Value="<"></asp:ListItem>
                                            <asp:ListItem Text="Greater Than" Value=">"></asp:ListItem>
                                            <asp:ListItem Text="Less Than or Equal to" Value="<="></asp:ListItem>
                                            <asp:ListItem Text="Greater Than or Equal to" Value=">="></asp:ListItem>
                                            <asp:ListItem Text="Between" Value="between"></asp:ListItem>
                                            <asp:ListItem Text="Starts With" Value="Starts With"></asp:ListItem>
                                            <asp:ListItem Text="Ends With" Value="Ends With"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td id="filval11">
                                        <asp:TextBox ID="txtFilter" Width="100px" runat="server" CssClass="lblTxt form-control" MaxLength="1024" Text=""></asp:TextBox>
                                    </td>
                                    <td id="filval12">
                                        <asp:TextBox ID="filVal2" runat="server" Width="100px" CssClass="lblTxt form-control" MaxLength="1024" Text="" disabled="disabled"></asp:TextBox>
                                        <asp:HiddenField ID="goval" runat="server" Value=""></asp:HiddenField>
                                        <asp:HiddenField ID="FilterRowCount" Value="0" runat="server" />
                                        <asp:HiddenField ID="hdnAction" runat="server" />
                                        <asp:HiddenField ID="hdnFilterXml" runat="server" />
                                        <asp:HiddenField ID="hdnValidate" runat="server" />
                                        <asp:HiddenField ID="hdnDateFields" runat="server" />
                                    </td>
                                    <td>
                                        <a class="curHand icon-arrows-circle-plus" tabindex="0" href="javascript:AddNewCondition();"
                                            id="btnAddNew"></a>
                                        <a class="curHand icon-arrows-circle-minus" href="javascript:ClearFirstRow();"
                                            id="clearCond" tabindex="0"></a>
                                    </td>
                                </tr>
                            </table>
                            <section id="dvNewrow" style="position: relative; top: 3%; height: auto; display: block">
                                <table id="tblFilter">
                                </table>
                            </section>
                        </section>
                    </section>
                </div>
                <!-- Widget Data Filter - end -->

                <!-- Widget Data Export - begins -->
                <div class="wizardContainer animated fadeIn" id="exWizardExport">
                    <section class="form-group col-md-6">
                        <asp:Label ID="lblexfiletype" runat="server" class="lblleft" meta:resourcekey="lblexfiletype" AssociatedControlID="ddlExFileType">
                                            File Type<span class="allowempty">*</span>
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question helptxt" data-toggle="popover" id="icocl3" data-content="Select the file format to which the data to be exported." data-placement="right" style="cursor: pointer;"></i>
                        <asp:DropDownList ID="ddlExFileType" CssClass="form-control" runat="server" onchange="javascript:EnableSeparator()">
                            <asp:ListItem Value="Text">Text</asp:ListItem>
                            <asp:ListItem Value="CSV">CSV</asp:ListItem>
                            <%--<asp:ListItem Value="Excel">Excel</asp:ListItem>--%>
                        </asp:DropDownList>
                        <span class="customErrorMessage SelectFielTypeErrorMessage"></span>
                    </section>
                    <section class="form-group col-md-6">
                        <asp:Label runat="server" class="lblleft" ID="lblexseparator" meta:resourcekey="lblexseparator" AssociatedControlID="ddlExSeparator">
                                            Seperator
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question helptxt" data-toggle="popover" id="icocl4" data-content="Select the type of seperator used to separate the column values on each line." data-placement="right" style="cursor: pointer;"></i>

                        <asp:DropDownList ID="ddlExSeparator" CssClass="form-control" runat="server" disabled="disabled">
                            <asp:ListItem Value="">-- Select --</asp:ListItem>
                            <asp:ListItem Value="," Selected="True">Comma(,)</asp:ListItem>
                            <asp:ListItem Value=";">Semicolon(;)</asp:ListItem>
                            <asp:ListItem Value="|">Pipe(|)</asp:ListItem>
                        </asp:DropDownList>
                    </section>
                    <section class="form-group col-md-12">
                        <asp:Label ID="lblexpfilename" meta:resourcekey="lblexpfilename" runat="server" class="lblleft" AssociatedControlID="txtExpFileName">
                                            File Name<span class="allowempty">*</span>
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question helptxt" data-toggle="popover" id="icocl5" data-content="Please specify the file name to which data will be exported.By default the file name will same as the selected form name." data-placement="right" style="cursor: pointer;"></i>

                        <asp:TextBox ID="txtExpFileName" CssClass="form-control checkForSpecialCharacters" MaxLength="250" onblur="javascript:DisChks()" runat="server"></asp:TextBox>
                        <span class="customErrorMessage FielNameToExportErrorMessage"></span>
                    </section>
                    <section class="form-group col-md-12 app">
                        <asp:Label runat="server" class="checkbox-inline" AssociatedControlID="chkWithHeader">
                            <asp:CheckBox ID="chkWithHeader" runat="server" Text="With Header" meta:resourcekey="lblWithHeader" />
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl6" data-content="If you wish to export the data with column names, check mark this option." data-placement="right" style="cursor: pointer;"></i>
                        <br />
                        <asp:Label runat="server" class="checkbox-inline" AssociatedControlID="chkExWithQuotes">
                            <asp:CheckBox ID="chkExWithQuotes" onclick="javascript:SetExType();" runat="server" meta:resourcekey="lblWithQuotes" />
                        </asp:Label>
                        <i tabindex="0" data-trigger="focus" class="icon-arrows-question" data-toggle="popover" id="icocl7" data-content="Use this option to specify how to identify text strings in a column. The default value is double quotation marks." data-placement="right" style="cursor: pointer;"></i>
                    </section>
                </div>
                <!-- Widget Data Export - end -->

                <!-- Widget Complete flow - begins -->
                <div class="wizardContainer animated fadeIn" id="exWizardComplete" style="text-align: center !important;">
                    <asp:ScriptManager runat="server" ID="sm">
                    </asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnExport" />
                        </Triggers>
                        <ContentTemplate>
                            <br />
                            <div class="container">
                                <asp:Label ID="lblSuccess" runat="server"></asp:Label>
                            </div>
                            <br />
                            <asp:HyperLink ID="lnkExpFile" CssClass="hotbtn btn" runat="server" Visible="false" ToolTip="Download"></asp:HyperLink>
                            <section style="display: none">
                                <asp:Button ID="btnExport" CssClass="hotbtn btn btn-info-full" runat="server" Text="Export" OnClick="btnExport_Click" />
                            </section>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <!-- Widget Complete flow - end -->

            </div>
        </div>
        <div id="wizardFooter" class="wizard-footer">
            <div class="pull-left">
                <button onclick="expWizardObj.checkClick(this,'prev')" title="Previous" type="button" id="wizardPrevbtn" style="display: none;" class="btn hotbtn prev-step">&lt; Prev</button>
            </div>
            <div class="pull-right">
                <button id="wizardCancelbtn" onclick="closeWindow()" type="button" title="Cancel" class="coldbtn btn btn-info-full ">Cancel</button>
                <button onclick="expWizardObj.checkClick(this,'next')" data-validator="" title="Next" id="wizardNextbtn" type="button" style="display: none;" class="hotbtn btn ">Next &gt;</button>
                <button type="button" style="display: none;" id="wizardCompbtn" class="hotbtn btn  " title="Done">Done</button>
            </div>
        </div>
        <!-- Widget work flow - end -->

        <div id='waitDiv' style='display: none;'>
            <div id='backgroundDiv' style='background: url(../Axpimages/loadingBars.gif) center center no-repeat rgba(255, 255, 255, 0.843137); background-size: 135px;'>
            </div>
        </div>

        <asp:HiddenField ID="hdnCondString" runat="server" />
    </form>
</body>
</html>
